package com.eurosoft.vtrack;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vtrack.Models.AuthCodeResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.LoginAndAuthResponse;
import com.eurosoft.vtrack.Models.VehicleStatusCode;
import com.eurosoft.vtrack.Utils.InputValidatorHelper;
import com.eurosoft.vtrack.Utils.NetworkAvailable.AppStatus;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.fxn.stash.Stash;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;
import static com.eurosoft.vtrack.Constants.GET_BACKEND_URL;
import static com.eurosoft.vtrack.Constants.GET_LIVEAPI_URL;
import static com.eurosoft.vtrack.Constants.GET_OPENSTREET_URL;
import static com.eurosoft.vtrack.Constants.GET_REPORT_URL;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText edtEmail;
    private EditText edtPassword;
    private TextInputLayout passwordTextInput, emailTextInput;
    private String email, password;
    private TextView welcome, signintocontinue;
    private ImageView viewPassToggle;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private Button btnLogin;
    private InputValidatorHelper inputValidatorHelper;
    private View content;
    private TextView forgetPass;
    private Dialog dialog;
    private ViewDialog alertDialoge;
    private String fetchedUrl = "";
    private MasterPojo masterPojo;
    private static Retrofit retrofit = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Stash.init(this);
        alertDialoge = new ViewDialog();
        inputValidatorHelper = new InputValidatorHelper();
        initViews();
        fetchedUrl = (String) Stash.getString(Constants.URL_FETCHED);
    }

    private void initViews() {
        emailTextInput = findViewById(R.id.input_layout_email);
        edtEmail = findViewById(R.id.edtEmail);
        passwordTextInput = findViewById(R.id.input_layout_password);
        edtPassword = findViewById(R.id.edtPassword);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        viewPassToggle = findViewById(R.id.viewPassToggle);
        btnLogin = findViewById(R.id.btnLogin);
        content = findViewById(R.id.content);
        forgetPass = findViewById(R.id.forgetPass);
        btnLogin.setOnClickListener(this);
        viewPassToggle.setOnClickListener(this);
        forgetPass.setOnClickListener(this);
        if (Stash.getObject(Constants.LANGUAGE_DATA, MasterPojo.class) != null) {
            masterPojo = (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA, MasterPojo.class);
        } else {
            masterPojo = new MasterPojo();
        }
        welcome = (TextView) findViewById(R.id.welcome);
        signintocontinue = (TextView) findViewById(R.id.signintocontinue);
        welcome.setText(masterPojo.getWelcome());
        btnLogin.setText(masterPojo.getSignIn());
        signintocontinue.setText(masterPojo.getSignintocontinue());
        edtEmail.setHint(masterPojo.getUserName());
        edtPassword.setHint(masterPojo.getPassword());
        emailTextInput.setHint(masterPojo.getUserName());
        passwordTextInput.setHint(masterPojo.getPassword());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                validation();
                break;
            case R.id.viewPassToggle:
                viewPassToggleState(v);
                break;
            case R.id.forgetPass:
                alertDialoge.showDialog(this);
                break;
        }
    }


    private void viewPassToggleState(View view) {
        if (edtPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
            ((ImageView) (view)).setImageResource(R.drawable.password_toggle);
            //Show Password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_remove_red_eye_24);
            //Hide Password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void validation() {
        btnLogin.setClickable(false);
        email = edtEmail.getText().toString().replace(" ", "");
        password = edtPassword.getText().toString();
        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toasty.warning(this, masterPojo.getUsernameisempty(), Toast.LENGTH_SHORT, true).show();
            btnLogin.setClickable(true);
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(password)) {
            Toasty.warning(this, masterPojo.getPasswordisempty(), Toast.LENGTH_SHORT, true).show();
            btnLogin.setClickable(true);
            return;
        }

        if (!AppStatus.getInstance(this).isOnline()) {
            Toasty.warning(this, masterPojo.getPleasecheckyourinternet(), Toasty.LENGTH_SHORT).show();
            btnLogin.setClickable(true);
            return;
        } else {
            callApiLogin(email, password);
        }
    }


    private void callApiLogin(String email, String password) {
        progressVisiblityVisible();
        LoginAndAuthResponse loginresponsee2 = new LoginAndAuthResponse();
        loginresponsee2.setAccountCode(email);
        loginresponsee2.setPassword(password);
/*        LoginResponse user = new LoginResponse();
        user.setUserName(email);
        user.setPassword(password);
        user.setClientId(Stash.getString(Constants.USER_CLIENT_ID));*/

        ApiInterface apiService = APIClient.getApiBaseClient().create(ApiInterface.class);
        Call<AuthCodeResponse> call = apiService.getAuthResponseAndLogin(loginresponsee2);

        call.enqueue(new Callback<AuthCodeResponse>() {
            @Override
            public void onResponse(Call<AuthCodeResponse> call, Response<AuthCodeResponse> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        progressVisiblityGone();
                        Toasty.error(getApplicationContext(), masterPojo.getInvalidUserNameorpassword(), Toast.LENGTH_SHORT, true).show();
                        return;
                    }
                    AuthCodeResponse authCodeResponse = response.body();
                    if (response.isSuccessful() && response.code() == 200) {
                        if (authCodeResponse.getVTrackInfo().getClientId() != null && authCodeResponse.getVTrackInfo().getPortalUrl() != null) {
                            Stash.put(Constants.URL_FETCHED, authCodeResponse.getVTrackInfo().getPortalUrl());
                            Stash.put(Constants.USER_CLIENT_ID, authCodeResponse.getVTrackInfo().getClientId());
                            loginWork(authCodeResponse, email);
                        } else {
                            progressVisiblityGone();
                            Toasty.error(getApplicationContext(), masterPojo.getInvalidUserNameorpassword(), Toast.LENGTH_SHORT, true).show();
                        }
                    }
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<AuthCodeResponse> call, Throwable t) {
                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }


    private void progressVisiblityGone() {
        btnLogin.setClickable(true);
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }


    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_forget_password);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            EditText editTextEmail = (EditText) dialog.findViewById(R.id.emailET);
            Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmit);

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String email = editTextEmail.getText().toString();
                    if (email.equalsIgnoreCase("")) {
                        Toasty.error(getApplicationContext(), masterPojo.getEmailisempty(), Toast.LENGTH_SHORT, true).show();
                        return;
                    }
                    if (!inputValidatorHelper.isValidEmail(email)) {
                        Toasty.error(getApplicationContext(), masterPojo.getNotavalidemail(), Toast.LENGTH_SHORT, true).show();
                        return;
                    } else {
                        callApiForgetPass(dialog);
                    }
                }
            });
            dialog.show();
        }
    }

    private void callApiForgetPass(Dialog dialog) {
        dialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public void loginWork(AuthCodeResponse authCodeResponse, String email) {

        SaveData(authCodeResponse);

    }

    public boolean removeFile(String name) {
        File dir = getFilesDir();
        File file = new File(dir, name + ".png");
        boolean delete = false;
        if (file.exists()) {
            delete = file.delete();
        } else {
            delete = true;
        }
        return delete;
    }

    private String saveToInternalStorage(Bitmap bitmapImage, String fileName) {
        removeFile(fileName);
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, fileName + "");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            //  Log.e("saveToInternalStorage()", "sssss"+directory);

        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());

        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                Log.e("saveToInternalStorage()", e.getMessage());

            }
        }
        return directory.getAbsolutePath();
    }


    private void imageLoader(AuthCodeResponse.VTrackAppResponse authCodeResponse) {
//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
        try {
            ArrayList<VehicleStatusCode> vehicleStatusCode = new ArrayList<VehicleStatusCode>();
            VehicleStatusCode vehicleStatusCode1;
            for (int i = 0; i < authCodeResponse.getData().getVehicleStatusCode().size(); i++) {
                //  Log.e("saveToInternalStorage()", "url" + authCodeResponse.getVTrackAppResponse().getData().getVehicleStatusCode().get(i).getVehicleImgUrl());
                Bitmap bitmap = getBitmapFromURL(authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleImgUrl());
                String filename = "" + authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleImgUrl() + ".png";
                filename = authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleStatus() + ".png";
                saveToInternalStorage(bitmap, filename);
                vehicleStatusCode1 = new VehicleStatusCode(authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleColor(), authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleStatus(), authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleImgUrl(), authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleStatusCode(), filename
                        , authCodeResponse.getData().getVehicleStatusCode().get(i).getVehicleTextColor()
                );
                vehicleStatusCode.add(vehicleStatusCode1);
                Log.e("getIcon_name()", vehicleStatusCode.get(i).getIcon_name());
            }
            Stash.put(Constants.VehicleDetails, vehicleStatusCode);

        } catch (Exception e) {
        }
//            }
//        });
//        thread.start();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void SaveData(AuthCodeResponse authCodeResponse) {
        class SaveData extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Gson gson = new Gson();
                    //   Log.e("getLoginSuccessfully", ""+authCodeResponse.getVTrackAppResponse());
                    AuthCodeResponse.VTrackAppResponse vTrackAppResponse = gson.fromJson(authCodeResponse.getVTrackAppResponse(), AuthCodeResponse.VTrackAppResponse.class);
                    MasterPojo masterPojo = gson.fromJson(vTrackAppResponse.getData().getLanguageDict(), MasterPojo.class);
                    imageLoader(vTrackAppResponse);
                    String mastermodel = gson.toJson(masterPojo);
                    Stash.put(Constants.LANGUAGE_DATA, mastermodel);
                    Log.e("getLoginSuccessfully", "masterPojo.getLoginSuccessfully()");
                    Stash.put(GET_REPORT_URL, authCodeResponse.getvTracklink().getVTrackReportsAPI());
                    Stash.put(GET_BACKEND_URL, authCodeResponse.getvTracklink().getVTrackBackendAPI());
                    Stash.put(GET_LIVEAPI_URL, authCodeResponse.getvTracklink().getVTrackLiveAPI());
                    Stash.put(GET_OPENSTREET_URL, authCodeResponse.getvTracklink().getVTrackAddressServerAPI());
                    Stash.put(Constants.isLoggenIn, true);
                    Stash.put(Constants.TIME_ZONE, vTrackAppResponse.getData().getTimezone());
                    Stash.put(Constants.USER, vTrackAppResponse.getData());
                    Stash.put(Constants.ClientName, vTrackAppResponse.getData().getClientName());
                    //  Log.e("getLoginSuccessfully", "" + vehicleStatusCodeArrayList);
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressVisiblityGone();
                        Toasty.success(getApplicationContext(), masterPojo.getLoginSuccessfully(), Toast.LENGTH_SHORT, true).show();
                        Intent homeIntent = new Intent(ActivityLogin.this, MainActivity.class);
                        startActivity(homeIntent);
                        finish();
                    }
                });

            }
        }
        new SaveData().execute();

    }


}