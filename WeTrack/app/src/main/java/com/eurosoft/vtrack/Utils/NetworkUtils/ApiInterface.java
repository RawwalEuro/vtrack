package com.eurosoft.vtrack.Utils.NetworkUtils;

import com.eurosoft.vtrack.Models.AuthCodeResponse;
import com.eurosoft.vtrack.Models.Journey;
import com.eurosoft.vtrack.Models.JourneyHistory;
import com.eurosoft.vtrack.Models.LocationResponseModel;
import com.eurosoft.vtrack.Models.LoginAndAuthResponse;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.NotificationBody;
import com.eurosoft.vtrack.Models.NotificationRulesModel;
import com.eurosoft.vtrack.Models.Notifications;
import com.eurosoft.vtrack.Models.ReportBody;
import com.eurosoft.vtrack.Models.ReportResponseModel;
import com.eurosoft.vtrack.Models.VersionUpdate;
import com.eurosoft.vtrack.Models.WebResponseString;
import com.eurosoft.vtrack.Models.Zone;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {


    @POST("GetVTrackInfoDetailNew")
    Call<AuthCodeResponse> getAuthResponseAndLogin(@Body LoginAndAuthResponse loginResponse);

    @POST("Applogin")
    Call<WebResponse<LoginResponse>> loginUser(@Body LoginResponse loginUser);


    @POST("TripsByDateAndVehicleV2")
    Call<CustomWebResponse<Journey>> getJourniesByDateAndVehReg(@Body Journey Journey ,  @Header("Authorization") String auth);


    @POST("TravelHistoryByDateV2")
    Call<CustomWebResponse<JourneyHistory>> getJourneyHistoryDetails(@Body JourneyHistory journeyHistory , @Header("Authorization") String auth);


    @POST("NotificationCenter/alertDetail")
    Call<CustomWebResponse<Notifications>> getAllEventsByPagination(@Query("page") int page, @Query("limit") int limit,@Body Notifications notifications, @Header("Authorization") String auth);

    @POST("IgnitionReportAddressWise")
    Call<ReportResponseModel> getReportsAddressWise(@Body ReportBody reportBody,@Header("Authorization") String auth);


    @POST("IgnitionReport")
    Call<ReportResponseModel> getReports(@Body ReportBody reportBody, @Header("Authorization") String auth);

    @POST("NotificationCenter/alertsCount")
    Call<CustomWebResponse<Zone>> getZones(@Body Notifications notification,@Header("Authorization") String auth);


    @POST("NotificationCenter/alertDetail")
    Call<CustomWebResponse<Notifications>> getAllEvents(@Body NotificationBody notificationBody, @Header("Authorization") String auth);


    @POST("AppVersion/Find")
    Call<CustomWebResponse<VersionUpdate>> versionUpdate(@Body VersionUpdate versionUpdate);

    @POST("NotificationCenter/AllRuleTypes")
    Call<List<NotificationRulesModel>> getAllRules(@Header("Authorization") String auth);

    @POST("NotificationCenter/tripAddress")
    Call<WebResponseString> getlocationaddress(@Body LocationResponseModel locationResponseModel, @Header("Authorization") String auth);
}
