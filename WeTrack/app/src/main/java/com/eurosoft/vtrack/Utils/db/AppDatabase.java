package com.eurosoft.vtrack.Utils.db;
import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.eurosoft.vtrack.Interfaces.VehicleDao;
import com.eurosoft.vtrack.Models.Vehicles;


@Database(entities = {Vehicles.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract VehicleDao vehiclesDao();

}
