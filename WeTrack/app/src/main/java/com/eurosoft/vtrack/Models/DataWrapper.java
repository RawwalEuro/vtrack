package com.eurosoft.vtrack.Models;

import java.math.BigDecimal;

public class DataWrapper {
    public static DataWrapper dataWrapper;

    public static DataWrapper getInstance(){
        if(dataWrapper==null){
            dataWrapper=new DataWrapper();
        }
        return dataWrapper;
    }

    public static BigDecimal numberWrapper(Object obj, int returnvalue){
        try {
            if(obj instanceof  Object){
                return (BigDecimal) obj;
            }
            else {
                return new BigDecimal(returnvalue);
            }
        }
        catch (Exception e){
            return new BigDecimal(returnvalue);

        }
    }

    public static Boolean booleanWrapper(Object obj){
        try {
            if(obj instanceof  Object){
                return (Boolean) obj;
            }
            else {
                return false;
            }
        }
        catch (Exception e){
            return false;

        }
    }

    public static String stringWrapper(Object obj){
        try {
            if(obj!=null && obj instanceof  Object){
                return (String) obj;
            }
            else {
                return "";
            }
        }
        catch (Exception e){
            return "";

        }
    }
}
