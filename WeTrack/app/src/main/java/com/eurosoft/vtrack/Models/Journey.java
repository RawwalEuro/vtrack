package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Journey implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Vehicle")
    @Expose
    private String vehicle;
    @SerializedName("IMEI")
    @Expose
    private String imei;
    @SerializedName("TripStart")
    @Expose
    private String tripStart;
    @SerializedName("TripEnd")
    @Expose
    private String tripEnd;
    @SerializedName("MilesCovered")
    @Expose
    private Double milesCovered;
    @SerializedName("TripDurationHr")
    @Expose
    private int TripDurationHr;
    @SerializedName("TripDurationMins")
    @Expose
    private int TripDurationMins;
    @SerializedName("fromDateTime")
    @Expose
    private String fromDateTime;
    @SerializedName("toDateTime")
    @Expose
    private String toDateTime;
    @SerializedName("VehicleReg")
    @Expose
    private String VehicleReg;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("AvgSpeed")
    @Expose
    private String AvgSpeed;
    @SerializedName("StartingPoint")
    @Expose
    private String Stargingpoint;
    @SerializedName("EndingPoint")
    @Expose
    private String EndingPoint;
    @SerializedName("TotalDistance")
    @Expose
    private String TotalDistance;
    @SerializedName("AverageSpeed")
    @Expose
    private String AverageSpeed;
    @SerializedName("TimeZone")
    @Expose
    private String TimeZone;
    @SerializedName("clientId")
    @Expose
    private String clientId;

    @SerializedName("TripStartDateLabel")
    @Expose
    private String TripStartDateLabel;
    @SerializedName("TripEndDateLabel")
    @Expose
    private String TripEndDateLabel;
    @SerializedName("TripStartTimeLabel")
    @Expose
    private String TripStartTimeLabel;
    @SerializedName("TripEndTimeLabel")
    @Expose
    private String TripEndTimeLabel;

    public String getTripStartDateLabel() {
        return TripStartDateLabel;
    }

    public void setTripStartDateLabel(String tripStartDateLabel) {
        TripStartDateLabel = tripStartDateLabel;
    }

    public String getTripEndDateLabel() {
        return TripEndDateLabel;
    }

    public void setTripEndDateLabel(String tripEndDateLabel) {
        TripEndDateLabel = tripEndDateLabel;
    }

    public String getTripStartTimeLabel() {
        return TripStartTimeLabel;
    }

    public void setTripStartTimeLabel(String tripStartTimeLabel) {
        TripStartTimeLabel = tripStartTimeLabel;
    }

    public String getTripEndTimeLabel() {
        return TripEndTimeLabel;
    }

    public void setTripEndTimeLabel(String tripEndTimeLabel) {
        TripEndTimeLabel = tripEndTimeLabel;
    }

    public String getTimeZone() {
        return TimeZone;
    }

    public void setTimeZone(String timeZone) {
        TimeZone = timeZone;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTotalDistance() {
        return TotalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        TotalDistance = totalDistance;
    }

    public String getAverageSpeed() {
        return AverageSpeed;
    }

    public void setAverageSpeed(String averageSpeed) {
        AverageSpeed = averageSpeed;
    }

    public String getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(String fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public String getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(String toDateTime) {
        this.toDateTime = toDateTime;
    }

    public String getVehicleReg() {
        return VehicleReg;
    }

    public void setVehicleReg(String vehicleReg) {
        VehicleReg = vehicleReg;
    }

    public String getAvgSpeed() {
        return AvgSpeed;
    }

    public void setAvgSpeed(String avgSpeed) {
        AvgSpeed = avgSpeed;
    }

    public String getStargingpoint() {
        return Stargingpoint;
    }

    public void setStargingpoint(String stargingpoint) {
        Stargingpoint = stargingpoint;
    }

    public String getEndingPoint() {
        return EndingPoint;
    }

    public void setEndingPoint(String endingPoint) {
        EndingPoint = endingPoint;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Journey(String fromDateTime, String toDateTime, String vehicleReg,String unit ,String clientId,String timeZone) {
        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        this.VehicleReg = vehicleReg;
        this.unit = unit;
        this.clientId = clientId;
        this.TimeZone = timeZone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getTripStart() {
        return tripStart;
    }

    public void setTripStart(String tripStart) {
        this.tripStart = tripStart;
    }

    public String getTripEnd() {
        return tripEnd;
    }

    public void setTripEnd(String tripEnd) {
        this.tripEnd = tripEnd;
    }

    public Double getMilesCovered() {
        return milesCovered;
    }

    public void setMilesCovered(Double milesCovered) {
        this.milesCovered = milesCovered;
    }


    public int getTripDurationHr() {
        return TripDurationHr;
    }

    public void setTripDurationHr(int tripDurationHr) {
        TripDurationHr = tripDurationHr;
    }

    public int getTripDurationMins() {
        return TripDurationMins;
    }

    public void setTripDurationMins(int tripDurationMins) {
        TripDurationMins = tripDurationMins;
    }
}
