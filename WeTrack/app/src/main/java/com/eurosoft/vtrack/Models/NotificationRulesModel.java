package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationRulesModel {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("ruleType")
    @Expose
    private String ruleType;
    @SerializedName("eventTypeId")
    @Expose
    private String eventTypeId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("LanguagDict")
    @Expose
    private String languagDict;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getLanguagDict() {
        return languagDict;
    }

    public void setLanguagDict(String languagDict) {
        this.languagDict = languagDict;
    }
}
