package com.eurosoft.vtrack.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.R;
import org.jetbrains.annotations.NotNull;

import java.util.List;


public class AdapterVehicles extends RecyclerView.Adapter<AdapterVehicles.ViewHolder> {


    private List<Vehicles> lstVehicles;
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    int row_index=-1;
    private MasterPojo masterPojo;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    public AdapterVehicles(Context ctx, List<Vehicles> vehiclesList, OnItemClickListener onItemClickListener,MasterPojo mmasterPojo) {
        context = ctx;
        lstVehicles = vehiclesList;
        mOnItemClickListener = onItemClickListener;
        masterPojo=mmasterPojo;
    }


    @Override
    public AdapterVehicles.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_vehicles, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterVehicles.ViewHolder holder, int position) {
        Vehicles vehicles = lstVehicles.get(position);
        holder.vehicleReg.setText(vehicles.getVehicleReg());
        holder.vehicleType.setText(vehicles.getVehicleMake());


        if (row_index == position) {
            holder.rlBg.setBackgroundColor(Color.parseColor("#03884A"));
            holder.vehicleReg.setTextColor(Color.parseColor("#FFFFFF"));
            holder.vehicleType.setTextColor(Color.parseColor("#FFFFFF"));
            holder.filter.setColorFilter(Color.parseColor("#FFFFFF"));
        } else {
            holder.rlBg.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.vehicleReg.setTextColor(Color.parseColor("#000000"));
            holder.vehicleType.setTextColor(Color.parseColor("#000000"));
            holder.filter.setColorFilter(Color.parseColor("#000000"));
        }

        holder.rlBg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                v.setClickable(false);
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());
                row_index=holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return lstVehicles.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vehicleReg, vehicleType;
        private RelativeLayout rlBg;
        private ImageView filter;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            vehicleReg = itemView.findViewById(R.id.vehicleReg);
            vehicleType = itemView.findViewById(R.id.vehicleType);
            rlBg = itemView.findViewById(R.id.rlBg);
            filter = itemView.findViewById(R.id.filter);
        }

    }
}
