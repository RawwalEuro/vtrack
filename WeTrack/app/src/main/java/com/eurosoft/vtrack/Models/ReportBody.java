package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportBody {


    @SerializedName("VehicleReg")
    @Expose
    private String vehicle;
    @SerializedName("fromDateTime")
    @Expose
    private String fromdate;
    @SerializedName("toDateTime")
    @Expose
    private String todate;
    @SerializedName("reportType")
    @Expose
    private String reportType;
    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("TimeZone")
    @Expose
    private String TimeZone;
    @SerializedName("ClientId")
    @Expose
    private String ClientId;


    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTimeZone() {
        return TimeZone;
    }

    public void setTimeZone(String timeZone) {
        TimeZone = timeZone;
    }

    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
}
