package com.eurosoft.vtrack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.vtrack.Adapters.AdapterListJourneyByDate;
import com.eurosoft.vtrack.Models.Journey;
import com.eurosoft.vtrack.Models.JourneyByVehicle;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Utils.NetworkAvailable.AppStatus;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;

public class ActivityListJourneyByDate extends AppCompatActivity {

    private ImageView closeIcon;
    private TextView header_title, vehicleId, vehicleType;
    private RecyclerView rvList;
    private AdapterListJourneyByDate adapterListJournies;
    private LoginResponse user;
    private RelativeLayout rlProgressBar, mainRl;
    private CircularProgressBar circularProgressBar;
    private ArrayList<Journey> arrayListJourney;
    private String fetchedUrl = "";
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_journey_by_date);
        Stash.init(this);
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        fetchedUrl = (String) Stash.getString(Constants.URL_FETCHED);
        initViews();
        getData();
    }

    private void initViews() {

        closeIcon = findViewById(R.id.closeIcon);
        header_title = findViewById(R.id.header_title);
        rvList = findViewById(R.id.rvList);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        vehicleId = findViewById(R.id.vehicleId);
        vehicleType = findViewById(R.id.vehicleType);
        masterPojo =  (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        TextView pleasewait_text = findViewById(R.id.pleasewait_text);
        TextView message = findViewById(R.id.message);
        pleasewait_text.setText(masterPojo.getPleasewait());
        message.setText(masterPojo.getWearelookingforyourtripshistory());
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getData() {

        JourneyByVehicle model = (JourneyByVehicle) getIntent().getSerializableExtra("journeyByVehicle");
        arrayListJourney = model.getJourneyArrayList();
        header_title.setText(masterPojo.getTrips() + " " + " (" + arrayListJourney.size() + ")");
        vehicleId.setText(model.getVehicleId());
        vehicleType.setText(model.getVehicleType());
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapterListJournies = new AdapterListJourneyByDate(getApplicationContext(), arrayListJourney, new AdapterListJourneyByDate.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Journey journey) {

                try {
                    if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
                        Toasty.error(getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
                        return;
                    }
                    if (journey.getTripEnd() == null) {
                        return;
                    }
                    Intent intent = new Intent(ActivityListJourneyByDate.this, ActivityJourneyTrack.class);
                    model.setTripId(arrayListJourney.get(position).getId());
                    model.setVehicleId(model.getVehicleId());
                    model.setStartAddress(arrayListJourney.get(position).getStargingpoint());
                    model.setEndAddress(arrayListJourney.get(position).getEndingPoint());
                    model.setStartDate(arrayListJourney.get(position).getTripStart());
                    model.setEndDate(arrayListJourney.get(position).getTripEnd());
                    intent.putExtra("journeyByVehicle", model);
                    startActivity(intent);
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }
            }
        }, masterPojo);
        rvList.setAdapter(adapterListJournies);
        progressVisiblityGone();


    }

  /*  private void callApi(String startDate, String endDate, String vehReg) {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient(fetchedUrl).create(ApiInterface.class);

        Journey journey = new Journey(startDate, endDate, vehReg);
        Call<CustomWebResponse<Journey>> call = apiService.getJourniesByDateAndVehReg(journey, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Journey>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Journey>> call, Response<CustomWebResponse<Journey>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {


                    arrayListJourney = response.body().getData();

                    rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    adapterListJournies = new AdapterListJourneyByDate(getApplicationContext(), response.body().getData(), new AdapterListJourneyByDate.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {


                        }
                    });
                    rvList.setAdapter(adapterListJournies);
                    progressVisiblityGone();

                }
            }


            @Override
            public void onFailure(Call<CustomWebResponse<Journey>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                progressVisiblityGone();

            }
        });

    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }
     */

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }


}