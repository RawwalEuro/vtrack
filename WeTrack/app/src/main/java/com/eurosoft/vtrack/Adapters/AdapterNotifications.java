package com.eurosoft.vtrack.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Models.Notifications;
import com.eurosoft.vtrack.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AdapterNotifications extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Notifications> lstNotification;
    private Context context;
    private static final int LOADING = 0;
    private static final int ITEM = 1;
    private boolean isLoadingAdded = false;

    public AdapterNotifications(Context ctx) {
        context = ctx;
        lstNotification = new LinkedList<>();
    }

    public AdapterNotifications(Context ctx, List<Notifications> notificationsList) {
        context = ctx;
        lstNotification = notificationsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.adapter_notifications, parent, false);
                viewHolder = new ItemViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingViewHolder(viewLoading);
                break;
        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Notifications notification = lstNotification.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                itemViewHolder.eventTitle.setText(notification.getEventTitle());
                itemViewHolder.eventDetail.setText(notification.getEventDetail() + "");
                itemViewHolder.eventOccuredOn.setText(notification.getEventOccuredOn() + "");
                break;
            case LOADING:
                LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
                loadingViewHolder.progressBar.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return lstNotification == null ? 0 : lstNotification.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == lstNotification.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Notifications());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        int position = lstNotification.size() - 1;
        Notifications result = getItem(position);

        if (result != null) {
            lstNotification.remove(position);
            notifyItemRemoved(position);
        }
    }


    public Notifications getItem(int position) {
        return lstNotification.get(position);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView eventTitle, eventDetail, eventOccuredOn;

        public ItemViewHolder(View itemView) {
            super(itemView);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            eventDetail = itemView.findViewById(R.id.eventDetail);
            eventOccuredOn = itemView.findViewById(R.id.eventOccuredOn);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        private ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
        }
    }

    public void setLstNotification(List<Notifications> notificationsList) {
        this.lstNotification = notificationsList;
    }

    public void add(Notifications notifications) {
        if (lstNotification == null) {
            lstNotification = new ArrayList<>();
            lstNotification.add(notifications);
            notifyItemInserted(lstNotification.size() - 1);
        } else {
            if(notifications.getVehicle() == null){
                return;
            }
            lstNotification.add(notifications);
            notifyItemInserted(lstNotification.size() - 1);
        }
    }

    public void addAll(List<Notifications> notificationsList) {
        for (Notifications result : notificationsList) {
            add(result);
        }
    }

    public void clearAll(){
        if(lstNotification != null || lstNotification.size() != 0){
          lstNotification.clear();
          notifyDataSetChanged();
        }
    }
}
