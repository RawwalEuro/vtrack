package com.eurosoft.vtrack.Interfaces;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.vtrack.Models.Vehicles;

import java.util.List;

@Dao
public interface VehicleDao {
    @Query("SELECT * FROM vehicles")
    List<Vehicles> getAll();

    @Insert
    void insert(Vehicles vehicles);

    @Delete
    void delete(Vehicles vehicles);

    @Update
    void update(Vehicles vehicles);

    @Query("DELETE FROM vehicles WHERE Id = :id")
    void deleteAll(int id);

    @Query("DELETE FROM vehicles")
    void deleteAll();

}
