package com.eurosoft.vtrack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vtrack.Models.Journey;
import com.eurosoft.vtrack.Models.JourneyByVehicle;
import com.eurosoft.vtrack.Models.JourneyHistory;
import com.eurosoft.vtrack.Models.LiveMapsModel;
import com.eurosoft.vtrack.Models.LocationResponseModel;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.WebResponseString;
import com.eurosoft.vtrack.Utils.NetworkAvailable.AppStatus;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.NetworkUtils.CustomWebResponse;
import com.fxn.stash.Stash;
import com.github.anastr.speedviewlib.SpeedView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.vtrack.Constants.APIV2;
import static com.eurosoft.vtrack.Constants.DATETIME_FORMATFROMBACKEND_ACTIVITYTRACK;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;
import static com.eurosoft.vtrack.Constants.GET_OPENSTREET_URL;

public class ActivityJourneyTrack extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {


    private RelativeLayout rlProgressBar, mainRl,bottomLayout, currentsummarylayout, currentlocationRR, controllerRR;
    private CircularProgressBar circularProgressBar;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LoginResponse user;
    private ArrayList<JourneyHistory> markersList = new ArrayList<>();
    private ArrayList<LatLng> listOfPoints = new ArrayList<>();
    private ImageView closeIcon,play, pause;
    private Marker mMarker;
    private BitmapDescriptor icon;
    private LinearLayout linearPlay, linearPause,summaryLayout, currentdataLL, speedometerlayout;
    private Handler handler;
    Runnable runnable;
    private boolean isPause,isAlreadyPlaying = false,isPlaying = false;
    private ViewDialog alertDialoge;
    private ArrayList<Marker> markersStop = new ArrayList<>();
    private TextView pointAText, pointBText, travelTime, distance, speed, currentdate, currenttravelTime, currentspeed, currentlocation, distancepermile,header_title;
    private com.warkiz.widget.IndicatorSeekBar seekBar;
    private MasterPojo masterPojo;
    private int Speed = 500,noofpoint,seconds= 120,totalpoint = 0,currentPt = 0;
    Spinner speed_spinner;
    ProgressBar progressBar;
    SpeedView speedometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_track);
        alertDialoge = new ViewDialog();
        Stash.init(this);
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        initViews();
        try {
            getData();
        } catch (ParseException e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }
        progressVisiblityVisible();
    }


    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(@NonNull @NotNull Marker marker) {
                if (isPlaying) {
                    return false;
                } else {
                    progressVisiblityVisible();
                    JourneyHistory journeyHistory = (JourneyHistory) marker.getTag();
                    callApiOkHttp(journeyHistory, true);
                }
                return false;
            }
        });
    }


    @Override
    public boolean onMarkerClick(@NonNull @NotNull Marker marker) {
        return false;
    }


    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.play:
                    isPlaying = true;
                    isPause = false;
                    bottomLayout.setVisibility(View.GONE);
                    currentsummarylayout.setVisibility(View.GONE);
                    if (!isAlreadyPlaying) {
                        if (markersList.get(0).getLat() != 0 && markersList.get(0).getLng() != 0) {
                            mMarker = mMap.addMarker(new MarkerOptions().anchor(0.5f, 0.5f).position(new LatLng(markersList.get(0).getLat(), markersList.get(0).getLng())).icon(BitmapDescriptorFactory.fromResource(R.drawable.avcar)).zIndex(0.10f));
                        } else {
                            Toast.makeText(ActivityJourneyTrack.this, "Lat and Long from Api is  lat:" + markersList.get(0).getLat() + "long:" + markersList.get(0).getLng(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    playJourney();
                    markersStop.clear();
                    break;
                case R.id.closeIcon:
                    finish();
                    break;
                case R.id.pause:
                    isPause = true;
                    isPlaying = false;
                    playPauseVisiblity(false);
                    setupMarkerInfo();
                    break;
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            Toasty.error(getApplicationContext(), masterPojo.getSomethingwentWrong(), Toast.LENGTH_SHORT, true).show();
        }
    }


    private void initViews() {

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        closeIcon = findViewById(R.id.closeIcon);
        play = findViewById(R.id.play);
        pause = findViewById(R.id.pause);
        linearPlay = findViewById(R.id.linearPlay);
        linearPause = findViewById(R.id.linearPause);
        pointAText = findViewById(R.id.pointAText);
        pointBText = findViewById(R.id.pointBText);
        bottomLayout = findViewById(R.id.bottomLayout);
        currentdate = findViewById(R.id.currentdate);
        currentlocation = findViewById(R.id.location);
        currenttravelTime = findViewById(R.id.currenttravelTime);
        speedometer = findViewById(R.id.speedView);
        distancepermile = findViewById(R.id.distancepermile);
        initspeedometer();
        currentspeed = findViewById(R.id.currentspeed);
        currentsummarylayout = findViewById(R.id.currentsummarylayout);
        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekChangeListener(seekBarChangeListener);
        controllerRR = findViewById(R.id.controlview);
        speedometerlayout = findViewById(R.id.speedometerlayout);
        currentdataLL = findViewById(R.id.itemLL);
        currentlocationRR = findViewById(R.id.locationRR);
        progressBar = findViewById(R.id.progressBar);
        summaryLayout = findViewById(R.id.totalsummary);
        travelTime = findViewById(R.id.travelTime);
        distance = findViewById(R.id.distance);
        speed = findViewById(R.id.speed);
        header_title = findViewById(R.id.header_title);
        masterPojo = (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA, MasterPojo.class);
        header_title.setText(masterPojo.getJourney());
        TextView pleasewait_text = findViewById(R.id.pleasewait_text);
        TextView message = findViewById(R.id.message);
        message.setText(masterPojo.getWearegettingyourtripdetails());
        pleasewait_text.setText(masterPojo.getPleasewait());
        play.setOnClickListener(this);
        closeIcon.setOnClickListener(this);
        pause.setOnClickListener(this);
        speed_spinner = (Spinner) findViewById(R.id.spinner);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void getData() throws ParseException {
        JourneyByVehicle model = (JourneyByVehicle) getIntent().getSerializableExtra("journeyByVehicle");
        pointAText.setText(model.getStartAddress());
        pointBText.setText(model.getEndAddress());
        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toasty.error(getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
            return;
        }
        callApi(model.getStartDate(), model.getEndDate(), model.getVehicleId(), model.getTripId(), model.getTripStartDate(), model.getTripEndDate());

    }

    private void callApi(String endDate, String startDate, String vehReg, String tripId, String startDateTime, String endDateTime) {

        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toasty.error(getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
            return;
        }
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient(APIV2).create(ApiInterface.class);
        JourneyHistory journeyHistory = new JourneyHistory(startDate, endDate, vehReg, tripId, Stash.getString(Constants.TIME_ZONE), Stash.getString(Constants.USER_CLIENT_ID), user.getUnit());
        Call<CustomWebResponse<JourneyHistory>> call = apiService.getJourneyHistoryDetails(journeyHistory, "Bearer " + user.getAccessToken());


        call.enqueue(new Callback<CustomWebResponse<JourneyHistory>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<JourneyHistory>> call, Response<CustomWebResponse<JourneyHistory>> response) {
                try {

                    if (response.code() != 200 || response.body() == null) {
                        Toasty.error(getApplicationContext(), response.body().getMessage());
                        progressVisiblityGone();
                        return;
                    }

                    if (!response.body().getSuccess()) {
                        progressVisiblityGone();
                        Toasty.error(getApplicationContext(), response.body().getMessage());
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        setUpRoutesMarker(response.body().getData());
                        progressVisiblityGone();
                    }
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<CustomWebResponse<JourneyHistory>> call, Throwable t) {
                Toasty.error(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                Log.e("Error", t.getMessage() + "");
                //  Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });


    }

    private void setUpRoutesMarker(ArrayList<JourneyHistory> response) {
        markersList = response;
        setupCarSpeed(response);


        for (int j = 0; j < markersList.size(); j++) {
            if (j == 0) {
                createMarker(markersList.get(j).getLat(), markersList.get(j).getLng(), markersList.get(j).getAngle(), markersList.get(j), true, false).setTag(markersList.get(j));
            } else {
                createMarker(markersList.get(j).getLat(), markersList.get(j).getLng(), markersList.get(j).getAngle(), markersList.get(j), false, false).setTag(markersList.get(j));
            }
            createMarker(markersList.get(j).getLat(), markersList.get(j).getLng(), markersList.get(j).getAngle(), markersList.get(j), false, false).setTag(markersList.get(j));
            if (j == markersList.size() - 1) {
                createMarker(markersList.get(j).getLat(), markersList.get(j).getLng(), markersList.get(j).getAngle(), markersList.get(j), false, true).setTag(markersList.get(j));
                LatLng coordinate = new LatLng(markersList.get(0).getLat(), markersList.get(0).getLng());
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        coordinate, 14);
                mMap.animateCamera(location);
            }
            LatLng latLng = new LatLng(markersList.get(j).getLat(), markersList.get(j).getLng());
            listOfPoints.add(latLng);
        }
        progressVisiblityGone();

    }

    private void playJourney() {

        playPauseVisiblity(true);
        isAlreadyPlaying = true;
        mMarker.setVisible(true);
        handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {

                if (isPause) {
                    return;
                }
                if (currentPt < listOfPoints.size()) {
                    //post again
                    controllerRR.setVisibility(View.VISIBLE);
                    speedometerlayout.setVisibility(View.VISIBLE);
                    Location targetLocation = new Location(LocationManager.GPS_PROVIDER);
                    targetLocation.setLatitude(listOfPoints.get(currentPt).latitude);
                    targetLocation.setLongitude(listOfPoints.get(currentPt).longitude);
                    animateMarkerNew(targetLocation, mMarker);
                    setupSpeedometre();
                    setupSeekbarProgress();
                    //   Log.e("ActivityJourneyTrack", "Speed " + Speed);
                    currentPt++;
                    handler.postDelayed(this, Speed);

                } else {
                    //removed callbacks
                    linearPause.setVisibility(View.GONE);
                    linearPlay.setVisibility(View.VISIBLE);
                    handler.removeCallbacks(this);
                    isPlaying = false;
                    currentPt = 0;
                    bottomLayout.setVisibility(View.VISIBLE);
                    totalSummary();
                }
            }
        }, Speed);
    }


    private void playPauseVisiblity(boolean b) {
        if (b) {
            linearPlay.setVisibility(View.GONE);
            linearPause.setVisibility(View.VISIBLE);
        } else {
            linearPlay.setVisibility(View.VISIBLE);
            linearPause.setVisibility(View.GONE);
        }
    }


    private void animateMarkerNew(final Location destination, final Marker marker) {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());
            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(Speed); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .zoom(15.5f)
                                .build()));

                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));

                    } catch (Exception e) {
                        Log.e(EXCEPTION_TAG, "" + e.getMessage());
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }

    }


    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);
        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }


    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }

    }


    protected Marker createMarker(double latitude, double longitude, int angle, JourneyHistory journeyHistory, boolean startPoint, boolean endPoint) {
        if (startPoint) {
            icon = BitmapDescriptorFactory.fromResource(R.drawable.pointa);
            return mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .icon(icon).zIndex(0.4f));
        } else if (!startPoint && endPoint) {
            icon = BitmapDescriptorFactory.fromResource(R.drawable.pointb);
            return mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .anchor(0.5f, 0.5f)
                    .icon(icon).zIndex(0.4f));
        } else {
            icon = BitmapDescriptorFactory.fromResource(R.drawable.icon_marker);
        }

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f).rotation(angle)
                .icon(icon));
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        play.setClickable(false);
        play.setEnabled(false);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        play.setClickable(true);
        play.setEnabled(true);
    }


   /* public String convertDateFormat(String date) throws ParseException {


        Date myDate = new Date();
        System.out.println(myDate);

        SimpleDateFormat mdyFormat = new SimpleDateFormat("MMMM dd yyyy  hh:mm:ss aa");
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");

        // Format the date to Strings
        String mdy = mdyFormat.format(myDate);
        String dmy = dmyFormat.format(myDate);


        return dmy;
    }


    public String converTimeFormat(String date) throws ParseException {


        Date myDate = new Date();
        System.out.println(myDate);

        SimpleDateFormat mdyFormat = new SimpleDateFormat("MMMM dd yyyy  hh:mm:ss aa");
        SimpleDateFormat dmyFormat = new SimpleDateFormat("hh:mm aa");

        // Format the date to Strings
        String mdy = mdyFormat.format(myDate);
        String dmy = dmyFormat.format(myDate);


        return dmy;
    }*/

    private void callApiOkHttp(JourneyHistory journeyHistory, Boolean state) {

        try {
            if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
                progressVisiblityGone();
                progressgone();
                Toasty.error(getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
                return;
            }

            if (journeyHistory == null) {
                progressVisiblityGone();
                progressgone();
                return;
            }


            if (journeyHistory.getLng() == null || journeyHistory.getLat() == null) {
                progressgone();
                return;
            }

            OkHttpClient client = new OkHttpClient();
            String openstreeturl = Stash.getString(GET_OPENSTREET_URL);
            String url = openstreeturl + "/nominatim/reverse.php?lat=" + "" + journeyHistory.getLat() + "" + "&lon=" + journeyHistory.getLng() + "&zoom=19&format=jsonv2";
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                progressgone();
                                progressVisiblityGone();
                                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();

                            } catch (Exception ew) {
                                Log.e(EXCEPTION_TAG, "" + ew.getMessage());
                                progressVisiblityGone();
                                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                            }
                        }
                    });
                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {

                    final String myResponse = response.body().string();

                    ActivityJourneyTrack.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (state) {
                                    JSONObject jsonObject = new JSONObject(myResponse);
                                    Log.e("ActivityJourneyTrack", "jsonobject" + jsonObject.toString());
                                    String displayName = jsonObject.getString("display_name");
                                    progressVisiblityGone();
                                    alertDialoge.showDialog(ActivityJourneyTrack.this, journeyHistory, displayName);
                                } else {
                                    JSONObject jsonObject = new JSONObject(myResponse);
                                    Log.e("ActivityJourneyTrack", "jsonobject" + jsonObject.toString());
                                    //  setEndPoint(jsonObject, journeyHistory);
                                }

                            } catch (Exception e) {
                                Log.e(EXCEPTION_TAG, "" + e.getMessage());
                                Toasty.error(getApplicationContext(), masterPojo.getSomethingwentWrong(), Toast.LENGTH_SHORT, true).show();
                                progressgone();
                            }
                        }
                    });


                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            Toasty.error(getApplicationContext(), masterPojo.getSomethingwentWrong(), Toast.LENGTH_SHORT, true).show();
            progressgone();
        }
    }


    public String convertDateFormat(String date, String formatter) throws ParseException {
        Log.e("ActivityJourneyTrack", "date" + date);
        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMATFROMBACKEND_ACTIVITYTRACK);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat(formatter);
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }


    public String converTimeFormat(String date, String formatter) throws ParseException {
        Log.e("ActivityJourneyTrack", "date" + date);
        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMATFROMBACKEND_ACTIVITYTRACK);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat(formatter);
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }


    public class ViewDialog {

        public void showDialog(Activity activity, JourneyHistory journeyHistory, String stopNameee) throws ParseException {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_stops);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView vehicleNo = (TextView) dialog.findViewById(R.id.vehicleNo);
            TextView time = (TextView) dialog.findViewById(R.id.time);
            TextView stopNamee = (TextView) dialog.findViewById(R.id.stopNameTv);
            Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
            TextView dateTime = (TextView) dialog.findViewById(R.id.dateTime);
            TextView heading = (TextView) dialog.findViewById(R.id.heading);
            TextView vechile_id_title = (TextView) dialog.findViewById(R.id.vechile_id_title);
            TextView date_title = (TextView) dialog.findViewById(R.id.date_title);
            TextView time_title = (TextView) dialog.findViewById(R.id.time_title);
            stopNamee.setText(stopNameee);
            vehicleNo.setText(journeyHistory.getVehicle());
            vehicleNo.setVisibility(View.GONE);
            vechile_id_title.setVisibility(View.GONE);
            Log.e("ActivityJourneyTrack", "journeyHistory.getTimeStamp()" + journeyHistory.getTimeStamp());
            dateTime.setText(convertDateFormat(journeyHistory.getTimeStamp(), "MMMM dd yyyy") + "");
            time.setText(converTimeFormat(journeyHistory.getTimeStamp(), "hh:mm:ss aa") + "");
            heading.setText(masterPojo.getDetails());
            vechile_id_title.setText(masterPojo.getVehicleID());
            date_title.setText(masterPojo.getDate());
            time_title.setText(masterPojo.getTime());
            btnClose.setText(masterPojo.getCLOSE());

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    public void setupCarSpeed(ArrayList<JourneyHistory> response) {
        Log.e("ActivityJourneyTrack", "response.size() " + response.size());
        try {
            noofpoint = response.size();
            totalpoint = response.size();
            seekBar.setMax(totalpoint);
            if (noofpoint > seconds) {
                noofpoint = 100 * noofpoint * 2 / seconds;
            } else {
                noofpoint = 10 * seconds;
            }
            Log.e("ActivityJourneyTrack", "Default Speed " + noofpoint);
            setupSpinner();
            speed_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            Speed = (int) noofpoint;
                            break;
                        case 1:
                            Speed = (int) (noofpoint / 2);
                            break;
                        case 2:
                            Speed = (int) (noofpoint / 3);
                            break;
                        case 3:
                            Speed = (int) (noofpoint / 4);
                            break;
                        case 4:
                            Speed = (int) (noofpoint / 5);
                            break;
                    }

                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }

    }

    @Override
    public void onDestroy() {
        try {
            if (handler != null) {
                handler.removeCallbacks(runnable);
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            progressgone();
        }
        super.onDestroy();
    }

    public void setupSpinner() {
        speed_spinner.setVisibility(View.VISIBLE);
        String[] Filter = new String[]{"1x", "2x", "3x", "4x", "5x"};
        speed_spinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, Filter);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        speed_spinner.setAdapter(adapter);
        speed_spinner.setSelection(1);
    }

    private void setEndPoint(String locationname, JourneyHistory journeyHistory) {
        try {
            String location = locationname;
            String speed = String.valueOf(journeyHistory.getSpeed());
            Log.e("ActivityJourneyTrack", "journeyHistory.getTimeStamp()" + journeyHistory.getTimeStamp());
            String date = convertDateFormat(journeyHistory.getTimeStamp(), "dd-MM-YYYY") + "";
            String time = converTimeFormat(journeyHistory.getTimeStamp(), "hh:mm aa") + "";
            Log.e("ActivityJourneyTrack", "location" + location);
            Log.e("ActivityJourneyTrack", "currentPt" + currentPt);
            progressBar.setVisibility(View.GONE);
            currentlocationRR.setVisibility(View.VISIBLE);
            currentdataLL.setVisibility(View.VISIBLE);
            currentlocation.setText(location);
            currentspeed.setText(speed + "");
            currentdate.setText(date);
            currenttravelTime.setText(time);
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }
    }

    private void totalSummary() {
        speedometerlayout.setVisibility(View.GONE);
        summaryLayout.setVisibility(View.VISIBLE);
        controllerRR.setVisibility(View.GONE);
        JourneyByVehicle model = (JourneyByVehicle) getIntent().getSerializableExtra("journeyByVehicle");
        pointBText.setText(model.getEndAddress());
        Journey journey = (Journey) Stash.getObject(Constants.JOURNEY_DATA, Journey.class);
        if (journey.getTripDurationHr() <= 0) {
            travelTime.setText("" + journey.getTripDurationMins() + masterPojo.getMins());
        } else {
            travelTime.setText("" + journey.getTripDurationHr() + masterPojo.getHr() + journey.getTripDurationMins() + masterPojo.getMins());
        }
        distance.setText(journey.getTotalDistance());
        if (journey.getAverageSpeed().contains("/ Hour")) {
            String[] speedstring = journey.getAverageSpeed().split("/ Hour");
            speed.setText(speedstring[0]);
        } else {
            speed.setText(journey.getAverageSpeed());
        }
    }

    private void progressLoader() {
        currentsummarylayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        currentlocationRR.setVisibility(View.GONE);
        currentdataLL.setVisibility(View.GONE);
    }

    private void setupMarkerInfo() {
        try {
            progressLoader();
            controllerRR.setVisibility(View.VISIBLE);
            speedometerlayout.setVisibility(View.GONE);
            bottomLayout.setVisibility(View.GONE);
            JourneyHistory journeyHistory = (JourneyHistory) markersList.get(currentPt);
            callApiForDisplayName(journeyHistory);
            //    callApiOkHttp(journeyHistory, false);
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            Toasty.error(getApplicationContext(), masterPojo.getSomethingwentWrong(), Toast.LENGTH_SHORT, true).show();
            progressgone();
            progressVisiblityGone();
        }

    }

    public void progressgone() {
        currentsummarylayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        currentlocationRR.setVisibility(View.VISIBLE);
        currentdataLL.setVisibility(View.VISIBLE);
    }

    OnSeekChangeListener seekBarChangeListener = new OnSeekChangeListener() {
        @Override
        public void onSeeking(SeekParams seekParams) {
            currentPt = seekParams.progress;
            setupRouteMarker();
            Log.e("ActivityJourneyTrack", "progress" + seekParams.progress);
        }

        @Override
        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            if (!isPlaying) {
                if (seekBar.getProgress() == totalpoint) {
                    linearPause.setVisibility(View.GONE);
                    linearPlay.setVisibility(View.VISIBLE);
                    isPlaying = false;
                    currentPt = 0;
                    bottomLayout.setVisibility(View.VISIBLE);
                    totalSummary();
                    currentsummarylayout.setVisibility(View.GONE);
                } else {
                    setupMarkerInfo();
                }
            }
        }
    };

    public void setupRouteMarker() {
        try {
            Location targetLocation = new Location(LocationManager.GPS_PROVIDER);
            targetLocation.setLatitude(listOfPoints.get(currentPt).latitude);
            targetLocation.setLongitude(listOfPoints.get(currentPt).longitude);
            animateMarkerNewWithSeekbar(targetLocation, mMarker);
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }

    }

    private void animateMarkerNewWithSeekbar(final Location destination, final Marker marker) {

        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());
            final float startRotation = marker.getRotation();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(500); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .zoom(15.5f)
                                .build()));

                        marker.setRotation(getBearing(startPosition, new LatLng(destination.getLatitude(), destination.getLongitude())));
                    } catch (Exception e) {
                        Log.e(EXCEPTION_TAG, "" + e.getMessage());
                        //I don't care atm..
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                }
            });
            valueAnimator.start();
        }
    }

    private void setupSpeedometre() {
        try {
            String speed=""+markersList.get(currentPt).getSpeed();
            String[] separated = speed.split(" ");
            float speedbreak= Integer.parseInt(separated[0]);
            speedometer.speedTo(speedbreak);
            speedometer.setUnit(""+separated[1]);
            distancepermile.setText(markersList.get(currentPt).getDistanceCovered() + "");
        }
        catch (Exception e){

        }

    }

    private void setupSeekbarProgress() {
        try {
            new Thread(new Runnable() {
                public void run() {
                    seekBar.setProgress(currentPt);
                }
            }).start();
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "seekBar" + e.getMessage());
        }
    }

    public void initspeedometer() {
        Typeface font1 = Typeface.createFromAsset(getAssets(), "poppins_light.ttf");
        speedometer.setSpeedTextTypeface(font1);
/*      speedometer.getSections().get(0).setColor(getResources().getColor(R.color.grey));
       speedometer.getSections().get(1).setColor(getResources().getColor(R.color.medium_grey));
       speedometer.getSections().get(2).setColor(getResources().getColor(R.color.dark_grey));*/
    }

    private void callApiForDisplayName(JourneyHistory journeyHistory) {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!AppStatus.getInstance(ActivityJourneyTrack.this).isOnline()) {
                        progressgone();
                        Toast.makeText(ActivityJourneyTrack.this, masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_LONG).show();
                    }
                    if (journeyHistory == null) {
                        progressgone();
                        return;
                    }


                    if (journeyHistory.getLat() == null || journeyHistory.getLng() == null) {
                        progressgone();
                        return;
                    }
                    try {
                        ApiInterface apiService = APIClient.getClient("").create(ApiInterface.class);
                        LocationResponseModel locationResponseModel = new LocationResponseModel();
                        locationResponseModel.setLatitude(journeyHistory.getLat());
                        locationResponseModel.setLongitude(journeyHistory.getLng());
                        retrofit2.Call<WebResponseString> call = apiService.getlocationaddress(locationResponseModel, "Bearer " + user.getAccessToken());
                        call.enqueue(new retrofit2.Callback<WebResponseString>() {
                            @Override
                            public void onResponse(retrofit2.Call<WebResponseString> call, retrofit2.Response<WebResponseString> response) {
                                try {
                                    WebResponseString webResponse = response.body();
                                    Boolean sucess = webResponse.getSuccess();
                                    String message = webResponse.getMessage();
                                    if (sucess) {
                                      //  progressgone();
                                        setEndPoint(webResponse.getData(), journeyHistory);
                                    } else {
                                        progressgone();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(ActivityJourneyTrack.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                    progressgone();
                                    Log.e(EXCEPTION_TAG, e.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<WebResponseString> call, Throwable t) {
                                Toast.makeText(ActivityJourneyTrack.this, t.getMessage(), Toast.LENGTH_LONG).show();
                                progressgone();
                                call.cancel();
                            }
                        });
                    } catch (Exception e) {
                        Toast.makeText(ActivityJourneyTrack.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        progressgone();
                        Log.e(EXCEPTION_TAG, e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            progressgone();
        }
    }
}


