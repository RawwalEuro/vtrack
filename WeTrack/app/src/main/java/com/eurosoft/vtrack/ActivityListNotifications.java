package com.eurosoft.vtrack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eurosoft.vtrack.Adapters.AdapterNotifications;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.Notifications;
import com.fxn.stash.Stash;

import java.util.List;

public class ActivityListNotifications extends AppCompatActivity {

    private RecyclerView rvEvents;
    private LinearLayoutManager linearLayoutManager;
    private AdapterNotifications adapterNotifications;
    private List<Notifications> notificationsArrayList;
    private ImageView backIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_notifications);
        Stash.init(getApplicationContext());
        notificationsArrayList = Stash.getArrayList(Constants.LIST_NOTIFICATION, Notifications.class);
        initViews();
    }

    private void initViews() {
        rvEvents = findViewById(R.id.rvEvents);
        backIcon = findViewById(R.id.backIcon);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvEvents.setLayoutManager(linearLayoutManager);
        adapterNotifications = new AdapterNotifications(this.getApplicationContext());
        rvEvents.setAdapter(adapterNotifications);
        MasterPojo masterPojo= (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        TextView header_title=findViewById(R.id.header_title);
        header_title.setText(masterPojo.getEvent()+" ("+notificationsArrayList.size()+")");
        TextView message=(TextView) findViewById(R.id.message);
        message.setText(masterPojo.getPleasewaitwearegettingyourevents());
        adapterNotifications.addAll(notificationsArrayList);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}