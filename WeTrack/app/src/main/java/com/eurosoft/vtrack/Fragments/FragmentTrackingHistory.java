package com.eurosoft.vtrack.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.ActivityListJourneyByDate;
import com.eurosoft.vtrack.Adapters.AdapterVehicles;
import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.Interfaces.onTripEventListener;
import com.eurosoft.vtrack.Models.Journey;
import com.eurosoft.vtrack.Models.JourneyByVehicle;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.R;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.vtrack.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.vtrack.Constants.APIV2;
import static com.eurosoft.vtrack.Constants.DATETIME_FORMAT_FROMBACKEND;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;

public class FragmentTrackingHistory extends Fragment implements View.OnClickListener {

    private List<Vehicles> vehiclesList;
    private RecyclerView rvVehicles;
    private LinearLayoutManager layoutManager;
    private AdapterVehicles mAdapter;
    private Calendar calendar;
    private DatePickerDialog dialog;
    private boolean isFrom = false;
    private TextView tilldate, fromdate;
    private LinearLayout llTilDate, fromDateLL;
    private onTripEventListener onTripEventListener;
    private String dateTill, dateFrom;
    private int pos = 0;
    private Button btnFilter;
    private TextView from, to;
    public FilterDialog filterDialog;
    private LoginResponse user;
    private ArrayList<Journey> journeyList = new ArrayList<>();
    private LinearLayout llNoData, llFilter;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private CircularProgressBar circularProgressBarFilter;
    private LinearLayout llProgress;
    private ImageView filter;
    private DecimalFormat f;
    private String fetchedUrl = "";
    private ImageView bckIcon;
    private String currentTime = "";
    private String clientId;
    private boolean isCustomSelected = false;
    private MasterPojo masterPojo;
    private ImageView navIcon;
    public DrawerLayout mDrawer;
    private RelativeLayout toolbarHead;
    Calendar calendarfromdate, calendertilldate;
    Dialog datePickerDialogfromdate, datePickerDialogtilldate;
    DatePicker datePickertilldate;
    int hrsFrom = 00, minsFrom = 00, hrsTill = 23, minsTill = 59;
    Boolean isDialogOpen=false;

    public static FragmentTrackingHistory newInstance() {
        return new FragmentTrackingHistory();
    }

    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tracking_history, container, false);

        initViews(view);
        getVehicles();
        Stash.init(getActivity());
        fetchedUrl = (String) Stash.getString(Constants.URL_FETCHED);
        clientId = Stash.getString(Constants.USER_CLIENT_ID);
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        getCalendarInstance();
        filterDialog = new FilterDialog();
        f = new DecimalFormat("00");
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onTripEventListener = (onTripEventListener) activity;
        } catch (ClassCastException e) {
            Log.e(EXCEPTION_TAG,""+e.getMessage());
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void initViews(View view) {
        masterPojo= (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        rvVehicles = view.findViewById(R.id.rvVehicles);
        btnFilter = view.findViewById(R.id.btnFilter);
        rlProgressBar = view.findViewById(R.id.rlProgressBar);
        circularProgressBar = view.findViewById(R.id.circularProgressBar);

        mDrawer = (DrawerLayout) view.findViewById(R.id.drawer);
        toolbarHead=view.findViewById(R.id.toolbarHead);
        toolbarHead.setVisibility(View.GONE);

        /*filter = view.findViewById(R.id.filter);
        bckIcon = view.findViewById(R.id.bckIcon);
        btnFilter.setOnClickListener(this);
          filter.setOnClickListener(this);
        bckIcon.setOnClickListener(this);
        bckIcon.setVisibility(View.GONE);
        navIcon = (ImageView) view.findViewById(R.id.navIcon);
        navIcon.setVisibility(View.GONE);
        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toasty.error(getActivity(), masterPojo.getNotworking(), Toasty.LENGTH_LONG).show();
            }
        });*/

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getVehicles();
        } else {
        }
    }


    private void getVehicles() {
        class GetVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                vehiclesList = DatabaseClient
                        .getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .getAll();
                return vehiclesList;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                if (vehiclesList == null || vehiclesList.size() == 0) {
                    return;
                } else {
                    setUpRecycler(vehiclesList);
                }
                Log.e("dfdfdf",""+vehiclesList.get(0).getVehicleId());
            }

        }
        GetVehiclesList gt = new GetVehiclesList();
        gt.execute();
    }

    private void setUpRecycler(List<Vehicles> vehiclesList) {
        mAdapter = new AdapterVehicles(getActivity(), vehiclesList, new AdapterVehicles.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {
                    case R.id.rlBg:
                        if(!isDialogOpen) {
                            pos = position;
                            isDialogOpen=true;
                            if (pos == -1) {
                                Toasty.error(getActivity(), masterPojo.getSelectvehicletofilter(), Toasty.LENGTH_SHORT).show();
                                isDialogOpen=false;
                                return;
                            }
                            try {
                                isCustomSelected = false;
                                filterDialog.showDialog(getActivity());
                            } catch (ParseException e) {
                                Log.e(EXCEPTION_TAG, "" + e.getMessage());
                                isDialogOpen=false;
                            }
                        }
                        break;

                }
            }
        },masterPojo);
        rvVehicles.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rvVehicles.setLayoutManager(layoutManager);
        rvVehicles.setAdapter(mAdapter);
    }


  /*  private void validate() {
        String tillDate = tilldate.getText().toString();
        String fromDate = fromdate.getText().toString();
        if (tillDate.equalsIgnoreCase("") || tillDate.equalsIgnoreCase("Till")) {
            Toasty.error(getActivity(), "You haven't selected till date", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (fromDate.equalsIgnoreCase("") || fromDate.equalsIgnoreCase("From")) {
            Toasty.error(getActivity(), "You haven't selected from date", Toasty.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(getActivity(), ActivityListJourneyByDate.class);
        intent.putExtra(Constants.STARTED_DATE, dateTill);
        intent.putExtra(Constants.END_DATE, dateFrom);
        intent.putExtra(Constants.VEH_REG, vehiclesList.get(pos).getVehicleReg());
        startActivity(intent);
    }


    private void getCalendarInstance() {
        calendar = Calendar.getInstance(TimeZone.getDefault());

        dialog = new DatePickerDialog(getActivity(), datePickerListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(new Date().getTime());

    }*/





/*
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            selectedMonth = selectedMonth + 1;
            String year = String.valueOf(selectedYear);
            //int seletdMonth = (selectedMonth<10? Integer.parseInt(("0"+selectedMonth)) :(selectedMonth));
            String seletdMonth = f.format(selectedMonth);
            String month = seletdMonth;
            String day = (f.format(selectedDay));
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);


            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            if (isFrom) {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateTill = year + "-" + month + "-" + day + "T" + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataIsFrom(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));


                            } else {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateFrom = year + "-" + month + "-" + day + "T" + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataTo(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));

                            }
                        }
                    }, hour, minute, true);
            if (!isFrom) {
                timePickerDialog.updateTime(00, 00);
            }
            timePickerDialog.show();
        }
    };
*/


    public String convertIntoTwo(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

   /* private void setDataTo(String year, String month, String day, String time) {
        //fromdate.setText(day + "-" + month + "-" + year);
        from.setText(day + "-" + month + "-" + year + " " + time);
    }

    private void setDataIsFrom(String year, String month, String day, String time) {
        //tilldate.setText(day + "-" + month + "-" + year);
        to.setText(day + "-" + month + "-" + year + " " + time);
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    public String formatMonth(String month) throws ParseException {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        return monthDisplay.format(monthParse.parse(month));
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter:
                //validate();
                try {
                    filterDialog.showDialog(getActivity());
                } catch (ParseException e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }
                break;
        }
    }


    private void callApi(String startDate, String endDate, String vehReg, Dialog dialogAlert) {

        llProgress.setVisibility(View.VISIBLE);
        llFilter.setVisibility(View.GONE);
        llNoData.setVisibility(View.GONE);
        circularProgressBarFilter.setIndeterminateMode(true);
        Log.e("FragmentTrackingHistory", "fetchedUrl"+fetchedUrl);
        ApiInterface apiService = APIClient.getClient(APIV2).create(ApiInterface.class);
        Journey journey = new Journey(endDate, startDate, vehReg, user.getUnit(), clientId, Stash.getString(Constants.TIME_ZONE));
        Call<CustomWebResponse<Journey>> call = apiService.getJourniesByDateAndVehReg(journey, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Journey>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Journey>> call, Response<CustomWebResponse<Journey>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        llFilter.setVisibility(View.GONE);
                        llNoData.setVisibility(View.VISIBLE);
                        llProgress.setVisibility(View.GONE);
                        progressVisiblityGone();
                        return;
                    }
                    if (!response.body().getSuccess()) {
                        llFilter.setVisibility(View.GONE);
                        llNoData.setVisibility(View.VISIBLE);
                        llProgress.setVisibility(View.GONE);
                        progressVisiblityGone();
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        journeyList = response.body().getData();
                        if (journeyList.size() == 0 || journeyList == null) {
                            llFilter.setVisibility(View.GONE);
                            llNoData.setVisibility(View.VISIBLE);
                            llProgress.setVisibility(View.GONE);
                            progressVisiblityGone();
                        } else {
                            progressVisiblityGone();
                            JourneyByVehicle journeyByVehicle = new JourneyByVehicle(vehReg, "Hybrid", journeyList);
                            journeyByVehicle.setStartDate(dateFrom);
                            journeyByVehicle.setEndDate(dateTill);
                            Intent i = new Intent(getActivity(), ActivityListJourneyByDate.class);
                            i.putExtra("journeyByVehicle", journeyByVehicle);
                            startActivity(i);
                            dialogAlert.dismiss();
                        }
                    }
                }
                catch (Exception e){
                    progressVisiblityGone();
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    Toasty.error(getActivity(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<Journey>> call, Throwable t) {
                Log.e(EXCEPTION_TAG, t.getLocalizedMessage());
                llFilter.setVisibility(View.GONE);
                llNoData.setVisibility(View.VISIBLE);
                Toasty.error(getActivity(), t.getLocalizedMessage() + "", Toast.LENGTH_SHORT, true).show();
                llProgress.setVisibility(View.GONE);
                progressVisiblityGone();
            }
        });

    }


    public class FilterDialog {

        public void showDialog(Activity activity) throws ParseException {
            final Dialog dialogFilter = new Dialog(activity, R.style.Theme_Dialog);
            dialogFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogFilter.setCancelable(false);
            dialogFilter.setContentView(R.layout.dialog_filter);
            dialogFilter.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            from = (TextView) dialogFilter.findViewById(R.id.from);
            to = (TextView) dialogFilter.findViewById(R.id.to);
            from.setHint(masterPojo.getSelectDateandTime());
            to.setHint(masterPojo.getSelectDateandTime());
            Button btnFilter = (Button) dialogFilter.findViewById(R.id.btnFilter);
            Button btnCancel = (Button) dialogFilter.findViewById(R.id.btnCancel);
            Button btnOk = (Button) dialogFilter.findViewById(R.id.btnOk);
            Button cancel = (Button) dialogFilter.findViewById(R.id.cancel);
            llFilter = (LinearLayout) dialogFilter.findViewById(R.id.llFilter);
            llNoData = (LinearLayout) dialogFilter.findViewById(R.id.llNoData);
            llProgress = (LinearLayout) dialogFilter.findViewById(R.id.llProgress);
            circularProgressBarFilter = (CircularProgressBar) dialogFilter.findViewById(R.id.circularProgressBar);
            RelativeLayout rlFrom = (RelativeLayout) dialogFilter.findViewById(R.id.rlFrom);
            RelativeLayout rlTo = (RelativeLayout) dialogFilter.findViewById(R.id.rlTo);
            setupDateView(rlFrom,rlTo);
            RadioButton today=(RadioButton) dialogFilter.findViewById(R.id.todayRB);
            RadioButton  custom=(RadioButton) dialogFilter.findViewById(R.id.customRB);
            RadioButton  last7day=(RadioButton) dialogFilter.findViewById(R.id.weeklyRB);
            RadioButton  last48hour=(RadioButton) dialogFilter.findViewById(R.id.last48RB);
            TextView  totitle = (TextView) dialogFilter.findViewById(R.id.to_title);
            TextView  fromtitle  = (TextView) dialogFilter.findViewById(R.id.from_title);
            TextView sorry_title=(TextView) dialogFilter.findViewById(R.id.sorry_title);
            TextView message=(TextView) dialogFilter.findViewById(R.id.message);
            message.setText(masterPojo.getPleasewaitwearefetchingyourtrips());
            today.setText(masterPojo.getToday());
            custom.setText(masterPojo.getCustom());
            last7day.setText(masterPojo.getLast7Days());
            last48hour.setText(masterPojo.getLast48Hours());
            fromtitle.setText(masterPojo.getFrom());
            totitle.setText(masterPojo.getTo());
            btnFilter.setText(masterPojo.getSearch());
            btnCancel.setText(masterPojo.getCancel());
            sorry_title.setText(masterPojo.getSorry());
            btnOk.setText(masterPojo.getOK());
            cancel.setText(masterPojo.getCancel());
            RadioGroup radioGroup = (RadioGroup) dialogFilter.findViewById(R.id.radioGroup);
            LinearLayout layoutCustom = (LinearLayout) dialogFilter.findViewById(R.id.layoutCustom);
            todaySetup(layoutCustom, 0, "today");
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (i) {
                        case R.id.todayRB:
                            todaySetup(layoutCustom, 0, "today");
                            isCustomSelected = false;
                            break;

                        case R.id.last48RB:
                            todaySetup(layoutCustom, -2, "last48RB");
                            isCustomSelected = false;
                            break;

                        case R.id.weeklyRB:
                            todaySetup(layoutCustom, -7, "last48RB");
                            isCustomSelected = false;
                            break;

                        case R.id.customRB:
                            layoutCustom.setVisibility(View.VISIBLE);
                            isCustomSelected = true;
                            break;
                    }
                }
            });




/*            rlFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isFrom = false;
                    dialog.show();
                }
            });


            rlTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isFrom = true;
                    dialog.show();
                }
            });*/


            btnFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isCustomSelected == true) {

                        String fromText = from.getText().toString();
                        String toText = to.getText().toString();

                        if (fromText.equalsIgnoreCase("")) {
                            Toasty.error(getActivity(), masterPojo.getYouhaventselectedfromdate(), Toasty.LENGTH_SHORT).show();
                            return;
                        }

                        if (toText.equalsIgnoreCase("")) {
                            Toasty.error(getActivity(), masterPojo.getYouhaventselectedfromdate(), Toasty.LENGTH_SHORT).show();
                            return;
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT_FROMBACKEND);
                        Date date1 = null;
                        Date date2 = null;
                        try {
                            Log.e("FragmentTrackingHistory","dateTill"+dateTill);
                            Log.e("FragmentTrackingHistory","dateFrom"+dateFrom);
                            date1 = sdf.parse(dateFrom);
                            date2 = sdf.parse(dateTill);
                            if (sdf.format(date2).compareTo(sdf.format(date1)) < 0) {
                                Toasty.error(getActivity(), masterPojo.getFromdatecantbegreaterthantodate(), Toasty.LENGTH_SHORT).show();
                                return;
                            }
                        } catch (ParseException e) {
                            Log.e(EXCEPTION_TAG,""+e.getMessage());
                            Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }

                    }
                    callApi(dateTill, dateFrom, vehiclesList.get(pos).getVehicleReg(), dialogFilter);
                    isDialogOpen=false;
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogFilter.dismiss();
                    isDialogOpen=false;

                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogFilter.dismiss();
                    isDialogOpen=false;

                }
            });

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogFilter.dismiss();
                    isDialogOpen=false;

                }
            });
            dialogFilter.show();
        }

        private void todaySetup(LinearLayout layoutCustom, int i2, String today) {
            layoutCustom.setVisibility(View.GONE);
            getCalculatedDate("yyyy-MM-dd", i2);
            currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            dateFrom = getCalculatedDate("yyyy-MM-dd", i2) + "T00:00:00Z";
            dateTill = getCalculatedDate("yyyy-MM-dd", 0) + "T" + currentTime + "Z";
            Log.e("FragmentTrackingHistory","dateFrom"+ dateFrom);
            Log.e("FragmentTrackingHistory", "dateTill"+dateTill);
            Log.e("FragmentTrackingHistory","dateFrom"+ dateFrom);
        }
    }

    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }


    public void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        rlProgressBar.bringToFront();
        circularProgressBar.invalidate();
    }


    public void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }


    private void setupDateView(RelativeLayout rlFrom,RelativeLayout rlTo) {
        getCalendarInstance();
        rlFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFrom = false;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);
            }
        });

        rlTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isFrom = true;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);
            }
        });

        initDatePickerFromDateDialog();
        initDatePickerTillDateDialog();
    }



    private void setFromDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);

        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        dateFrom =selectedYear + "-" + monthString + "-" + dayString + "T" + time + ":00Z";
        Log.e("FragmentTrackingHistory","dateFrom"+dateFrom);
        calendarfromdate.set(selectedYear, selectedMonth, selectedDay);
        from.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogfromdate.dismiss();
    }

    private void setTillDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);
        String daytillString = String.valueOf(datePickertilldate.getDayOfMonth());


        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        if (daytillString.length() == 1) {
            daytillString = "0" + daytillString;
        }
        dateTill = datePickertilldate.getYear() + "-" + monthString + "-" + daytillString + "T" + time + ":00Z";
        Log.e("FragmentTrackingHistory","dateTill"+dateTill);
        calendertilldate.set(selectedYear, selectedMonth, selectedDay);
        to.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogtilldate.dismiss();

    }

    private void getCalendarInstance() {
        calendarfromdate = Calendar.getInstance(TimeZone.getDefault());
        calendertilldate = Calendar.getInstance(TimeZone.getDefault());
    }

    public void initDatePickerFromDateDialog() {
        datePickerDialogfromdate = new Dialog(getActivity());
        datePickerDialogfromdate.setContentView(R.layout.datepicker_dialog);
        DatePicker datePicker = datePickerDialogfromdate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogfromdate.findViewById(R.id.today);
        EditText hours = datePickerDialogfromdate.findViewById(R.id.hours);
        EditText mins = datePickerDialogfromdate.findViewById(R.id.mins);
        hours.setHint("00");
        mins.setHint("00");
        textView1.setText(masterPojo.getFrom());
        TextView message = datePickerDialogfromdate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        TextView Ok = datePickerDialogfromdate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogfromdate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        datePicker.init(calendarfromdate.get(Calendar.YEAR), calendarfromdate.get(Calendar.MONTH), calendarfromdate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsFrom > 23 || minsFrom > 60) {
                    Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }

                if (hours.getText().toString().equals("")) {
                    hrsFrom=00;
                }
                if( mins.getText().toString().equals("")){
                    minsFrom=00;

                }
                setFromDate(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear(), convertIntoTwo(hrsFrom) + ":" + convertIntoTwo(minsFrom));
                datePickerDialogfromdate.dismiss();

            }
        });
        datePicker.setMaxDate(new Date().getTime());

        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogfromdate.dismiss();
            }
        });

        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsFrom = Integer.parseInt(hours.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals("")){
                        hrsFrom = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());}

                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsFrom = Integer.parseInt(mins.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals("")) {
                        minsFrom = Integer.parseInt(mins.getText().toString());
                        mins.setText(mins.getText().toString());
                        mins.setSelection(mins.getText().length());
                    }

                 /*   if (hrsFrom > 23 || minsFrom > 60) {
                        Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                        return false;
                    }

                    if (hours.getText().toString().equals("")) {
                        hrsFrom=00;
                    }
                    if( mins.getText().toString().equals("")){
                        minsFrom=00;

                    }
                    setFromDate(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear(), convertIntoTwo(hrsFrom) + ":" + convertIntoTwo(minsFrom));
                    datePickerDialogfromdate.dismiss();*/

                    return true;
                }
                return false;
            }
        });
    }

    public void initDatePickerTillDateDialog() {
        datePickerDialogtilldate = new Dialog(getActivity());
        datePickerDialogtilldate.setContentView(R.layout.datepicker_dialog);
        datePickertilldate = datePickerDialogtilldate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogtilldate.findViewById(R.id.today);
        textView1.setText(masterPojo.getTillDate());
        EditText hours = datePickerDialogtilldate.findViewById(R.id.hours);
        TextView message = datePickerDialogtilldate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        EditText mins = datePickerDialogtilldate.findViewById(R.id.mins);
        updateDatePickerTillDate(0l,hours,mins);
        hours.setHint("23");
        mins.setHint("59");
        TextView Ok = datePickerDialogtilldate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogtilldate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogtilldate.dismiss();
            }
        });

        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsTill = Integer.parseInt(hours.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals(""))
                        hrsTill = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());
                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsTill = Integer.parseInt(mins.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals("")) {
                        minsTill = Integer.parseInt(mins.getText().toString());
                        mins.setText(mins.getText().toString());
                        mins.setSelection(mins.getText().length());
                    }


                  /*  if (hrsTill > 23 || minsTill > 60) {
                        Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                        return false;
                    }
                    if (hours.getText().toString().equals("") ) {
                        hrsTill=23;
                    }
                    if(mins.getText().toString().equals("")){
                        minsTill=59;
                    }
                    setTillDate(datePickertilldate.getDayOfMonth(), datePickertilldate.getMonth(), datePickertilldate.getYear(), convertIntoTwo(hrsTill) + ":" + convertIntoTwo(minsTill));
                    datePickerDialogtilldate.dismiss();*/

                    return true;
                }
                return false;
            }
        });

    }

    public void updateDatePickerTillDate(Long mindate,EditText hours,EditText mins) {
        datePickertilldate.init(calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsTill > 23 || minsTill > 60) {
                    Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }
                if (hours.getText().toString().equals("") ) {
                    hrsTill=23;
                }
                if(mins.getText().toString().equals("")){
                    minsTill=59;
                }
                setTillDate(datePickertilldate.getDayOfMonth(), datePickertilldate.getMonth(), datePickertilldate.getYear(), convertIntoTwo(hrsTill) + ":" + convertIntoTwo(minsTill));
                datePickerDialogtilldate.dismiss();

            }
        });
        datePickertilldate.setMaxDate(new Date().getTime());
        datePickertilldate.setMinDate(mindate);

    }

    public void tillDateValidation(){

    }
}
