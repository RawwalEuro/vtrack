package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.SerializedName;

public class MasterPojo {

    ////////////////////////////////////////// MainActivity //////////////////////////////////////////////////
    @SerializedName("LiveMap")
    private String LiveMap = "Live Map";
    @SerializedName("TrackingHistory")
    private String TrackingHistory = "Tracking History";
    @SerializedName("Events")
    private String Events = "Events";
    @SerializedName("LiveTracking")
    private String LiveTracking = "Live Tracking";
    @SerializedName("Version")
    private String Version="Version ";
    public String getVersion() {
        return Version;
    }

    ////////////////////////////////////////// ActivityReports //////////////////////////////////////////////////
    @SerializedName("Report")
    private String Report = "Report";
    @SerializedName("SaveFile")
    private String SaveFile = "Save File";
    @SerializedName("Pleasewait")
    private String Pleasewait = "Please wait .....";
    @SerializedName("Wearelookingforyourvehicledetails")
    private String Wearelookingforyourvehicledetails = "We are looking for your vehicle details";
    @SerializedName("Excel")
    private String Excel = "Excel";
    @SerializedName("PDF")
    private String PDF = "PDF";
    @SerializedName("ViewReports")
    private String ViewReports = "View Reports";
    @SerializedName("SelectDate")
    private String SelectDate = "Select Date";
    @SerializedName("Today")
    private String Today = "Today";
    @SerializedName("Last48Hours")
    private String Last48Hours = "Last 48 Hours";
    @SerializedName("Last7Days")
    private String Last7Days = "Last 7 Days";
    @SerializedName("Custom")
    private String Custom = "Custom";
    @SerializedName("IgnitionReport")
    private String IgnitionReport = "Ignition Report";
    @SerializedName("Wearegettingyourtripdetails")
    private String  Wearegettingyourtripdetails = " We are getting your trip details";
    @SerializedName("pointA")
    private String  pointA = "Start Trip";
    @SerializedName("pointB")
    private String  pointB = "End Trip";

    public String getPointA() {
        return pointA;
    }

    public String getPointB() {
        return pointB;
    }

    public String getWearegettingyourtripdetails() {
        return Wearegettingyourtripdetails;
    }

    ////////////////////////////////////////// ActivityLogin //////////////////////////////////////////////////
    @SerializedName("Usernameisempty")
    private String Usernameisempty = "User name is empty";
    @SerializedName("Passwordisempty")
    private String Passwordisempty = "Password is empty";
    @SerializedName("Pleasecheckyourinternet")
    private String Pleasecheckyourinternet = "Please check your internet!!!!";
    @SerializedName("InvalidUserNameorpassword")
    private String InvalidUserNameorpassword = "Invalid UserName or password";
    @SerializedName("LoginSuccessfully")
    private String LoginSuccessfully = "Login Successfully";
    @SerializedName("Emailisempty")
    private String Emailisempty = "Email is empty";
    @SerializedName("Notavalidemail")
    private String Notavalidemail = "Not a valid email";
    @SerializedName("SignIn")
    private String SignIn = "Sign In";
    @SerializedName("Welcome")
    private String Welcome = "Welcome";
    @SerializedName("Signintocontinue")
    private String Signintocontinue = "Sign in to continue";
    @SerializedName("UserName")
    private String UserName = "User Name";
    @SerializedName("Password")
    private String Password = "Password";
    ////////////////////////////////////////// ActivityListJourneyByDate //////////////////////////////////////////////////
    @SerializedName("Trips")
    private String Trips = "Trips";
    @SerializedName("Trip")
    private String Trip = "Trip #";
    @SerializedName("Totaltraveltimeis")
    private String Totaltraveltimeis  = "Total travel time is ";
    @SerializedName("mins")
    private String mins = " mins";
    @SerializedName("Hr")
    private String Hr = " Hr ";
    @SerializedName("Distance")
    private String Distance = "Distance";
    @SerializedName("AvgSpeed")
    private String AvgSpeed = "Avg. Speed";
    ////////////////////////////////////////// ActivityJourneyTrack //////////////////////////////////////////////////
    @SerializedName("Journey")
    private String Journey = "Journey";
    @SerializedName("Details")
    private String Details = "Details";
    @SerializedName("VehicleID")
    private String VehicleID = "Vehicle ID :";
    @SerializedName("Date")
    private String Date = "Date :";
    @SerializedName("Time")
    private String Time = "Time :";
    @SerializedName("CLOSE")
    private String CLOSE = "CLOSE";
    ////////////////////////////////////////// FragmentLeftMenu //////////////////////////////////////////////////
    @SerializedName("Logout")
    private String Logout = "Logout";
    @SerializedName("Reports")
    private String Reports = "Reports";
    @SerializedName("Notifications")
    private String Notifications = "Notifications";
    @SerializedName("TripHistory")
    private String TripHistory = "Trip History";
    @SerializedName("Dashboard")
    private String Dashboard = "Dashboard";
    @SerializedName("Areyousure")
    private String Areyousure = "Are you sure?";
    @SerializedName("Youwanttologout")
    private String Youwanttologout = "You want to logout";
    @SerializedName("Yes")
    private String Yes = "Yes";
    @SerializedName("LoggedOut")
    private String LoggedOut = "Logged Out!";
    @SerializedName("No")
    private String No = "No";
    @SerializedName("Share")
    private String Share = "Share";
    ////////////////////////////////////////// ActivityTrackHistory //////////////////////////////////////////////////
    @SerializedName("Selectvehicletofilter")
    private String Selectvehicletofilter = "Select vehicle to filter";
    @SerializedName("Notworking")
    private String Notworking = "Not working";
    @SerializedName("SelectDateandTime")
    private String SelectDateandTime = " Select Date and Time";
    @SerializedName("Search")
    private String Search = "Search";
    @SerializedName("Sorry")
    private String Sorry = "Sorry";
    @SerializedName("Type")
    private String Type = "Type: ";
    //////////////////////////////////////////  //////////////////////////////////////////////////
    @SerializedName("IgnitionReportAddressWise")
    private String IgnitionReportAddressWise = "Ignition Report (Address Wise)";
    @SerializedName("SelectVehicle")
    private String SelectVehicle = "Select Vehicle";
    @SerializedName("EventType")
    private String EventType = "Event Type";
    @SerializedName("Vehicle")
    private String Vehicle = "Vehicle";
    @SerializedName("Youhaventselectedfromdate")
    private String Youhaventselectedfromdate = "You haven't selected from date";
    @SerializedName("SelectVehicletogeneratereport")
    private String SelectVehicletogeneratereport = "Select Vehicle to generate report";
    @SerializedName("Fromdatecantbegreaterthantodate")
    private String Fromdatecantbegreaterthantodate = "From date can't be greater than to date";
    @SerializedName("SomethingwentWrong")
    private String SomethingwentWrong = "Something went Wrong";
    @SerializedName("FailedSomethingwentWrong")
    private String FailedSomethingwentWrong = "Failed Something went Wrong";
    @SerializedName("Filesavedto")
    private String Filesavedto = "File saved to ";
    @SerializedName("SaveReport")
    private String SaveReport = "Save Report";
    @SerializedName("FileName")
    private String FileName = "File Name";
    @SerializedName("Save")
    private String Save = "Save";
    @SerializedName("Cancel")
    private String Cancel = "Cancel";
    @SerializedName("Pleaseenteryourfilename")
    private String Pleaseenteryourfilename = "Please enter your file name";
    @SerializedName("From")
    private String From = "From";
    @SerializedName("To")
    private String To = "Till";
    ////////////////////////////////////////// ActivityNotification //////////////////////////////////////////////////
    @SerializedName("ViewEvents")
    private String ViewEvents = "View Events";
    @SerializedName("Youarenotconnectedtointernet")
    private String Youarenotconnectedtointernet = "You are not connected to internet";
    @SerializedName("NoResultFound")
    private String NoResultFound = "No Result Found";
    @SerializedName("Speed")
    private String Speed = "Speed";
    @SerializedName("Zone")
    private String Zone = "Zone";
    @SerializedName("HarshBreak")
    private String HarshBreak = "Harsh Break";
    @SerializedName("HarshCornering")
    private String HarshCornering = "Harsh Cornering";
    @SerializedName("Close")
    private String Close = "Close";
    @SerializedName("Selectreporttype")
    private String Selectreporttype = "Select report type";
    @SerializedName("OK")
    private String OK = "OK";
    ////////////////////////////////////////// ActivityNotification //////////////////////////////////////////////////
    @SerializedName("Thisismycar")
    private String Thisismycar = "This is my car ";
    @SerializedName("locationanditscurrentlyonthisaddress")
    private String locationanditscurrentlyonthisaddress  = " location and it's currently on this address ";
    @SerializedName("location")
    private String location = " location";
    @SerializedName("Event")
    private String Event = "Event";
    @SerializedName("Dismiss")
    private String Dismiss = "Dismiss";
    @SerializedName("Update")
    private String Update = " Update";
    @SerializedName("Wehavenoticedthatyouareusingolderversionofthisapp")
    private String Wehavenoticedthatyouareusingolderversionofthisapp = "We have noticed that you are using older version of this app.\\nPlease update your app for better result.";
    @SerializedName("Pleasewaitwearegettingyourevents")
    private String Pleasewaitwearegettingyourevents = " Please wait we are getting your events ...";
    @SerializedName("Wearelookingforyourtripshistory")
    private String Wearelookingforyourtripshistory = " We are looking for your trips history";
    @SerializedName("Pleasewaitwearefetchingyourtrips")
    private String Pleasewaitwearefetchingyourtrips = "Please wait \n we are fetching your trips";
    @SerializedName("FromDate")
    private String FromDate = "From Date";
    @SerializedName("InvalidTime")
    private String InvalidTime = "Invalid Time";
    @SerializedName("TillDate")
    private String TillDate = " Till Date";
    @SerializedName("Selecttimein24HoursFormat")
    private String Selecttimein24HoursFormat = "Select time in 24 Hours Format";
    @SerializedName("ThisTripisRunning")
    private String ThisTripisRunning = "This Trip is Running";
    @SerializedName("ConnectionFailed")
    private String ConnectionFailed = "Connection Failed";
    @SerializedName("PleaseselectanyCar")
    private String PleaseselectanyCar = "Please select any Car";
    @SerializedName("Permissionwasdeniedbutisneededforcorefunctionality")
    private String Permissionwasdeniedbutisneededforcorefunctionality = "Permission was denied, but is needed for core functionality";
    @SerializedName("Settings")
    private String Settings = "Settings";
    @SerializedName("ShowAll")
    private String ShowAll = "Show All";
    /////////////////////////////////////////////////Need to go ///////////////////////////////////////////
    @SerializedName("AllVehicles")
    private String AllVehicles = "All Vehicles";
    @SerializedName("Updating")
    private String Updating = "Updating";

    /////////////////////////////////////////////////Need to go ///////////////////////////////////////////

    public String getAllVehicles() {
        return AllVehicles;
    }

    public String getUpdating() {
        return Updating;
    }

    public String getShowAll() {
        return ShowAll;
    }
    public String getPermissionwasdeniedbutisneededforcorefunctionality() {
        return Permissionwasdeniedbutisneededforcorefunctionality;
    }

    public String getSettings() {
        return Settings;
    }

    public String getPleaseselectanyCar() {
        return PleaseselectanyCar;
    }

    public String getConnectionFailed() {
        return ConnectionFailed;
    }

    public String getThisTripisRunning() {
        return ThisTripisRunning;
    }

    public String getSelecttimein24HoursFormat() {
        return Selecttimein24HoursFormat;
    }

    public String getFromDate() {
        return FromDate;
    }

    public String getInvalidTime() {
        return InvalidTime;
    }

    public String getTillDate() {
        return TillDate;
    }

    public String getPleasewaitwearefetchingyourtrips() {
        return Pleasewaitwearefetchingyourtrips;
    }

    public String getWearelookingforyourtripshistory() {
        return Wearelookingforyourtripshistory;
    }

    public String getPleasewaitwearegettingyourevents() {
        return Pleasewaitwearegettingyourevents;
    }

    public String getDismiss() {
        return Dismiss;
    }

    public String getUpdate() {
        return Update;
    }

    public String getWehavenoticedthatyouareusingolderversionofthisapp() {
        return Wehavenoticedthatyouareusingolderversionofthisapp;
    }

    public String getShare() {
        return Share;
    }

    public String getNo() {
        return No;
    }

    public String getAreyousure() {
        return Areyousure;
    }

    public String getYouwanttologout() {
        return Youwanttologout;
    }

    public String getYes() {
        return Yes;
    }

    public String getLoggedOut() {
        return LoggedOut;
    }


    public String getEvent() {
        return Event;
    }

    public String getDistance() {
        return Distance;
    }

    public String getAvgSpeed() {
        return AvgSpeed;
    }

    public String getTrip() {
        return Trip;
    }

    public String getTotaltraveltimeis() {
        return Totaltraveltimeis;
    }

    public String getMins() {
        return mins;
    }

    public String getHr() {
        return Hr;
    }

    public String getThisismycar() {
        return Thisismycar;
    }

    public String getLocationanditscurrentlyonthisaddress() {
        return locationanditscurrentlyonthisaddress;
    }

    public String getLocation() {
        return location;
    }

    public String getOK() {
        return OK;
    }

    public String getSorry() {
        return Sorry;
    }

    public String getSearch() {
        return Search;
    }

    public String getSelectDateandTime() {
        return SelectDateandTime;
    }

    public String getNotworking() {
        return Notworking;
    }

    public String getSelectvehicletofilter() {
        return Selectvehicletofilter;
    }

    public String getType() {
        return Type;
    }

    public String getLogout() {
        return Logout;
    }

    public String getReports() {
        return Reports;
    }

    public String getNotifications() {
        return Notifications;
    }

    public String getTripHistory() {
        return TripHistory;
    }

    public String getDashboard() {
        return Dashboard;
    }

    public String getDetails() {
        return Details;
    }

    public String getVehicleID() {
        return VehicleID;
    }

    public String getDate() {
        return Date;
    }

    public String getTime() {
        return Time;
    }

    public String getCLOSE() {
        return CLOSE;
    }

    public String getJourney() {
        return Journey;
    }

    public String getTrips() {
        return Trips;
    }

    public String getSignIn() {
        return SignIn;
    }

    public String getWelcome() {
        return Welcome;
    }

    public String getSignintocontinue() {
        return Signintocontinue;
    }

    public String getUserName() {
        return UserName;
    }

    public String getPassword() {
        return Password;
    }

    public String getUsernameisempty() {
        return Usernameisempty;
    }

    public String getPasswordisempty() {
        return Passwordisempty;
    }

    public String getPleasecheckyourinternet() {
        return Pleasecheckyourinternet;
    }

    public String getInvalidUserNameorpassword() {
        return InvalidUserNameorpassword;
    }

    public String getLoginSuccessfully() {
        return LoginSuccessfully;
    }

    public String getEmailisempty() {
        return Emailisempty;
    }

    public String getNotavalidemail() {
        return Notavalidemail;
    }

    public String getIgnitionReportAddressWise() {
        return IgnitionReportAddressWise;
    }

    public String getSpeed() {
        return Speed;
    }

    public String getZone() {
        return Zone;
    }

    public String getHarshBreak() {
        return HarshBreak;
    }

    public String getHarshCornering() {
        return HarshCornering;
    }

    public String getClose() {
        return Close;
    }

    public String getSelectreporttype() {
        return Selectreporttype;
    }

    public String getNoResultFound() {
        return NoResultFound;
    }


    public String getYouarenotconnectedtointernet() {
        return Youarenotconnectedtointernet;
    }


    public String getViewEvents() {
        return ViewEvents;
    }

    public String getFrom() {
        return From;
    }

    public String getTo() {
        return To;
    }

    public String getFilesavedto() {
        return Filesavedto;
    }


    public String getPleaseenteryourfilename() {
        return Pleaseenteryourfilename;
    }


    public String getSaveReport() {
        return SaveReport;
    }

    public String getFileName() {
        return FileName;
    }

    public String getSave() {
        return Save;
    }

    public String getCancel() {
        return Cancel;
    }


    public String getSomethingwentWrong() {
        return SomethingwentWrong;
    }

    public String getFailedSomethingwentWrong() {
        return FailedSomethingwentWrong;
    }

    public String getFromdatecantbegreaterthantodate() {
        return Fromdatecantbegreaterthantodate;
    }

    public String getYouhaventselectedfromdate() {
        return Youhaventselectedfromdate;
    }

    public String getSelectVehicletogeneratereport() {
        return SelectVehicletogeneratereport;
    }


    public String getToday() {
        return Today;
    }

    public String getLast48Hours() {
        return Last48Hours;
    }

    public String getLast7Days() {
        return Last7Days;
    }

    public String getCustom() {
        return Custom;
    }

    public String getIgnitionReport() {
        return IgnitionReport;
    }

    public String getSelectVehicle() {
        return SelectVehicle;
    }

    public String getEventType() {
        return EventType;
    }

    public String getVehicle() {
        return Vehicle;
    }

    public String getSelectDate() {
        return SelectDate;
    }

    public String getViewReports() {
        return ViewReports;
    }

    public String getReport() {
        return Report;
    }

    public String getSaveFile() {
        return SaveFile;
    }

    public String getPleasewait() {
        return Pleasewait;
    }

    public String getWearelookingforyourvehicledetails() {
        return Wearelookingforyourvehicledetails;
    }

    public String getExcel() {
        return Excel;
    }

    public String getPDF() {
        return PDF;
    }

    public String getLiveMap() {
        return LiveMap;
    }

    public String getTrackingHistory() {
        return TrackingHistory;
    }

    public String getEvents() {
        return Events;
    }

    public String getLiveTracking() {
        return LiveTracking;
    }
}
