package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AuthCodeResponse {

    @SerializedName("VTrackInfo")
    @Expose
    private VTrackInfo vTrackInfo;
    @SerializedName("VTrackResponse")
    @Expose
    private String vTrackAppResponse;
    @SerializedName("VTracklink")
    @Expose
    private VTracklink vTracklink;




    public VTracklink getvTracklink() {
        return vTracklink;
    }

    public void setvTracklink(VTracklink vTracklink) {
        this.vTracklink = vTracklink;
    }

    public VTrackInfo getVTrackInfo() {
        return vTrackInfo;
    }

    public void setVTrackInfo(VTrackInfo vTrackInfo) {
        this.vTrackInfo = vTrackInfo;
    }

    public String getVTrackAppResponse() {
        return vTrackAppResponse;
    }

    public void setVTrackAppResponse(String vTrackAppResponse) {
        this.vTrackAppResponse = vTrackAppResponse;
    }
    public class VTrackAppResponse {

        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }
    public class VTrackInfo {

        @SerializedName("ClientId")
        @Expose
        private String clientId;
        @SerializedName("ClientName")
        @Expose
        private String clientName;
        @SerializedName("PortalUrl")
        @Expose
        private String portalUrl;
        @SerializedName("ExpiryDate")
        @Expose
        private String expiryDate;
        @SerializedName("AccountCode")
        @Expose
        private String accountCode;
        @SerializedName("Password")
        @Expose
        private String password;
        @SerializedName("Reason")
        @Expose
        private Object reason;

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getPortalUrl() {
            return portalUrl;
        }

        public void setPortalUrl(String portalUrl) {
            this.portalUrl = portalUrl;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Object getReason() {
            return reason;
        }

        public void setReason(Object reason) {
            this.reason = reason;
        }
    }
    public class Data {

        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("refreshToken")
        @Expose
        private String refreshToken;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("clientId")
        @Expose
        private String clientId;
        @SerializedName("FullName")
        @Expose
        private String fullName;
        @SerializedName("Email")
        @Expose
        private String email;
        @SerializedName("clientLanguage")
        @Expose
        private String clientLanguage;
        @SerializedName("MapType")
        @Expose
        private String mapType;
        @SerializedName("clientName")
        @Expose
        private String clientName;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("userLanguage")
        @Expose
        private String userLanguage;
        @SerializedName("timezone")
        @Expose
        private String timezone;
        @SerializedName("languageDict")
        @Expose
        private String languageDict;
        @SerializedName("VehicleStatusCode")
        @Expose
        private ArrayList<VehicleStatusCode> vehicleStatusCode = new ArrayList<>();

        public ArrayList<VehicleStatusCode> getVehicleStatusCode() {
            return vehicleStatusCode;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getClientLanguage() {
            return clientLanguage;
        }

        public void setClientLanguage(String clientLanguage) {
            this.clientLanguage = clientLanguage;
        }

        public String getMapType() {
            return mapType;
        }

        public void setMapType(String mapType) {
            this.mapType = mapType;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getUserLanguage() {
            return userLanguage;
        }

        public void setUserLanguage(String userLanguage) {
            this.userLanguage = userLanguage;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public String getLanguageDict() {
            return languageDict;
        }

        public void setLanguageDict(String languageDict) {
            this.languageDict = languageDict;
        }

    }
    public class VTracklink {

        @SerializedName("VTrackBackendAPI")
        @Expose
        private String vTrackBackendAPI;
        @SerializedName("VTrackLiveAPI")
        @Expose
        private String vTrackLiveAPI;
        @SerializedName("VTrackReportsAPI")
        @Expose
        private String vTrackReportsAPI;
        @SerializedName("VTrackAddressServerAPI")
        @Expose
        private String vTrackAddressServerAPI;

        public String getVTrackBackendAPI() {
            return vTrackBackendAPI;
        }

        public void setVTrackBackendAPI(String vTrackBackendAPI) {
            this.vTrackBackendAPI = vTrackBackendAPI;
        }

        public String getVTrackLiveAPI() {
            return vTrackLiveAPI;
        }

        public void setVTrackLiveAPI(String vTrackLiveAPI) {
            this.vTrackLiveAPI = vTrackLiveAPI;
        }

        public String getVTrackReportsAPI() {
            return vTrackReportsAPI;
        }

        public void setVTrackReportsAPI(String vTrackReportsAPI) {
            this.vTrackReportsAPI = vTrackReportsAPI;
        }

        public String getVTrackAddressServerAPI() {
            return vTrackAddressServerAPI;
        }

        public void setVTrackAddressServerAPI(String vTrackAddressServerAPI) {
            this.vTrackAddressServerAPI = vTrackAddressServerAPI;
        }
    }
}
