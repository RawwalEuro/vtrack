package com.eurosoft.vtrack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.multidex.BuildConfig;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.ReportBody;
import com.eurosoft.vtrack.Models.ReportResponseModel;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import es.dmoral.toasty.Toasty;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.vtrack.Constants.DATETIME_FORMAT_FROMBACKEND;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;


public class ActivityReports extends AppCompatActivity {

    private static File directory;
    private Button pdfBtn, excelBtn;
    private boolean isFrom = false;
    private PDFView pdfView;
    private boolean isCustomSelected = false;
    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };
    private byte[] decodedString;
    private ImageView backIcon;
    private TextView from, to;
    private RelativeLayout rlFrom, rlTo;
    private Calendar calendar;
    private DatePickerDialog dialog;
    private DecimalFormat f;
    private String dateTill, dateFrom;
    private List<Vehicles> vehiclesList = new ArrayList<>();
    private RelativeLayout vehicleLl;
    private TextView vehicle;
    ArrayList<String> items = new ArrayList<>();
    SpinnerDialog spinnerDialog;
    SpinnerDialog spinnerDialogIgintitonType;
    private RadioGroup radioGroup, radioGroupOne;
    private RadioButton radioButton;
    private int reportType = 0;
    private String selectedVehicle = "";
    private Button btnDownload,btnOk;
    private ReportBody reportBody;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout rlProgressBar,parentLayout;
    private RelativeLayout mainRl;
    private String clientId;
    private LoginResponse user;
    private LinearLayout llCustom,llNoData;
    private String currentTime = "";
    private RelativeLayout layoutPdfViewer, rlPdfHeader;
    private TextView header_title,pdfHeader;
    private TextView waitTextview,waitMessageView;
    private ImageView pdfBackIcon;
    private ImageView pdfSaveIcon;
    private String baseToSet;
    public FilterDialog filterDialog;
    private ArrayList<String> itemsType;
    private TextView ignition;
    private RelativeLayout rlIgnition;
    int isPdf = 0;
    int ignitionInt = 0;
    private MasterPojo masterPojo;
    private TextView vehicleTitle,eventTypeTitle,totitle,fromtitle,pagecount;
    private RadioButton today,last48hour,custom,last7day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        f = new DecimalFormat();
        Stash.init(this);
        clientId = Stash.getString(Constants.USER_CLIENT_ID);
        filterDialog = new FilterDialog();
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        initViews();
        getVehicles();
        checkPermissions();
    }

    private void initViews() {
        backIcon = findViewById(R.id.backIcon);
        pdfBtn = findViewById(R.id.pdf);
        excelBtn = findViewById(R.id.excel);
        pdfView = findViewById(R.id.pdfView);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        totitle = (TextView) findViewById(R.id.to_title);
        fromtitle  = (TextView) findViewById(R.id.from_title);
        rlFrom = (RelativeLayout) findViewById(R.id.rlFrom);
        rlTo = (RelativeLayout) findViewById(R.id.rlTo);
        vehicleLl = (RelativeLayout) findViewById(R.id.vehicleLl);
        vehicle = (TextView) findViewById(R.id.vehicle);
        radioGroup = (RadioGroup) findViewById(R.id.radio);
        btnDownload = (Button) findViewById(R.id.btnDownload);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        mainRl = findViewById(R.id.mainRl);
        llCustom = findViewById(R.id.llCustom);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        radioGroupOne = (RadioGroup) findViewById(R.id.radioGroup);
        layoutPdfViewer = (RelativeLayout) findViewById(R.id.layoutPdfViewer);
        rlPdfHeader = (RelativeLayout) findViewById(R.id.rlPdfHeader);
        pdfHeader = (TextView) findViewById(R.id.pdfHeader);
        header_title = (TextView) findViewById(R.id.header_title);
        pdfBackIcon = (ImageView) findViewById(R.id.pdfBackIcon);
        pdfSaveIcon = (ImageView) findViewById(R.id.pdfSaveIcon);
        ignition = (TextView) findViewById(R.id.ignition);
        rlIgnition = (RelativeLayout) findViewById(R.id.rlIgnition);
        waitTextview=(TextView) findViewById(R.id.pleasewait_view) ;
        waitMessageView=(TextView) findViewById(R.id.wait_message_view) ;
        vehicleTitle=(TextView) findViewById(R.id.vechile_title);
        eventTypeTitle=(TextView) findViewById(R.id.event_type_title);
        today=(RadioButton) findViewById(R.id.todayRB);
        custom=(RadioButton) findViewById(R.id.customRB);
        last7day=(RadioButton) findViewById(R.id.weeklyRB);
        last48hour=(RadioButton) findViewById(R.id.last48RB);
        pagecount=(TextView) findViewById(R.id.pagescount);
        llNoData=(LinearLayout) findViewById(R.id.llNoData);
        TextView sorry_title=(TextView) findViewById(R.id.sorry_title);
        parentLayout=(RelativeLayout) findViewById(R.id.parent_layout);
        btnOk = (Button) findViewById(R.id.btnOk);
        masterPojo= (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        sorry_title.setText(masterPojo.getSorry());
        btnOk.setText(masterPojo.getOK());
        pdfHeader.setText(masterPojo.getSaveFile());
        pdfBtn.setText(masterPojo.getPDF());
        excelBtn.setText(masterPojo.getExcel());
        header_title.setText(masterPojo.getReport());
        btnDownload.setText(masterPojo.getViewReports());
        to.setHint(masterPojo.getSelectDate());
        from.setHint(masterPojo.getSelectDate());
        waitTextview.setText(masterPojo.getPleasewait());
        waitMessageView.setText(masterPojo.getWearelookingforyourvehicledetails());
        vehicleTitle.setText(masterPojo.getVehicle());
        vehicle.setHint(masterPojo.getSelectVehicle());
        eventTypeTitle.setText(masterPojo.getEventType());
        ignition.setHint(masterPojo.getIgnitionReport());
        today.setText(masterPojo.getToday());
        custom.setText(masterPojo.getCustom());
        last7day.setText(masterPojo.getLast7Days());
        last48hour.setText(masterPojo.getLast48Hours());
        fromtitle.setText(masterPojo.getFrom());
        totitle.setText(masterPojo.getTo());
        todaySetup(llCustom, 0, "today");

        radioGroupOne.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.todayRB:

                        todaySetup(llCustom, 0, "today");
                        isCustomSelected = false;
                        break;

                    case R.id.last48RB:
                        todaySetup(llCustom, -2, "last48RB");
                        isCustomSelected = false;
                        break;

                    case R.id.weeklyRB:
                        todaySetup(llCustom, -7, "last48RB");
                        isCustomSelected = false;
                        break;

                    case R.id.customRB:
                        llCustom.setVisibility(View.VISIBLE);
                        isCustomSelected = true;
                        break;
                }
            }
        });


        pdfSaveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    filterDialog.showDialog(ActivityReports.this);
                } catch (ParseException e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }
            }
        });

        pdfBackIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainRl.setVisibility(View.VISIBLE);
                layoutPdfViewer.setVisibility(View.GONE);
                rlPdfHeader.setVisibility(View.GONE);
            }
        });


        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation(isCustomSelected);
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);
                if (radioButton.getText().toString().equalsIgnoreCase("PDF")) {
                    reportType = 0;
                } else {
                    reportType = 1;
                }
            }
        });
        setupDateView();
    }

    private void validation(boolean isCustomTrue) {

        if (isCustomTrue == true) {

            String fromText = from.getText().toString();
            String toText = to.getText().toString();

            if (fromText.equalsIgnoreCase("") || fromText.equalsIgnoreCase("")) {
                Toasty.error(ActivityReports.this, masterPojo.getSelectVehicletogeneratereport(), Toasty.LENGTH_SHORT).show();
                return;
            }

            if (toText.equalsIgnoreCase("") || toText.equalsIgnoreCase("")) {
                Toasty.error(ActivityReports.this, masterPojo.getYouhaventselectedfromdate(), Toasty.LENGTH_SHORT).show();
                return;
            }
        }

        if (selectedVehicle.equalsIgnoreCase("")) {
            Toasty.warning(getApplicationContext(), masterPojo.getSelectVehicletogeneratereport(), Toasty.LENGTH_LONG).show();
            return;
        }

        reportBody = new ReportBody();
        reportBody.setVehicle(selectedVehicle);
        reportBody.setTodate(dateTill);
        reportBody.setFromdate(dateFrom);
        reportBody.setReportType(String.valueOf(isPdf));
        reportBody.setClientId(clientId);
        reportBody.setUnit(user.getUnit());
        reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));

        if (isStoragePermissionGranted()) {
            SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT_FROMBACKEND);
//dates to be compare
            Date date1 = null;
            Date date2 = null;
            try {
                Log.e("ActivityReports","dateTill:"+dateTill);
                Log.e("ActivityReports","dateFrom:"+dateFrom);
                date1 = sdf.parse(dateFrom);
                date2 = sdf.parse(dateTill);
            } catch (ParseException e) {
                Log.e(EXCEPTION_TAG,""+e.getMessage());
            }

            if (isCustomTrue == true) {

                try {
                    if (sdf.format(date2).compareTo(sdf.format(date1)) < 0) {
                        Toasty.error(ActivityReports.this, masterPojo.getFromdatecantbegreaterthantodate(), Toasty.LENGTH_SHORT).show();
                        return;
                    }
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }

            }
            if (ignitionInt == 0) {
                callApiFirst(0, "");
            } else if (ignitionInt == 1) {
                callApiAddressWise(0, "");
            }
        } else {
            checkPermissions();
        }
    }

    private void todaySetup(LinearLayout layoutCustom, int i2, String today) {
        layoutCustom.setVisibility(View.GONE);
        getCalculatedDate("yyyy-MM-dd", i2);
        currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        dateFrom = getCalculatedDate("yyyy-MM-dd", i2) + "T00:00:00Z";
        dateTill = getCalculatedDate("yyyy-MM-dd", 0) + "T" + currentTime + "Z";
        Log.e("ActivityReports", "dateFrom"+dateFrom);
    }


    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.e("ActivityReports", "Permission is granted");
                return true;
            } else {
                Log.e("ActivityReports", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.e("TAG", "Permission is granted");
            return true;
        }
    }


    private void callApiFirst(int type, String fileName) {

        Log.e("ActivityReports", "isPdf:"+isPdf);
        reportBody = new ReportBody();
        reportBody.setVehicle(selectedVehicle);
        reportBody.setTodate(dateTill);
        reportBody.setFromdate(dateFrom);
        reportBody.setReportType(String.valueOf(0));
        reportBody.setClientId(clientId);
        reportBody.setUnit(user.getUnit());
        reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getApiReportClient().create(ApiInterface.class);
        Call<ReportResponseModel> call = apiService.getReports(reportBody, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<ReportResponseModel>() {
            @Override
            public void onResponse(Call<ReportResponseModel> call, Response<ReportResponseModel> response) {
                try {
                    ReportResponseModel mresponse=response.body();
                    Boolean sucess=mresponse.getSuccess();
                    String message=mresponse.getMessage();
                    List<ReportResponseModel.data> data=mresponse.getData();
                    if(sucess){
                        progressVisiblityGone();
                        Log.e("ActivityReports", "Response"+data.get(0).getReportString() + "");
                        saveFile(data.get(0).getReportString());
                    }
                    else {
                        progressVisiblityGone();
                        shownoReportFoundDialog();
                    }
                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<ReportResponseModel> call, Throwable t) {
                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });
    }


    private void callApiAddressWise(int type, String fileName) {

        Log.e("ActivityReports", "isPdf:"+isPdf + "");
        reportBody = new ReportBody();
        reportBody.setVehicle(selectedVehicle);
        reportBody.setTodate(dateTill);
        reportBody.setFromdate(dateFrom);
        reportBody.setReportType(String.valueOf(0));
        reportBody.setClientId(clientId);
        reportBody.setUnit(user.getUnit());
        reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getApiReportClient().create(ApiInterface.class);
        Call<ReportResponseModel> call = apiService.getReportsAddressWise(reportBody, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<ReportResponseModel>() {
            @Override
            public void onResponse(Call<ReportResponseModel> call, Response<ReportResponseModel> response) {
                try {
                    ReportResponseModel mresponse=response.body();
                    Boolean sucess=mresponse.getSuccess();
                    String message=mresponse.getMessage();
                    List<ReportResponseModel.data> data=mresponse.getData();
                    if (sucess) {
                        progressVisiblityGone();
                        Log.e("ActivityReports", "data.toString()"+data.toString() + "");
                        saveFile(data.get(0).getReportString());
                        return;
                    } else {
                        progressVisiblityGone();
                        shownoReportFoundDialog();
                    }
                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<ReportResponseModel> call, Throwable t) {
                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });
    }

    private void callApi(int type, String fileName) {


        Log.e("isPdf", isPdf + "");
        if (isPdf == 1) {
            reportBody = new ReportBody();
            reportBody.setVehicle(selectedVehicle);
            reportBody.setTodate(dateTill);
            reportBody.setFromdate(dateFrom);
            reportBody.setReportType(String.valueOf(1));
            reportBody.setClientId(clientId);
            reportBody.setUnit(user.getUnit());
            reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));
        } else if (isPdf == 0) {
            reportBody = new ReportBody();
            reportBody.setVehicle(selectedVehicle);
            reportBody.setTodate(dateTill);
            reportBody.setFromdate(dateFrom);
            reportBody.setReportType(String.valueOf(0));
            reportBody.setClientId(clientId);
            reportBody.setUnit(user.getUnit());
            reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getApiReportClient().create(ApiInterface.class);
        Call<ReportResponseModel> call = apiService.getReports(reportBody, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<ReportResponseModel>() {
            @Override
            public void onResponse(Call<ReportResponseModel> call, Response<ReportResponseModel> response) {
                try {
                    ReportResponseModel mresponse=response.body();
                    Boolean sucess=mresponse.getSuccess();
                    String message=mresponse.getMessage();
                    List<ReportResponseModel.data> data=mresponse.getData();
                    if(sucess){
                        progressVisiblityGone();
                        Log.e("ActivityReports", "Response"+response.body() + "");
                        if (type == 0) {
                            storetoExcelandOpen(getApplicationContext(), data.get(0).getReportString(), fileName, 0);
                        } else if (type == 1) {
                            storetoExcelandOpen(getApplicationContext(), data.get(0).getReportString(), fileName, 1);
                        }

                        isPdf = 0;
                        return;
                    }
                    else {
                        progressVisiblityGone();
                        shownoReportFoundDialog();
                        isPdf = 0;
                        return;
                    }

                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<ReportResponseModel> call, Throwable t) {
                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
                isPdf = 0;
            }
        });
    }


    private void callApiForAddressWiseSave(int type, String fileName) {


        Log.e("ActivityReports", "isPdf"+isPdf + "");
        if (isPdf == 1) {
            reportBody = new ReportBody();
            reportBody.setVehicle(selectedVehicle);
            reportBody.setTodate(dateTill);
            reportBody.setFromdate(dateFrom);
            reportBody.setReportType(String.valueOf(1));
            reportBody.setClientId(clientId);
            reportBody.setUnit(user.getUnit());
            reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));
        } else if (isPdf == 0) {
            reportBody = new ReportBody();
            reportBody.setVehicle(selectedVehicle);
            reportBody.setTodate(dateTill);
            reportBody.setFromdate(dateFrom);
            reportBody.setReportType(String.valueOf(0));
            reportBody.setClientId(clientId);
            reportBody.setUnit(user.getUnit());
            reportBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getApiReportClient().create(ApiInterface.class);
        Call<ReportResponseModel> call = apiService.getReportsAddressWise(reportBody, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<ReportResponseModel>() {
            @Override
            public void onResponse(Call<ReportResponseModel> call, Response<ReportResponseModel> response) {
                try {

                    ReportResponseModel mresponse=response.body();
                    Boolean sucess=mresponse.getSuccess();
                    List<ReportResponseModel.data> data=mresponse.getData();
                    if(sucess){
                        progressVisiblityGone();
                        Log.e("ActivityReports", "Response"+data.toString() + "");
                        if (type == 0) {
                            storetoExcelandOpen(getApplicationContext(), data.get(0).getReportString(), fileName, 0);
                        } else if (type == 1) {
                            storetoExcelandOpen(getApplicationContext(), data.get(0).getReportString(), fileName, 1);
                        }
                        isPdf = 0;
                        return;
                    }
                    else {
                        progressVisiblityGone();
                        shownoReportFoundDialog();
                        isPdf = 0;
                    }

                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<ReportResponseModel> call, Throwable t) {
                Toasty.error(getApplicationContext(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
                isPdf = 0;
            }
        });
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (requestCode == 100) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
                // callApi(reportType);
            }
            else {
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_LONG)
                .setAction(getString(actionStringId), listener).show();
    }


/*    private void getCalendarInstance() {
        calendar = Calendar.getInstance(TimeZone.getDefault());

        dialog = new DatePickerDialog(ActivityReports.this, datePickerListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(new Date().getTime());

      *//*  setDataIsFrom(String.valueOf(calendar.get(Calendar.YEAR)), String.valueOf(calendar.get(Calendar.MONTH) + 1), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        setDataTo(String.valueOf(calendar.get(Calendar.YEAR)), String.valueOf(calendar.get(Calendar.MONTH) + 1), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        dateTill = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        dateFrom = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));*//*
    }*/

    public void saveFile(String base) {
        baseToSet = base;
        layoutPdfViewer.setVisibility(View.VISIBLE);
        rlPdfHeader.setVisibility(View.VISIBLE);
        pdfView.setVisibility(View.VISIBLE);
        mainRl.setVisibility(View.GONE);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        decodedString = Base64.decode(base, Base64.DEFAULT);
        Log.e("ActivityReports", "decodedString"+decodedString + "");

        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.hhs.gov/sites/default/files/sample-ce-statement-urdu.pdf"));
        startActivity(browserIntent);*/

        try {
            pdfView.fromBytes(decodedString) // stream is written to bytearray - native code cannot use Java Streams or pdfView.fromSource(DocumentSource) or pdfView.fromAsset(String)
                    // .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .enableAnnotationRendering(false)
                    .defaultPage(0)
                    .onPageChange(new OnPageChangeListener() {
                        @Override
                        public void onPageChanged(int page, int pageCount) {
                            int mCurrentPage = page+1;
                            pagecount.setText(""+mCurrentPage+"/"+pageCount);
                            pagecount.setVisibility(View.VISIBLE);
                        }
                    })
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(30)
                    .load();
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG,""+e.getMessage());
        }

    }


    public void storetoExcelandOpen(Context context, String base, String name, int type) {

        if (Build.VERSION_CODES.R >= Build.VERSION.SDK_INT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "/VTrack");
            }
            if (!directory.exists())
                directory.mkdir();
        } else {
            directory = new File(Environment.getExternalStorageDirectory() + "/VTrack");
            directory.mkdirs();
            if (!directory.exists() && !directory.isDirectory()) {
                // create empty directory
                if (directory.mkdirs()) {
                    Log.e("ActivityReports", "App dir created");
                } else {
                    Log.e("ActivityReports", "Unable to create app dir!");
                }
            } else {
                Log.e("ActivityReports", "App dir already exists");
            }
        }

        if (type == 1) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = name + n + ".xls";
            File file = new File(directory, fname);
            if (file.exists())
                file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                byte[] excelAsBytes = Base64.decode(base, Base64.NO_WRAP);
                out.write(excelAsBytes);
                out.flush();
                out.close();
                File imgFile = new File(directory, fname);
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                Uri uri;
                uri = Uri.parse("file://" + imgFile);
                Toasty.success(ActivityReports.this, masterPojo.getFilesavedto()+" " + directory.getAbsolutePath(), Toasty.LENGTH_LONG).show();
               /* sendIntent.setDataAndType(uri, "application/vnd.ms-excel");
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(sendIntent);*/
            } catch (Exception e) {
                Log.e(EXCEPTION_TAG,""+e.getMessage());
                return;
            }
        } else {


            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = name + n + ".pdf";
            File file = new File(directory, fname);
            if (file.exists())
                file.delete();
            try {

                FileOutputStream out = new FileOutputStream(file);
                byte[] excelAsBytes = Base64.decode(base, Base64.NO_WRAP);
                out.write(excelAsBytes);
                out.flush();
                out.close();
                Toasty.success(ActivityReports.this, masterPojo.getFilesavedto()+"" + directory.getAbsolutePath(), Toasty.LENGTH_LONG).show();

                /* decodedString = Base64.decode(base, Base64.DEFAULT);

                pdfView.fromBytes(decodedString) // stream is written to bytearray - native code cannot use Java Streams or pdfView.fromSource(DocumentSource) or pdfView.fromAsset(String)
                        .pages(0, 2, 1, 3, 3, 3) // all pages are displayed by default
                        .enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)
                        .password(null)
                        .scrollHandle(null)
                        .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                        // spacing between pages in dp. To define spacing color, set view background
                        .spacing(0)
                        .load();*/

            } catch (Exception e) {
                Log.e(EXCEPTION_TAG,""+e.getMessage());
                return;
            }
        }

    }
    public class FilterDialog {
        public void showDialog(Activity activity) throws ParseException {
            final Dialog dialogFilter = new Dialog(activity, R.style.Theme_Dialog);
            dialogFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogFilter.setCancelable(false);
            dialogFilter.setContentView(R.layout.diaolog_save_file);
            dialogFilter.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            Button btnSave = (Button) dialogFilter.findViewById(R.id.btnSave);
            Button btnCancel = (Button) dialogFilter.findViewById(R.id.btnCancel);
            TextView fileNameHeading=(TextView) dialogFilter.findViewById(R.id.fileNameHeading);
            TextView saveReportAs=(TextView) dialogFilter.findViewById(R.id.saveReportAs);
            saveReportAs.setText(masterPojo.getSaveReport());
            btnSave.setText(masterPojo.getSave());
            btnCancel.setText(masterPojo.getCancel());
            fileNameHeading.setText(masterPojo.getFileName());
            RadioGroup fileFormatRG = (RadioGroup) dialogFilter.findViewById(R.id.fileFormatRG);
            RadioButton excel=(RadioButton) dialogFilter.findViewById(R.id.pdfExcel);
            RadioButton pdf=(RadioButton) dialogFilter.findViewById(R.id.pdfRd);
            excel.setText(masterPojo.getExcel());
            pdf.setText(masterPojo.getPDF());
            TextView fileName = (TextView) dialogFilter.findViewById(R.id.fileName);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogFilter.dismiss();
                }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fileName.getText().toString() == null || fileName.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), masterPojo.getPleaseenteryourfilename(), Toast.LENGTH_SHORT).show();
                        return;
                    } else {

                        Log.e("ActivityReports","isPdf"+isPdf + "");
                        Log.e("ActivityReports","ignitionInt"+ignitionInt + "");
                        if (isPdf == 0) {
                            if (ignitionInt == 0) {
                                callApi(0, fileName.getText().toString());
                                dialogFilter.dismiss();
                            } else {
                                callApiForAddressWiseSave(0, fileName.getText().toString());
                                dialogFilter.dismiss();
                            }
                        } else if (isPdf == 1) {
                            if (ignitionInt == 1) {
                                callApiForAddressWiseSave(1, fileName.getText().toString());
                                dialogFilter.dismiss();
                            } else {
                                callApi(1, fileName.getText().toString());
                                dialogFilter.dismiss();
                            }

                        }
                    }
                }
            });

            fileFormatRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (i) {
                        case R.id.pdfRd:
                            isPdf = 0;
                            break;
                        case R.id.pdfExcel:
                            isPdf = 1;
                            break;
                    }
                }
            });

            dialogFilter.show();

        }

      /*  private void todaySetup(LinearLayout layoutCustom, int i2, String today) {
            layoutCustom.setVisibility(View.GONE);
            getCalculatedDate("yyyy-MM-dd", i2);
            currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
            dateFrom = getCalculatedDate("yyyy-MM-dd", i2) + "T00:00:00Z";
            dateTill = getCalculatedDate("yyyy-MM-dd", 0) + "T" + currentTime + "Z";
            Log.e(today, dateFrom);
        }*/
    }


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            selectedMonth = selectedMonth + 1;
            String year = String.valueOf(selectedYear);
            //int seletdMonth = (selectedMonth<10? Integer.parseInt(("0"+selectedMonth)) :(selectedMonth));
            String seletdMonth = f.format(selectedMonth);
            String month = seletdMonth;
            String day = (f.format(selectedDay));
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog timePickerDialog = new TimePickerDialog(ActivityReports.this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            if (isFrom) {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateTill = year + "-" + month + "-" + day + " " + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataIsFrom(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));
                            } else {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateFrom = year + "-" + month + "-" + day + " " + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataTo(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));
                            }
                        }
                    }, hour, minute, true);
            if (!isFrom) {
                timePickerDialog.updateTime(00, 00);
            }
            timePickerDialog.show();


            /*timePickerDialog.show();

            if (isFrom) {
                dateTill = year + "-" + month + "-" + day + " " + "00:00:00Z";
                setDataIsFrom(year, month, day);
            } else {
                dateFrom = year + "-" + month + "-" + day + " " + "00:00:00Z";
                setDataTo(year, month, day);
            }*/
        }
    };


    public String convertIntoTwo(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }


    private void setDataIsFrom(String year, String month, String day, String time) {
        to.setText(day + "-" + month + "-" + year + " " + time);
    }


    private void setDataTo(String year, String month, String day, String time) {
        //fromdate.setText(day + "-" + month + "-" + year);
        from.setText(day + "-" + month + "-" + year + " " + " " + time);
    }

    @Override
    public void onBackPressed() {
        if (pdfView.getVisibility() == View.VISIBLE) {
            layoutPdfViewer.setVisibility(View.GONE);
            pdfView.setVisibility(View.GONE);
            mainRl.setVisibility(View.VISIBLE);
            return;
        }
        super.onBackPressed();

    }

    private void getVehicles() {
        class GetVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                vehiclesList = DatabaseClient
                        .getInstance(ActivityReports.this)
                        .getAppDatabase()
                        .vehiclesDao()
                        .getAll();
                return vehiclesList;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                if (vehiclesList == null || vehiclesList.size() == 0) {
                    return;
                } else {
                    items = new ArrayList<>();
                    for (int i = 0; i < vehiclesList.size(); i++) {
                        items.add(vehiclesList.get(i).getVehicleReg());
                    }
                    vehicle.setText(vehiclesList.get(0).getVehicleReg());
                    selectedVehicle = vehiclesList.get(0).getVehicleReg();
                    itemsType = new ArrayList<>();
                    itemsType.add(masterPojo.getIgnitionReport());
                    itemsType.add(masterPojo.getIgnitionReportAddressWise());
                    spinnerDialog = new SpinnerDialog(ActivityReports.this, items, masterPojo.getSelectVehicle(), masterPojo.getClose());// With No Animation
                    spinnerDialogIgintitonType = new SpinnerDialog(ActivityReports.this, itemsType, masterPojo.getSelectreporttype(), masterPojo.getClose());// With 	Animation
                    spinnerDialog.setCancellable(true); // for cancellable
                    spinnerDialog.setShowKeyboard(false);// for open keyboard by default
                    spinnerDialogIgintitonType.setCancellable(true); // for cancellable
                    spinnerDialogIgintitonType.setShowKeyboard(false);
                    spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String item, int position) {
                            Toast.makeText(ActivityReports.this, item + "  ", Toast.LENGTH_SHORT).show();
                            vehicle.setText(item);
                            selectedVehicle = item;
                        }
                    });

                    spinnerDialogIgintitonType.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String item, int position) {
                            Toast.makeText(ActivityReports.this, item + "  ", Toast.LENGTH_SHORT).show();
                            ignition.setText(item);
                            if (item.equalsIgnoreCase("Ignition Report")) {
                                ignitionInt = 0;
                            } else if (item.equalsIgnoreCase("Ignition Report (Address Wise)")) {
                                ignitionInt = 1;
                            } else {
                                ignitionInt = 0;
                            }
                        }
                    });


                    findViewById(R.id.rlIgnition).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            spinnerDialogIgintitonType.showSpinerDialog();
                        }
                    });

                    findViewById(R.id.vehicleLl).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            spinnerDialog.showSpinerDialog();
                        }
                    });
                    //  setUpRecycler(vehiclesList);
                }
            }

        }
        GetVehiclesList gt = new GetVehiclesList();
        gt.execute();
    }

    public void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        btnDownload.setClickable(false);
        mainRl.setEnabled(false);
    }


    public void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        btnDownload.setClickable(true);
        mainRl.setEnabled(true);
    }

    Calendar calendarfromdate, calendertilldate;
    Dialog datePickerDialogfromdate, datePickerDialogtilldate;
    DatePicker datePickertilldate;
    int hrsFrom = 00, minsFrom = 00, hrsTill = 23, minsTill = 59;
    private void setupDateView() {
        getCalendarInstance();
        rlFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFrom = false;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);
            }
        });

        rlTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFrom = true;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);
            }
        });

        initDatePickerFromDateDialog();
        initDatePickerTillDateDialog();
    }



    private void setFromDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);

        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        dateFrom =selectedYear + "-" + monthString + "-" + dayString + "T" + time + ":00Z";
        Log.e("ActivityReports","dateFrom"+dateFrom);
        calendarfromdate.set(selectedYear, selectedMonth, selectedDay);
        from.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogfromdate.dismiss();
    }

    private void setTillDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);
        String daytillString = String.valueOf(datePickertilldate.getDayOfMonth());


        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        if (daytillString.length() == 1) {
            daytillString = "0" + daytillString;
        }
        dateTill = datePickertilldate.getYear() + "-" + monthString + "-" + daytillString + "T" + time + ":00Z";
        Log.e("ActivityReports","dateTill"+dateTill);
        calendertilldate.set(selectedYear, selectedMonth, selectedDay);
        to.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogtilldate.dismiss();

    }


    private void getCalendarInstance() {
        calendarfromdate = Calendar.getInstance(TimeZone.getDefault());
        calendertilldate = Calendar.getInstance(TimeZone.getDefault());
    }

    public void initDatePickerFromDateDialog() {
        datePickerDialogfromdate = new Dialog(ActivityReports.this);
        datePickerDialogfromdate.setContentView(R.layout.datepicker_dialog);
        DatePicker datePicker = datePickerDialogfromdate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogfromdate.findViewById(R.id.today);
        EditText hours = datePickerDialogfromdate.findViewById(R.id.hours);
        EditText mins = datePickerDialogfromdate.findViewById(R.id.mins);
        hours.setHint("00");
        mins.setHint("00");
        textView1.setText(masterPojo.getFrom());
        TextView message = datePickerDialogfromdate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        TextView Ok = datePickerDialogfromdate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogfromdate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        datePicker.init(calendarfromdate.get(Calendar.YEAR), calendarfromdate.get(Calendar.MONTH), calendarfromdate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsFrom > 23 || minsFrom > 60) {
                    Toasty.error(ActivityReports.this,  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }

                if (hours.getText().toString().equals("")) {
                    hrsFrom=00;
                }
                if( mins.getText().toString().equals("")){
                    minsFrom=00;

                }
                setFromDate(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear(), convertIntoTwo(hrsFrom) + ":" + convertIntoTwo(minsFrom));
                datePickerDialogfromdate.dismiss();

            }
        });
        datePicker.setMaxDate(new Date().getTime());

        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogfromdate.dismiss();
            }
        });

        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsFrom = Integer.parseInt(hours.getText().toString());
                }


                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals(""))

                        hrsFrom = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());

                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsFrom = Integer.parseInt(mins.getText().toString());
                }


                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals(""))
                        minsFrom = Integer.parseInt(mins.getText().toString());
                    mins.setText(mins.getText().toString());
                    mins.setSelection(mins.getText().length());
                    return true;
                }
                return false;
            }
        });
    }

    public void initDatePickerTillDateDialog() {
        datePickerDialogtilldate = new Dialog(ActivityReports.this);
        datePickerDialogtilldate.setContentView(R.layout.datepicker_dialog);
        datePickertilldate = datePickerDialogtilldate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogtilldate.findViewById(R.id.today);
        textView1.setText(masterPojo.getTillDate());
        EditText hours = datePickerDialogtilldate.findViewById(R.id.hours);
        TextView message = datePickerDialogtilldate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        EditText mins = datePickerDialogtilldate.findViewById(R.id.mins);
        updateDatePickerTillDate(0l,hours,mins);
        hours.setHint("23");
        mins.setHint("59");
        TextView Ok = datePickerDialogtilldate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogtilldate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogtilldate.dismiss();
            }
        });

        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsTill = Integer.parseInt(hours.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals(""))
                        hrsTill = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());
                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsTill = Integer.parseInt(mins.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals(""))
                        minsTill = Integer.parseInt(mins.getText().toString());
                    mins.setText(mins.getText().toString());
                    mins.setSelection(mins.getText().length());

                    return true;
                }
                return false;
            }
        });

    }

    public void updateDatePickerTillDate(Long mindate,EditText hours,EditText mins) {
        datePickertilldate.init(calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsTill > 23 || minsTill > 60) {
                    Toasty.error(ActivityReports.this,  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }
                if (hours.getText().toString().equals("") ) {
                    hrsTill=23;
                }
                if(mins.getText().toString().equals("")){
                    minsTill=59;
                }
                setTillDate(datePickertilldate.getDayOfMonth(), datePickertilldate.getMonth(), datePickertilldate.getYear(), convertIntoTwo(hrsTill) + ":" + convertIntoTwo(minsTill));
                datePickerDialogtilldate.dismiss();

            }
        });

        datePickertilldate.setMaxDate(new Date().getTime());
        datePickertilldate.setMinDate(mindate);

    }
    private void shownoReportFoundDialog(){
        llNoData.setVisibility(View.VISIBLE);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNoData.setVisibility(View.GONE);
            }
        });

    }
}