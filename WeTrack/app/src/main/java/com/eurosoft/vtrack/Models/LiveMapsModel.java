package com.eurosoft.vtrack.Models;

import android.widget.ScrollView;

public class LiveMapsModel {

    private Double longitude;
    private Double latitude;
    private String Altitude;
    private int Angle;
    private String satellites;
    private int speed;
    private String timestamp;
    private int ignition;
    private String clientId;
    private String vehicleId;
    private String vehicleNo;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleReg;
    private String speedUnit;
    private String speedWithUnitDesc;
    private String vehicleStatusCode;




    public String getVehicleStatusCode() {
        return vehicleStatusCode;
    }

    public LiveMapsModel(Double longitude, Double latitude, int angle, int speed, String timestamp, String vehicleNo, int ignition, String vehicleId, String vehicleMake, String vehicleModel
           , String speedWithUnitDesc, String vehicleStatusCode) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.Angle = angle;
        this.speed = speed;
        this.timestamp = timestamp;
        this.vehicleNo = vehicleNo;
        this.ignition = ignition;
        this.vehicleMake=vehicleMake;
        this.vehicleId=vehicleId;
        this.vehicleModel=vehicleModel;
        this.vehicleReg=vehicleNo;
        this.vehicleStatusCode=vehicleStatusCode;
        this.speedWithUnitDesc=speedWithUnitDesc;
    }

    public String getSpeedWithUnitDesc() {
        return speedWithUnitDesc;
    }

    public String getSpeedUnit() {
        return speedUnit;
    }

    public void setSpeedUnit(String speedUnit) {
        this.speedUnit = speedUnit;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getAltitude() {
        return Altitude;
    }

    public void setAltitude(String altitude) {
        Altitude = altitude;
    }

    public int getAngle() {
        return Angle;
    }

    public void setAngle(int angle) {
        Angle = angle;
    }

    public String getSatellites() {
        return satellites;
    }

    public void setSatellites(String satellites) {
        this.satellites = satellites;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getIgnition() {
        return ignition;
    }

    public void setIgnition(int ignition) {
        this.ignition = ignition;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public void setVehicleReg(String vehicleReg) {
        this.vehicleReg = vehicleReg;
    }
}
