package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notifications {


    @SerializedName("ClientId")
    @Expose
    private String ClientId;
    @SerializedName("NotificationTypeId")
    @Expose
    private String NotificationTypeId;
    @SerializedName("EventTitle")
    @Expose
    private String eventTitle;
    @SerializedName("EventDetail")
    @Expose
    private String eventDetail;
    @SerializedName("EventTypeId")
    @Expose
    private String eventTypeId;
    @SerializedName("EventOccuredOn")
    @Expose
    private String eventOccuredOn;
    @SerializedName("Vehicle")
    @Expose
    private String vehicle;
    @SerializedName("isAcknowledge")
    @Expose
    private Boolean isAcknowledge;
    @SerializedName("EventType")
    @Expose
    private String EventType;


    public Notifications() {
    }


    public Notifications(String clientId) {
        ClientId = clientId;
    }

    public Notifications(String clientId, String eventType) {
        ClientId = clientId;
        EventType = eventType;
    }


    public String getEventType() {
        return EventType;
    }

    public void setEventType(String eventType) {
        EventType = eventType;
    }

    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        this.ClientId = clientId;
    }

    public String getNotificationTypeId() {
        return NotificationTypeId;
    }

    public void setNotificationTypeId(String notificationTypeId) {
        this.NotificationTypeId = notificationTypeId;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventDetail() {
        return eventDetail;
    }

    public void setEventDetail(String eventDetail) {
        this.eventDetail = eventDetail;
    }

    public String getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(String eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventOccuredOn() {
        return eventOccuredOn;
    }

    public void setEventOccuredOn(String eventOccuredOn) {
        this.eventOccuredOn = eventOccuredOn;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Boolean getIsAcknowledge() {
        return isAcknowledge;
    }

    public void setIsAcknowledge(Boolean isAcknowledge) {
        this.isAcknowledge = isAcknowledge;
    }
}
