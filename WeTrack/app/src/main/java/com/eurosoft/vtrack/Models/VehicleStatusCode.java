package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleStatusCode {
    @SerializedName("vehicleColor")
        @Expose
        private String vehicleColor;
        @SerializedName("vehicleStatus")
        @Expose
        private String vehicleStatus;
        @SerializedName("vehicleImgUrl")
        @Expose
        private String vehicleImgUrl;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("vehicleStatusCode")
        @Expose
        private String vehicleStatusCode;
        @SerializedName("id")
        @Expose
        private String id;
      @SerializedName("vehicleTextColor")
      @Expose
       private String vehicleTextColor;

    public String getVehicleTextColor() {
        return vehicleTextColor;
    }

    private String icon_name;

    public void setIcon_name(String icon_name) {
        this.icon_name = icon_name;
    }

    public String getIcon_name() {
        return icon_name;
    }

    public String getVehicleColor() {
            return vehicleColor;
        }

        public void setVehicleColor(String vehicleColor) {
            this.vehicleColor = vehicleColor;
        }

        public String getVehicleStatus() {
            return vehicleStatus;
        }

        public void setVehicleStatus(String vehicleStatus) {
            this.vehicleStatus = vehicleStatus;
        }

        public String getVehicleImgUrl() {
            return vehicleImgUrl;
        }

        public void setVehicleImgUrl(String vehicleImgUrl) {
            this.vehicleImgUrl = vehicleImgUrl;
        }

        public Integer getV() {
            return v;
        }

    public VehicleStatusCode(String vehicleColor, String vehicleStatus, String vehicleImgUrl, String vehicleStatusCode, String icon_name
            ,String vehicleTextColor
    ) {
        this.vehicleColor = vehicleColor;
        this.vehicleStatus = vehicleStatus;
        this.vehicleImgUrl = vehicleImgUrl;
        this.vehicleStatusCode = vehicleStatusCode;
        this.icon_name = icon_name;
       this.vehicleTextColor=vehicleTextColor;
    }

    public void setV(Integer v) {
            this.v = v;
        }

        public String getVehicleStatusCode() {
            return vehicleStatusCode;
        }

        public void setVehicleStatusCode(String vehicleStatusCode) {
            this.vehicleStatusCode = vehicleStatusCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }


}
