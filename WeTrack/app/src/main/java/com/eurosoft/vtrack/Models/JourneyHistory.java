package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JourneyHistory {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("Vehicle")
    @Expose
    private String vehicle;
    @SerializedName("angle")
    @Expose
    private Integer angle;
    @SerializedName("speed")
    @Expose
    private String speed;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("tripId")
    @Expose
    private String tripId;
    @SerializedName("distanceCovered")
    @Expose
    private String distanceCovered;

    public String getDistanceCovered() {
        return distanceCovered;
    }

    @SerializedName("VehicleReg")
    @Expose
    private String VehicleReg;

    @SerializedName("toDateTime")
    @Expose
    private String toDateTime;

    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("TimeZone")
    @Expose
    private String TimeZone;

    @SerializedName("clientId")
    @Expose
    private String clientId;

    @SerializedName("fromDateTime")
    @Expose
    private String fromDateTime;

    @SerializedName("TripStart")
    @Expose
    private String TripStart;

    @SerializedName("TripEnd")
    @Expose
    private String TripEnd;


    public String getTripStart() {
        return TripStart;
    }

    public void setTripStart(String tripStart) {
        TripStart = tripStart;
    }

    public String getTripEnd() {
        return TripEnd;
    }

    public void setTripEnd(String tripEnd) {
        TripEnd = tripEnd;
    }

    public JourneyHistory(String toDateTime, String fromDateTime, String vehicleReg, String tripId, String TimeZone , String clientId , String unit) {

        this.fromDateTime = fromDateTime;
        this.toDateTime = toDateTime;
        this.VehicleReg = vehicleReg;
        this.tripId = tripId;
        this.TimeZone = TimeZone;
        this.clientId = clientId;
        this.unit = unit;
        this.TripEnd = unit;
        this.TripStart = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public Integer getAngle() {
        return angle;
    }

    public void setAngle(Integer angle) {
        this.angle = angle;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

}
