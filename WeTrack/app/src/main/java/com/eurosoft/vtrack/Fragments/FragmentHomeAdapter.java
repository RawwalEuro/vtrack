package com.eurosoft.vtrack.Fragments;

import android.content.Context;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentHomeAdapter extends FragmentPagerAdapter {

    Context context;
    ArrayList<Fragment> fragments;
    private Map<Integer, String> mFragmentTags;
    private FragmentManager mFragmentManager;

    public FragmentHomeAdapter(FragmentManager fm, Context context, ArrayList<Fragment> fragments) {
        super(fm);
        mFragmentManager = fm;
        this.context = context;
        this.fragments = fragments;
        mFragmentTags = new HashMap<Integer, String>();
    }


    @NonNull
    @NotNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            String tag = fragment.getTag();
            mFragmentTags.put(position, tag);
        }
        return object;
    }

    public Fragment getFragment(int position) {
        Fragment fragment = null;
        String tag = mFragmentTags.get(position);
        if (tag != null) {
            fragment = mFragmentManager.findFragmentByTag(tag);
        }
        return fragment;
    }
}
