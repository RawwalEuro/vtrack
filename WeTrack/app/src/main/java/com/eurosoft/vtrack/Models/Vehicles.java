package com.eurosoft.vtrack.Models;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Vehicles implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "vehicleId")
    private String vehicleId;



    @ColumnInfo(name = "vehicleReg")
    private String vehicleReg;

    @ColumnInfo(name = "vehicleMake")
    private String vehicleMake;

    @ColumnInfo(name = "vehicleModel")
    private String vehicleModel;


    public Vehicles(String vehicleId, String vehicleReg, String vehicleMake, String vehicleModel) {
        this.vehicleId = vehicleId;
        this.vehicleReg = vehicleReg;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public void setVehicleReg(String vehicleReg) {
        this.vehicleReg = vehicleReg;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }
}
