package com.eurosoft.vtrack.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Models.Zone;
import com.eurosoft.vtrack.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterEventTypes extends RecyclerView.Adapter<AdapterEventTypes.ViewHolder> {


    private OnItemClickListenerEvent mOnItemClickListener;
    private Context context;
    private List<Zone> zonesList;
    int row_index = 0;

    public AdapterEventTypes(Context ctx,List<Zone> zones, AdapterEventTypes.OnItemClickListenerEvent onItemClickListener) {
        context = ctx;
        zonesList = zones;
        mOnItemClickListener = onItemClickListener;
    }


    public interface OnItemClickListenerEvent {
        public void onItemClick(View view, int position,Zone zone);
    }

    @NonNull
    @Override
    public AdapterEventTypes.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_event_types, parent, false);
        AdapterEventTypes.ViewHolder vh = new AdapterEventTypes.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterEventTypes.ViewHolder holder, int position) {

        Zone zone = zonesList.get(position);
        holder.zoneType.setText(zone.getId() + " " + zone.getCount());
        holder.rlBg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(),zone);
                row_index = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {
            holder.rlBg.setBackgroundColor(Color.parseColor("#03884A"));
            holder.zoneType.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.rlBg.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.zoneType.setTextColor(Color.parseColor("#000000"));
        }


    }

    @Override
    public int getItemCount() {
        return zonesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView zoneType;
        private RelativeLayout rlBg;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            zoneType = itemView.findViewById(R.id.zoneType);
            rlBg = itemView.findViewById(R.id.containerZone);

        }
    }
}
