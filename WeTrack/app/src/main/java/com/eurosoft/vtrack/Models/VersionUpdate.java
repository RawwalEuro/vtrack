package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VersionUpdate implements Serializable {


    @SerializedName("clientId")
    @Expose
    private String clientId;


    @SerializedName("isUpdate")
    @Expose
    private boolean isUpdate;

    @SerializedName("priority")
    @Expose
    private int priority;

    @SerializedName("__v")
    @Expose
    private int __v;

    public String getClientId() {
        return clientId;
    }

    public boolean isUpdate() {


        return isUpdate;
    }

    public int getPriority() {


        if(priority == 0){
            return 0;
        }
        return priority;
    }

    public int get__v() {
        return __v;
    }
    public VersionUpdate(String clientId) {
        this.clientId = clientId;
    }


    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }
}
