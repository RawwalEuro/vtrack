package com.eurosoft.vtrack.Utils.NetworkUtils;

import com.fxn.stash.Stash;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.eurosoft.vtrack.Constants.BASE_URL;
import static com.eurosoft.vtrack.Constants.GET_BACKEND_URL;
import static com.eurosoft.vtrack.Constants.GET_REPORT_URL;


public class APIClient {

    private static APIClient instance;
    private static Retrofit retrofit = null;

    private APIClient() {
    }

    public static APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }
    final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    public static Retrofit getClient(String url) {
         String BACKEND_URL= Stash.getString(GET_BACKEND_URL);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BACKEND_URL + url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            return retrofit;
    }
    public static Retrofit getApiBaseClient() {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            return retrofit;
    }
    public static Retrofit getApiReportClient() {
        String REPORT_URL= Stash.getString(GET_REPORT_URL)+"/api/";
            retrofit = new Retrofit.Builder()
                    .baseUrl(REPORT_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            return retrofit;
    }



}