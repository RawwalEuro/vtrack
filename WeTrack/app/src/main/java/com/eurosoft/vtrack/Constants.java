package com.eurosoft.vtrack;

public class Constants {

/*    public static final String BASE_URL = "http://88.198.109.106:4001/api/v1/";
    public static final String TEST_URL = "http://88.198.109.106:4001/api/v1/";
     public static final String isAuthorized = "isAuthorized";
   public static final String REPORT_URL = "https://reports.vtracksolutions.com/api/";
   public static final String BACKEND_URL = "https://backend.vtracksolutions.com/";
  public static final String LIVEAPI_URL = "https://live.vtracksolutions.com/graphql";*/

    public static final String isLoggenIn = "isLoggenIn";
    public static final String USER_CLIENT_ID = "USER_CLIENT_ID";
    public static final String USER = "USER";
    public static final String STARTED_DATE = "STARTED_DATE";
    public static final String END_DATE = "END_DATE";
    public static final String VEH_REG = "VEH_REG";
    public static final String URL_FETCHED = "URL_FETCHED";
    public static final String SHOW_BACK_BTN = "SHOW_BACK_BTN";
    public static final int HOME_TAB = 0;
    public static final int TRACKING_HISTORY_TAB = 1;
    public static final int NOTIFICATIONS_TAB = 2;
    public static final String TIME_ZONE = "TIME_ZONE";
    public static final String LIST_NOTIFICATION = "LIST_NOTIFICATION";
    public static final String JOURNEY_DATA = "JOURNEY_DATA";
    public static final String LANGUAGE_DATA = "LANGUAGE_DATA";
    public static final String BASE_URL = "http://eurlic.co.uk/license/api/Cab/";
    public static final String GET_REPORT_URL = "GET_REPORT_URL";
    public static final String GET_BACKEND_URL = "GET_BACKEND_URL";
    public static final String GET_LIVEAPI_URL = "GET_LIVEAPI_URL";
    public static final String GET_OPENSTREET_URL = "GET_OPENSTREET_URL";
    public static final String DATETIME_FORMAT_FROMBACKEND = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATETIME_FORMATFROMBACKEND_ACTIVITYTRACK = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATETIME_FORMATFROMBACKEND_HOMEFRAGEMENT = "MMMM dd yyyy hh:mm:ss aa";
    public static final String DATETIME_FORMATFROMBACKEND_ADAPTERLISTJOURNEY = "yyyy-MM-dd hh:mm:ss";
    public static final String DATABASE_NAME="WeTrack";
    public static final String EXCEPTION_TAG ="ExceptionTag";
    public static final String APIV1="/api/v1/";
    public static final String APIV2="/v2/";
    public static final String ClientName="ClientName";
    public static final String VehicleDetails="VehicleDetails";
    public static final String IsSatelliteView="IsSatelliteView";



}
