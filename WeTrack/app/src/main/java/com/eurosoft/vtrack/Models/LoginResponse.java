package com.eurosoft.vtrack.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("ClientId")
    @Expose
    private String clientId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("clientLanguage")
    @Expose
    private String clientLanguage;
    @SerializedName("MapType")
    @Expose
    private String mapType;
    @SerializedName("userLanguage")
    @Expose
    private String userLanguage;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("timezone")
    @Expose
    private String timezone;

    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("languageDict")
    @Expose
    private String languageDict;
    @SerializedName("ClientName")
    @Expose
    private String ClientName;

    public String getClientName() {
        return ClientName;
    }

    public String getLanguageDict() {
        return languageDict;
    }

    public LoginResponse(String password, String userName, String ClientId) {
        this.password = password;
        this.userName = userName;
        this.clientId = ClientId;
    }




    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public LoginResponse() {
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientLanguage() {
        return clientLanguage;
    }

    public void setClientLanguage(String clientLanguage) {
        this.clientLanguage = clientLanguage;
    }

    public String getMapType() {
        return mapType;
    }

    public void setMapType(String mapType) {
        this.mapType = mapType;
    }

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }
}
