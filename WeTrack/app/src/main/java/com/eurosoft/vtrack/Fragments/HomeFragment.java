package com.eurosoft.vtrack.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Adapters.AdapterHomeVehicles;
import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.MainActivity;
import com.eurosoft.vtrack.Models.LiveMapsModel;
import com.eurosoft.vtrack.Models.LocationResponseModel;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.VehicleStatusCode;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.Models.WebResponseString;
import com.eurosoft.vtrack.R;
import com.eurosoft.vtrack.Utils.NetworkAvailable.AppStatus;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.eurosoft.vtrack.Constants.DATETIME_FORMATFROMBACKEND_HOMEFRAGEMENT;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;
import static com.eurosoft.vtrack.Constants.GET_LIVEAPI_URL;
import static com.eurosoft.vtrack.Constants.GET_OPENSTREET_URL;
import static com.eurosoft.vtrack.Constants.IsSatelliteView;


public class HomeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, MainActivity.onFilterClick, MainActivity.onDrawerListener
        , MainActivity.onThreadListener {

    private GoogleMap mMap;
    private ArrayList<LiveMapsModel> liveMapsList = new ArrayList<>();
    private List<Vehicles> vehiclesList = new ArrayList<>();
    private List<LiveMapsModel> filterMaplist = new ArrayList<>();
    private ArrayList<Integer> noOfVehicleStatus = new ArrayList<>();
    private ArrayList<VehicleStatusCode> vehicleDetails = new ArrayList<>();
    private Marker myMarker;
    private ViewDialog alertDialoge;
    private RecyclerView rvVehicles;
    private AdapterHomeVehicles mAdapter;
    private LinearLayoutManager layoutManager;
    public  DrawerLayout mDrawer;
    private ImageView navIcon;
    private Button currentLocation;
    private LoginResponse currentUser;
    private Boolean isClick = false,isCarSelected = false,isDataInsertinDb = false,onfirstlaunch = true,onClickAllVehicle = true,isAnimated = false,vehicleAutoUpdate=true,isDefaultMap=true;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout rlProgressBar,selected_cars_RR;
    private String clientId,vehiclenotemp = "";
    private MasterPojo masterPojo;
    private int vehiclePosition = 0;
    private LinearLayout carMenuLL,LocationLayout;
    private TextView showAllCar, vehicleno,LocationName;
    private EditText filterlist;
    private CardView select_car;
    private Thread thread;
    private LoginResponse user;
    ImageView switch_map;
    TextView map_text;
    CardView map_cardview;
    public static HomeFragment newInstance() {
        return new HomeFragment();
    }


    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Stash.init(getActivity());
        currentUser = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        clientId = Stash.getString(Constants.USER_CLIENT_ID);
        alertDialoge = new ViewDialog();
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mDrawer = (DrawerLayout) view.findViewById(R.id.drawer);
        navIcon = (ImageView) view.findViewById(R.id.navIcon);
        LocationLayout = (LinearLayout) view.findViewById(R.id.locationlayout);
        LocationName = (TextView) view.findViewById(R.id.location_name);
        mDrawer = (DrawerLayout) view.findViewById(R.id.drawer);
        rlProgressBar = view.findViewById(R.id.rlProgressBar);
        circularProgressBar = view.findViewById(R.id.circularProgressBar);
        masterPojo = (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA, MasterPojo.class);
        TextView pleasewait_text = (TextView) view.findViewById(R.id.pleasewait_text);
        TextView message = (TextView) view.findViewById(R.id.message);
        pleasewait_text.setText(masterPojo.getPleasewait());
        message.setText(masterPojo.getWearelookingforyourvehicledetails());
        carMenuLL = (LinearLayout) view.findViewById(R.id.car_menu_LL);
        mapFragment.getMapAsync(this);
        rvVehicles = view.findViewById(R.id.rvVehicles);
        rvVehicles.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvVehicles.setLayoutManager(layoutManager);
        showAllCar = (TextView) view.findViewById(R.id.show_all);
        vehicleno = (TextView) view.findViewById(R.id.vehicleno);
        filterlist = (EditText) view.findViewById(R.id.filterlist);
        filterlist.setHint(masterPojo.getSearch());
        showAllCar.setText(masterPojo.getShowAll());
        selected_cars_RR = (RelativeLayout) view.findViewById(R.id.selected_cars_RR);
        select_car = (CardView) view.findViewById(R.id.select_car);
        currentLocation = (Button) view.findViewById(R.id.mylocation);
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        vehicleDetails = Stash.getArrayList(Constants.VehicleDetails, VehicleStatusCode.class);
         switch_map= view.findViewById(R.id.switch_map);
         map_text=view.findViewById(R.id.map_text);
         map_cardview=view.findViewById(R.id.map_cardview);
        map_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(mMap!=null){
                        if(!isDefaultMap) {
                            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                            isDefaultMap=true;
                            map_cardview.getBackground().setTint(getActivity().getResources().getColor(R.color.white));
                            map_text.setTextColor(getActivity().getResources().getColor(R.color.black));
                            switch_map.setColorFilter(getActivity().getResources().getColor(R.color.black));
                            Stash.put(IsSatelliteView,false);

                        }
                        else {
                            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE); // Here is where you set the map type
                            isDefaultMap=false;
                            map_text.setTextColor(getActivity().getResources().getColor(R.color.white));
                            map_cardview.getBackground().setTint(Color.parseColor("#03884A"));
                            switch_map.setColorFilter(getActivity().getResources().getColor(R.color.white));
                            Stash.put(IsSatelliteView,true);

                        }
                    }
                }
                catch (Exception e){

                }

            }
        });
        return view;
    }

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
        }
    };


    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        mMap = googleMap;
        if(Stash.getBoolean(IsSatelliteView)){
            mMap.setMapType(mMap.MAP_TYPE_SATELLITE); // Here is where you set the map type
            isDefaultMap=false;
            map_text.setTextColor(getActivity().getResources().getColor(R.color.white));
            map_cardview.getBackground().setTint(Color.parseColor("#03884A"));
            switch_map.setColorFilter(getActivity().getResources().getColor(R.color.white));
        }
        else {
            mMap.setMapType(mMap.MAP_TYPE_NORMAL); // Here is where you set the map type
            isDefaultMap=true;
            map_cardview.getBackground().setTint(getActivity().getResources().getColor(R.color.white));
            map_text.setTextColor(getActivity().getResources().getColor(R.color.black));
            switch_map.setColorFilter(getActivity().getResources().getColor(R.color.black));
        }

    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        markerObject((LiveMapsModel) marker.getTag());
        return false;
    }


    public void closeDrawer() {
        Toasty.success(getActivity(), "Sucess", Toasty.LENGTH_LONG).show();
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    public void callApiOkHttp() {
        try {
            if (getActivity() == null)
                return;
            JSONObject json = new JSONObject();
            Log.e("HomeFragment", "clientId" + clientId + "");
            //60705b8108db5718c8b499fd
            json.put("query", "\n query {\n Currentlocation(id:\"" + clientId + "\"" + "){\n id,\n Value\n \n }\n }");
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.connectTimeout(8, TimeUnit.SECONDS);
            clientBuilder.readTimeout(8, TimeUnit.SECONDS);
            OkHttpClient client = clientBuilder.build();
            String url = Stash.getString(GET_LIVEAPI_URL);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), json.toString());
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            if (!AppStatus.getInstance(getActivity()).isOnline()) {
                carMenuLL.setVisibility(View.GONE);
                selected_cars_RR.setVisibility(View.GONE);
                LocationLayout.setVisibility(View.GONE);
                Toasty.warning(getActivity(), masterPojo.getPleasecheckyourinternet(), Toasty.LENGTH_SHORT).show();
                return;
            }

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity().getApplicationContext(), "Failure Response:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response rawResponse) throws IOException {
                    try {
                        liveMapsList = new ArrayList<>();
                       /* getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity().getApplicationContext(), "Data arrived from server", Toast.LENGTH_SHORT).show();
                            }
                        });*/
                        String response = rawResponse.body().string();
                        if (response != null && !response.equals("")) {
                            if (!isDataInsertinDb) {
                                deleteAllVehicles();
                            }
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObjectArray = new JSONObject(jsonObject.getJSONObject("data").getJSONObject("Currentlocation").getString("Value"));
                            final JSONArray myResponse = new JSONArray(jsonObjectArray.getJSONArray("cacheList").toString());
                            vehiclesList = new ArrayList<>();
                            for (int i = 0; i < myResponse.length(); i++) {
                                // Assuming your JSONArray contains JSONObjects
                                JSONObject entry = myResponse.getJSONObject(i);
                                Log.e("HomeFragment", "entry" + entry.toString());
                                LiveMapsModel liveMapsModel = new LiveMapsModel(
                                        entry.getJSONObject("gps").getDouble("longitude"),
                                        entry.getJSONObject("gps").getDouble("latitude"),
                                        entry.getJSONObject("gps").getInt("Angle"),
                                        entry.getJSONObject("gps").getInt("speed"),
                                        entry.getString("timestamp"),
                                        entry.getString("vehicleReg"),
                                        entry.getInt("ignition"),
                                        entry.getString("vehicleId"),
                                        entry.getString("vehicleMake"),
                                        entry.getString("vehicleModel"),
                                        entry.getJSONObject("gps").getString("speedWithUnitDesc"),
                                        entry.getString("vehicleStatusCode")

                                );
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (entry.getString("vehicleStatusCode") == null) {
                                                Toast.makeText(getActivity(), "Status null " + entry.getString("vehicleReg"), Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            Log.e(EXCEPTION_TAG, e.getMessage());
                                        }
                                    }
                                });

                                Log.e("HomeFragment", "entry" + entry.toString());
                                Log.e("HomeFragment", "speed" + entry.getJSONObject("gps").getDouble("speed"));
                                Log.e("HomeFragment", "No of Car " + liveMapsList.size());
                                liveMapsModel.setSpeedUnit(entry.getJSONObject("gps").getString("speedUnit").toString());
                                liveMapsList.add(liveMapsModel);

                            }
                            Collections.sort(liveMapsList, new CustomComparator());
                            updateMarker(liveMapsList);
                            if (!isDataInsertinDb) {
                                getVehicles(liveMapsList);
                            }

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ifUserHaveManyCar(liveMapsList);
                                }
                            });
                        }

                    } catch (Exception e) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Toasty.warning(getActivity(), e.getMessage(), Toasty.LENGTH_SHORT).show();
//                            }
//                        });
                        Log.e(EXCEPTION_TAG, "" + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toasty.warning(getActivity(), e.getMessage(), Toasty.LENGTH_SHORT).show();
//                }
//            });
                 Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }
    }


    private void deleteAllVehicles() {
        class DeleteVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {

                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);

            }

        }
        DeleteVehiclesList gt = new DeleteVehiclesList();
        gt.execute();
    }

    private void markerObject(LiveMapsModel mapsModel) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (rlProgressBar.getVisibility() == View.VISIBLE)
                    return;
                try {
                    callApiForDisplayNameMarker(mapsModel);
                    // callApiForLatLongName(mapsModel);
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                }

            }
        });
    }


    private void callApiForLatLongName(LiveMapsModel liveMapsModel) {
        progressVisiblityVisible();

        if (liveMapsModel == null) {
            progressVisiblityGone();
            return;
        }


        if (liveMapsModel.getLatitude() == null || liveMapsModel.getLongitude() == null) {
            progressVisiblityGone();
            return;
        }

        OkHttpClient client = new OkHttpClient();
        String openstreeturl = Stash.getString(GET_OPENSTREET_URL);
        String url = openstreeturl + "/nominatim/reverse.php?lat=" + "" + liveMapsModel.getLatitude() + "" + "&lon=" + liveMapsModel.getLongitude() + "&zoom=19&format=jsonv2";
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                progressVisiblityGone();
                                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                            } catch (Exception ew) {
                                Log.e(EXCEPTION_TAG, "" + ew.getMessage());
                                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                            }

                        }
                    });

                }

                @Override
                public void onResponse(@NotNull okhttp3.Call call, @NotNull okhttp3.Response response) throws IOException {

                    final String myResponse = response.body().string();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject = new JSONObject(myResponse);
                                String displayName = jsonObject.getString("display_name");
                                Log.e("HomeFragment", "Response" + jsonObject.toString());
                                if (liveMapsModel != null) {
                                    progressVisiblityGone();
                                    alertDialoge.showDialog(getActivity(), liveMapsModel, displayName);
                                }
                            } catch (ParseException | JSONException e) {
                                Log.e(EXCEPTION_TAG, "" + e.getMessage());
                                Toasty.error(getActivity(), e.getMessage(), Toast.LENGTH_SHORT, true).show();

                            }

                        }
                    });
                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            Toasty.error(getActivity(), e.getMessage(), Toast.LENGTH_SHORT, true).show();
        }
    }


    private void updateMarker(ArrayList<LiveMapsModel> liveMapsList) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (myMarker != null) {
                        mMap.clear();
                    }
                    if (liveMapsList.size() == 1) {
                        ifonevehiclemarker();
                    } else {
                        ifmorethanonevechile();
                    }
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (handler != null) {
                handler.removeCallbacks(runnable1);
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            if (currentState) {
                callAsynchronousTask();
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
    }

    private void animateCameraToCar(LiveMapsModel liveMapsModel) {

        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(liveMapsModel.getLatitude(),
                            liveMapsModel.getLongitude()), 16), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    isAnimated=false;
                }

                @Override
                public void onCancel() {

                }
            });

        } else {
            return;
        }
    }

    public Bitmap createCustomPickDropMarker(Context context, String vehicleNo, int speed, int ignition, int angle, String speednit, String vehiclename
            , String speedunitindesr, String statuscode) {
        mMap.setOnMarkerClickListener(this);
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_pick_drop, null);
        TextView txt_name_sub = marker.findViewById(R.id.snippet);
        TextView speedview = marker.findViewById(R.id.speed);
        ImageView car = marker.findViewById(R.id.carStop);
        loadImageFromStorage("", car, statuscode, angle, speedview, speedunitindesr, speed, ignition);
        txt_name_sub.setText(vehicleNo);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);
        return bitmap;
    }


    @Override
    public void setOnFilterClickListener() {
        if (!isClick) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressVisiblityGone();
                            isClick = false;
                        }
                    }, 3000);
                    progressVisiblityVisible();
                    try {
                        isClick = true;
                        //   callApiOkHttps();
                    } catch (Exception e) {
                        Log.e(EXCEPTION_TAG, "" + e.getMessage());
                        isClick = false;
                    }
                }
            });
        }
    }


    public class ViewDialog {

        public void showDialog(Activity activity, LiveMapsModel liveMapsModel, String displayName) throws ParseException {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_vehicles_detail);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView vehicleNo = (TextView) dialog.findViewById(R.id.vehicleNo);
            TextView lat = (TextView) dialog.findViewById(R.id.lat);
            TextView longitude = (TextView) dialog.findViewById(R.id.longitude);
            TextView angle = (TextView) dialog.findViewById(R.id.angle);
            TextView speed = (TextView) dialog.findViewById(R.id.speed);
            TextView ignition = (TextView) dialog.findViewById(R.id.ignition);
            Button btnClose = (Button) dialog.findViewById(R.id.btnClose);
            TextView dateTime = (TextView) dialog.findViewById(R.id.dateTime);
            TextView date = (TextView) dialog.findViewById(R.id.date);
            TextView locationName = (TextView) dialog.findViewById(R.id.locationName);
            Button btnShareLoc = (Button) dialog.findViewById(R.id.btnShareLoc);
            vehicleNo.setText(liveMapsModel.getVehicleNo());
            lat.setText(liveMapsModel.getLatitude() + "");
            longitude.setText(liveMapsModel.getLongitude() + "");
            angle.setText(liveMapsModel.getAngle() + "");
            speed.setText(liveMapsModel.getSpeedWithUnitDesc());
            ignition.setText(liveMapsModel.getIgnition() + "");
            dateTime.setText(converTimeFormat(liveMapsModel.getTimestamp()) + "");
            date.setText(convertDateFormat(liveMapsModel.getTimestamp()) + "");
            locationName.setText(displayName + "");
            btnShareLoc.setText(masterPojo.getShare());
            btnClose.setText(masterPojo.getClose());

            btnShareLoc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    /*This will be the actual content you wish you share.*/
                    String shareBody = masterPojo.getThisismycar() + liveMapsModel.getVehicleNo() + masterPojo.getLocationanditscurrentlyonthisaddress() + displayName + "\n" +
                            "http://maps.google.com/maps?saddr=" + liveMapsModel.getLatitude() + "," + liveMapsModel.getLongitude();
                    /*The type of the content is text, obviously.*/
                    intent.setType("text/plain");
                    /*Applying information Subject and Body.*/
                    intent.putExtra(android.content.Intent.EXTRA_SUBJECT, masterPojo.getThisismycar() + liveMapsModel.getVehicleNo() + masterPojo.getLocation());
                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    /*Fire!*/
                    startActivity(Intent.createChooser(intent, masterPojo.getThisismycar() + liveMapsModel.getVehicleNo() + masterPojo.getLocation()));
                }
            });

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }


    public String convertDateFormat(String date) throws ParseException {
        Log.e("HomeFragment", "date" + date);
        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMATFROMBACKEND_HOMEFRAGEMENT);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMMM dd yyyy");
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }


    public String converTimeFormat(String date) throws ParseException {
        Log.e("HomeFragment", "date" + date);
        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMATFROMBACKEND_HOMEFRAGEMENT);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm:ss aa");
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }

    private class TimeToGetStatus extends TimerTask {
        @Override
        public void run() {
            thread = new Thread() {
                @Override
                public void run() {
                    callApiOkHttp();
                    Log.e("TimerUpdates", "running");
                }
            };
            thread.start();
        }
    }


    private void addVehicleToDb(Vehicles vehicles) {
        class AddVehicle extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getContext()).getAppDatabase()
                        .vehiclesDao()
                        .insert(vehicles);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.e("HomeFragment", "TRUE");
                isDataInsertinDb = true;

            }
        }

        AddVehicle st = new AddVehicle();
        st.execute();
    }


    private void getVehicles(ArrayList<LiveMapsModel> liveMapsModels) {
        class GetVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                vehiclesList = DatabaseClient
                        .getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .getAll();

                return vehiclesList;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                Vehicles vehicles = null;
                try {
                    for (int i = 0; i < liveMapsModels.size(); i++) {
                        vehicles = new Vehicles(liveMapsModels.get(i).getVehicleId(),
                                liveMapsModels.get(i).getVehicleReg(),
                                liveMapsModels.get(i).getVehicleMake(),
                                liveMapsModels.get(i).getVehicleModel());
                        addVehicleToDb(vehicles);
                    }
                    isDataInsertinDb = true;
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                }
            }
        }
        GetVehiclesList gt = new GetVehiclesList();
        gt.execute();
    }

    public void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }


    public void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }


    private void ifonevehiclemarker() {


        try {
            LatLng latLng = new LatLng(liveMapsList.get(0).getLatitude(),
                    liveMapsList.get(0).getLongitude());
            myMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromBitmap(createCustomPickDropMarker(getActivity(),
                            liveMapsList.get(0).getVehicleNo(),
                            liveMapsList.get(0).getSpeed(), liveMapsList.get(0).getIgnition(), liveMapsList.get(0).getAngle(), liveMapsList.get(0).getSpeedUnit(), liveMapsList.get(0).getVehicleNo()
                            , liveMapsList.get(0).getSpeedWithUnitDesc(), liveMapsList.get(0).getVehicleStatusCode()))));

            myMarker.setTag(liveMapsList.get(0));

            if (onfirstlaunch) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(liveMapsList.get(0).getLatitude(),
                                liveMapsList.get(0).getLongitude()), 16));
                updatelocation();
                callApiForDisplayName(liveMapsList.get(0));
                onfirstlaunch = false;
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(liveMapsList.get(0).getLatitude(),
                                liveMapsList.get(0).getLongitude()), mMap.getCameraPosition().zoom));
                updatelocation();
                callApiForDisplayName(liveMapsList.get(0));
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
            Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void ifmorethanonevechile() {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            noOfVehicleStatus = new ArrayList<>();
            for (int i = 0; i < liveMapsList.size(); i++) {
                LatLng latLng = new LatLng(liveMapsList.get(i).getLatitude(),
                        liveMapsList.get(i).getLongitude());
                myMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .anchor(0.5f, 0.5f)
                        .icon(BitmapDescriptorFactory.fromBitmap(createCustomPickDropMarker(getActivity(),
                                liveMapsList.get(i).getVehicleNo(),
                                liveMapsList.get(i).getSpeed(), liveMapsList.get(i).getIgnition(), liveMapsList.get(i).getAngle(), liveMapsList.get(i).getSpeedUnit(), liveMapsList.get(i).getVehicleNo()
                                , liveMapsList.get(i).getSpeedWithUnitDesc(), liveMapsList.get(i).getVehicleStatusCode()))));

                myMarker.setFlat(true);
                myMarker.setTag(liveMapsList.get(i));
                builder.include(myMarker.getPosition());

            }
            multipleCarCondition(builder);

        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
            Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    private void ifUserHaveManyCar(List<LiveMapsModel> vehiclesList) {
        try {
            if (vehiclesList.size() > 1) {
                selected_cars_RR.setVisibility(View.VISIBLE);
                if (filterMaplist.size() == 0) {
                    onDataUpdate(vehiclesList);
                } else {
                    onDataUpdate(filterMaplist);
                }
            } else {
                carMenuLL.setVisibility(View.GONE);
                selected_cars_RR.setVisibility(View.GONE);
            }

            showAllCar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isCarSelected = false;
                    filterlist.setText("");
                    filterMaplist.clear();
                    LocationLayout.setVisibility(View.GONE);
                    setVehicle();
                    onClickAllVehicle = true;
                    ifmorethanonevechile();
                    closeKeyboard(filterlist);
                    carMenuLL.setVisibility(View.GONE);
                }
            });

            selected_cars_RR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppStatus.getInstance(getActivity()).isOnline()) {
                        carMenuLL.setVisibility(View.GONE);
                        selected_cars_RR.setVisibility(View.GONE);
                        Toasty.warning(getActivity(), masterPojo.getPleasecheckyourinternet(), Toasty.LENGTH_SHORT).show();
                        return;
                    }
                    mAdapter.notifyDataSetChanged();
                    if (carMenuLL.getVisibility() == View.VISIBLE) {
                        carMenuLL.setVisibility(View.GONE);
                    } else {
                        selected_cars_RR.setVisibility(View.VISIBLE);
                        carMenuLL.setVisibility(View.VISIBLE);

                    }


                }
            });

            currentLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!AppStatus.getInstance(getActivity()).isOnline()) {
                        Toasty.warning(getActivity(), masterPojo.getPleasecheckyourinternet(), Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    if (filterlist.getText().toString().isEmpty()) {
                        if (!isCarSelected) {
                            //  isCarSelected=false;
                            onClickAllVehicle = true;
                            ifmorethanonevechile();
                        } else {
                            isAnimated=true;
                            if (filterMaplist.size() == 0) {
                                // isCarSelected = true;
                                LiveMapsModel liveMapsModel = liveMapsList.get(vehiclePosition);
                                animateCameraToCar(liveMapsModel);
                                updatelocation();
                                callApiForDisplayName(liveMapsModel);
                            } else {
                                //  isCarSelected = true;
                                LiveMapsModel liveMapsModel = filterMaplist.get(vehiclePosition);
                                animateCameraToCar(liveMapsModel);
                                updatelocation();
                                callApiForDisplayName(liveMapsModel);

                            }

                        }
                        closeKeyboard(filterlist);

                    } else {
                        Toasty.warning(getActivity(), masterPojo.getPleaseselectanyCar(), Toasty.LENGTH_SHORT).show();
                    }
                }
            });

            if (filterlist.getText().toString().isEmpty()) {
                mAdapter = new AdapterHomeVehicles(getActivity(), vehiclesList, new AdapterHomeVehicles.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, LinearLayout layout, String mvehicleno, List<LiveMapsModel> FilterList, String vehiclecode) {
                        switch (view.getId()) {
                            case R.id.mainLL:
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            isCarSelected = true;
                                            carMenuLL.setVisibility(View.GONE);
                                            isAnimated=true;
                                            vehiclenotemp = mvehicleno;
                                            vehiclePosition = position;
                                            setVehicleColor(mvehicleno, vehiclecode);
                                            closeKeyboard(filterlist);
                                            Log.e("HomeFragment", "position" + position);
                                            if (filterlist.getText().toString().isEmpty()) {
                                                LiveMapsModel liveMapsModel = liveMapsList.get(position);
                                                animateCameraToCar(liveMapsModel);
                                                updatelocation();
                                                callApiForDisplayName(liveMapsModel);
                                                filterlist.setText("");
                                                filterMaplist.clear();
                                            } else {
                                                filterMaplist = FilterList;
                                                LiveMapsModel liveMapsModel = FilterList.get(position);
                                                animateCameraToCar(liveMapsModel);
                                                updatelocation();
                                                callApiForDisplayName(liveMapsModel);
                                                filterlist.setText("");
                                            }
                                        } catch (Exception e) {
                                            Log.e(EXCEPTION_TAG, "EXCEPTION_TAG" + e.getMessage());
                                        }
                                    }
                                });
                                break;
                        }
                    }
                }, vehicleDetails);
                rvVehicles.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            filterlist.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    filterdata(s.toString(), vehiclesList, noOfVehicleStatus);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "EXCEPTION_TAG" + e.getMessage());
        }
    }

    private void filterdata(String text, List<LiveMapsModel> vehiclesList, List<Integer> mcolorList) {
        try {
            List<LiveMapsModel> filteredList = new ArrayList<>();

            for (LiveMapsModel item : vehiclesList) {
                if (item.getVehicleNo().toLowerCase().startsWith(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }

            if (mAdapter != null) {
                mAdapter.filterList(filteredList);
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }

    }

    private void setVehicleColor(String mvehicleno, String statuscode) {
        selected_cars_RR.setVisibility(View.VISIBLE);
        vehicleno.setText(mvehicleno);
        Log.e("HomeFragment", "position" + vehiclePosition);
        Log.e("HomeFragment", "vehicleDetails" + vehicleDetails);
        for (int i = 0; i < vehicleDetails.size(); i++) {
            if (statuscode.equals(vehicleDetails.get(i).getVehicleStatusCode())) {
                select_car.setCardBackgroundColor(Color.parseColor(vehicleDetails.get(i).getVehicleColor()));
                vehicleno.getBackground().setTint(Color.parseColor(vehicleDetails.get(i).getVehicleColor()));
                vehicleno.setTextColor(Color.parseColor(vehicleDetails.get(i).getVehicleTextColor()));
                stopCarLocationUpdateonParked(i);
                break;
            }
        }
    }

    public void multipleCarCondition(LatLngBounds.Builder builder) {
        if (filterMaplist.size() == 0) {
            if (!isCarSelected) {
                LatLngBounds bounds = builder.build();
                int padding = 200; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                if (onClickAllVehicle) {
                    mMap.animateCamera(cu);
                    onClickAllVehicle = false;
                }
                LocationLayout.setVisibility(View.GONE);
            } else if (isCarSelected) {
                if(!isAnimated) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(liveMapsList.get(vehiclePosition).getLatitude(),
                                    liveMapsList.get(vehiclePosition).getLongitude()), mMap.getCameraPosition().zoom));

                    if(vehicleAutoUpdate) {
                        updatelocation();
                        callApiForDisplayName(liveMapsList.get(vehiclePosition));
                    }
                    else {
                        Log.e("HomeFragment","Auto Location Update is Off");
                    }
                }

            }
        } else {
            if (isCarSelected) {
                if(!isAnimated) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(filterMaplist.get(vehiclePosition).getLatitude(),
                                        filterMaplist.get(vehiclePosition).getLongitude()), mMap.getCameraPosition().zoom));
                    if (vehicleAutoUpdate) {
                        updatelocation();
                        callApiForDisplayName(filterMaplist.get(vehiclePosition));
                    }
                    else {
                        Log.e("HomeFragment","Auto Location Update is Off");
                    }
                }
            }
        }
    }

    public void closeKeyboard(View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.closeKeyboard();

    }

    @Override
    public void setonDrawerListener(Boolean state) {
        try {

            if (state) {
                mMap.getUiSettings().setAllGesturesEnabled(false);
            } else {
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
    }

    public class CustomComparator implements Comparator<LiveMapsModel> {
        @Override
        public int compare(LiveMapsModel o1, LiveMapsModel o2) {
            return o1.getVehicleNo().compareTo(o2.getVehicleNo());
        }
    }


    private void specificCarCase(int speed, int angle, int ignition, ImageView car, LinearLayout mainRl, String vehiclename) {
        //  carStop.setVisibility(View.GONE);
        car.setVisibility(View.VISIBLE);
        Log.e("HomeFragment", "angle" + angle);
        Matrix mat = new Matrix();
        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.movecar);
        mat.postRotate(Integer.parseInt(String.valueOf(angle)));
        Bitmap bMapRotate = Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(), bMap.getHeight(), mat, true);
        car.setImageBitmap(bMapRotate);
        //  carPause.setVisibility(View.GONE);
    }

    boolean currentState = true;

    @Override
    public void setonThreadListener(Boolean state) {
        try {
            currentState = state;
            Log.e("HomeFragment", "TimerUpdates:" + state.toString());
            if (state) {
                callAsynchronousTask();
            } else {
                if (handler != null) {
                    handler.removeCallbacks(runnable1);
                    handler.removeCallbacksAndMessages(null);
                }
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }

    }

    Runnable runnable1 = new Runnable() {
        @Override
        public void run() {
            try {
                if (getActivity() != null) {
                    callApiOkHttp();
                    handler.postDelayed(runnable1, 10000);
                }
            } catch (Exception e) {
                Log.e(EXCEPTION_TAG, e.getMessage());
            }
        }
    };

    public void callAsynchronousTask() {

        if (handler != null) {
            handler.removeCallbacks(runnable1);
            handler.post(runnable1);
        }

    }

    public void setVehicle() {
        vehicleno.setText(masterPojo.getAllVehicles());
        LocationLayout.setVisibility(View.GONE);
        select_car.setCardBackgroundColor(Color.parseColor("#000000"));
        vehicleno.setBackground(getActivity().getDrawable(R.drawable.round_vehicle_black_button));
        vehicleno.setTextColor(Color.parseColor("#FFFFFF"));
    }

    public void onDataUpdate(List<LiveMapsModel> vehiclesList) {
        if (isCarSelected && !vehiclenotemp.equals("")) {
            if (filterMaplist.size() == 0)
                setVehicleColor(vehiclenotemp, vehiclesList.get(vehiclePosition).getVehicleStatusCode());
            else {
                setVehicleColor(vehiclenotemp, filterMaplist.get(vehiclePosition).getVehicleStatusCode());
            }
        } else {
            setVehicle();
        }
    }

    public void updatelocation() {
        LocationLayout.setVisibility(View.VISIBLE);
        LocationName.setText(masterPojo.getUpdating() + " ..");
    }

    private void callApiForDisplayName(LiveMapsModel liveMapsModel) {
        try {
            if (getActivity() == null)
                return;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!AppStatus.getInstance(getActivity()).isOnline()) {
                        progressVisiblityGone();
                        Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_LONG).show();
                    }
                    if (liveMapsModel == null) {
                        LocationLayout.setVisibility(View.GONE);
                        return;
                    }


                    if (liveMapsModel.getLatitude() == null || liveMapsModel.getLongitude() == null) {
                        LocationLayout.setVisibility(View.GONE);
                        return;
                    }
                    try {
                        ApiInterface apiService = APIClient.getClient("").create(ApiInterface.class);
                        LocationResponseModel locationResponseModel = new LocationResponseModel();
                        locationResponseModel.setLatitude(liveMapsModel.getLatitude());
                        locationResponseModel.setLongitude(liveMapsModel.getLongitude());
                        retrofit2.Call<WebResponseString> call = apiService.getlocationaddress(locationResponseModel, "Bearer " + user.getAccessToken());
                        call.enqueue(new retrofit2.Callback<WebResponseString>() {
                            @Override
                            public void onResponse(retrofit2.Call<WebResponseString> call, retrofit2.Response<WebResponseString> response) {
                                try {
                                    WebResponseString webResponse = response.body();
                                    Boolean sucess = webResponse.getSuccess();
                                    String message = webResponse.getMessage();
                                    if (sucess) {
                                        String displayName = webResponse.getData() + "";
                                        LocationLayout.setVisibility(View.VISIBLE);
                                        LocationName.setText("" + displayName);
                                    } else {
                                        LocationLayout.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    LocationLayout.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                    Log.e(EXCEPTION_TAG, e.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<WebResponseString> call, Throwable t) {
                                LocationLayout.setVisibility(View.GONE);
                                //   Toast.makeText(get, t.getMessage(), Toast.LENGTH_LONG).show();
                                call.cancel();
                            }
                        });
                    } catch (Exception e) {
                        LocationLayout.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(EXCEPTION_TAG, e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
    }

    private void callApiForDisplayNameMarker(LiveMapsModel liveMapsModel) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressVisiblityVisible();
                    if (!AppStatus.getInstance(getActivity()).isOnline()) {
                        progressVisiblityGone();
                        Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_LONG).show();
                    }


                    if (liveMapsModel == null) {
                        progressVisiblityGone();
                        return;
                    }


                    if (liveMapsModel.getLatitude() == null || liveMapsModel.getLongitude() == null) {
                        progressVisiblityGone();
                        return;
                    }
                    try {
                        ApiInterface apiService = APIClient.getClient("").create(ApiInterface.class);
                        LocationResponseModel locationResponseModel = new LocationResponseModel();
                        locationResponseModel.setLatitude(liveMapsModel.getLatitude());
                        locationResponseModel.setLongitude(liveMapsModel.getLongitude());
                        retrofit2.Call<WebResponseString> call = apiService.getlocationaddress(locationResponseModel, "Bearer " + user.getAccessToken());
                        call.enqueue(new retrofit2.Callback<WebResponseString>() {
                            @Override
                            public void onResponse(retrofit2.Call<WebResponseString> call, retrofit2.Response<WebResponseString> response) {
                                try {
                                    WebResponseString webResponse = response.body();
                                    Boolean sucess = webResponse.getSuccess();
                                    String message = webResponse.getMessage();
                                    if (sucess) {
                                        if (liveMapsModel != null) {
                                            progressVisiblityGone();
                                            alertDialoge.showDialog(getActivity(), liveMapsModel, webResponse.getData());
                                        }
                                    } else {
                                        progressVisiblityGone();
                                    }
                                } catch (Exception e) {
                                    progressVisiblityGone();
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                    Log.e(EXCEPTION_TAG, e.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(retrofit2.Call<WebResponseString> call, Throwable t) {
                                progressVisiblityGone();
                                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                                call.cancel();
                            }
                        });
                    } catch (Exception e) {
                        progressVisiblityGone();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e(EXCEPTION_TAG, e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
    }

    private void loadImageFromStorage(String path, ImageView car, String statuscode, int angle, TextView speedview, String speedunitindesr, int speed, int ignition) {
        try {
            for (int i = 0; i < vehicleDetails.size(); i++) {
                if (vehicleDetails.get(i).getVehicleStatusCode().equals(statuscode)) {
                    // Log.e("getIcon_name()","vehicleDetails.get(i).getIcon_name()"+vehicleDetails.get(i).getIcon_name());
                    Matrix mat = new Matrix();
                    Bitmap b = getThumbnail("/" + vehicleDetails.get(i).getIcon_name());
                    mat.postRotate(Integer.parseInt(String.valueOf(angle)));
                    Bitmap bMapRotate = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), mat, true);
                    car.setImageBitmap(bMapRotate);
                    car.setVisibility(View.VISIBLE);
                    if (speed == 0 && ignition == 0) {
                        Log.e("HomeFragment", "angle" + angle);
                        speedview.setText("" + vehicleDetails.get(i).getVehicleStatus());
                    } else if (speed > 0 && ignition == 1) {
                        speedview.setText(" " + speedunitindesr + "");
                    } else if (speed == 0 && ignition == 1) {
                        speedview.setText("" + vehicleDetails.get(i).getVehicleStatus());
                    } else if (speed != 0 && ignition == 0) {
                        speedview.setText(" " + speedunitindesr);
                    }
                }
            }

        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }

    }

    public Bitmap getThumbnail(String filename) {
        Bitmap thumbnail = null;
        try {
            if (thumbnail == null) {
                try {
                    ContextWrapper cw = new ContextWrapper(getActivity());
                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                    FileInputStream fi = new FileInputStream(new File(directory + filename));
                    thumbnail = BitmapFactory.decodeStream(fi);
                    fi.close();

                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, e.getMessage());
                }
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
        return thumbnail;
    }

    @Override
    public void onDestroy() {
        try {
            if (handler != null) {
                handler.removeCallbacks(runnable1);
            }
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, e.getMessage());
        }
        super.onDestroy();
    }
    private void stopCarLocationUpdateonParked(int i){
        if(vehicleDetails.get(i).getVehicleStatusCode().equals("3")){
            vehicleAutoUpdate=false;
        }
        else {
            vehicleAutoUpdate=true;
        }
    }

}
