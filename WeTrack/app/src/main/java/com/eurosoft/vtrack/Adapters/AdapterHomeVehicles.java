package com.eurosoft.vtrack.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.Models.LiveMapsModel;
import com.eurosoft.vtrack.Models.VehicleStatusCode;
import com.eurosoft.vtrack.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterHomeVehicles extends RecyclerView.Adapter<AdapterHomeVehicles.ViewHolder> {


    private List<LiveMapsModel> lstVehicles;
    private Context context;
    private AdapterHomeVehicles.OnItemClickListener mOnItemClickListener;
    ArrayList<VehicleStatusCode> vehicleDetails;
    int row_index = 0;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position,LinearLayout layout, String vechileno,List<LiveMapsModel> lstVehicles,String code);
    }


    public AdapterHomeVehicles(Context ctx, List<LiveMapsModel> vehiclesList, AdapterHomeVehicles.OnItemClickListener onItemClickListener,  ArrayList<VehicleStatusCode> vmehicleDetails) {
        context = ctx;
        lstVehicles = vehiclesList;
        mOnItemClickListener = onItemClickListener;
        vehicleDetails=vmehicleDetails;
    }


    @Override
    public AdapterHomeVehicles.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_home_vehicles, parent, false);
        AdapterHomeVehicles.ViewHolder vh = new AdapterHomeVehicles.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterHomeVehicles.ViewHolder holder, int position) {
        LiveMapsModel vehicles = lstVehicles.get(position);
        holder.vehicleReg.setText(vehicles.getVehicleNo());
        holder.mainLL.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, position,holder.mainLL,vehicles.getVehicleNo(),lstVehicles,vehicles.getVehicleStatusCode());
                row_index = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
        setVehicleColor(holder,vehicles,vehicles.getVehicleStatusCode(),position);
    }

    @Override
    public int getItemCount() {
        return lstVehicles.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vehicleReg;
        private RelativeLayout rlBg;
        private LinearLayout mainLL;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            vehicleReg = itemView.findViewById(R.id.numberPlate);
            rlBg = itemView.findViewById(R.id.container);
            mainLL = itemView.findViewById(R.id.mainLL);
        }
    }
    private void setVehicleColor( AdapterHomeVehicles.ViewHolder holder,LiveMapsModel vehicles,String statuscode,int position){

        try {

            for (int i = 0; i <vehicleDetails.size() ; i++) {
//                Log.e("AdapterHomeVehicles", "color"+vehicleDetails.get(i).getVehicleColor());
//                Log.e("AdapterHomeVehicles", "statuscode"+statuscode);
                if(statuscode.equals(vehicleDetails.get(i).getVehicleStatusCode())){
                    holder.rlBg.setBackgroundColor(Color.parseColor(vehicleDetails.get(i).getVehicleColor()));
                    holder.mainLL.getBackground().setTint(Color.parseColor(vehicleDetails.get(i).getVehicleColor()));
                    holder.vehicleReg.setTextColor(Color.parseColor(vehicleDetails.get(i).getVehicleTextColor()));
                 //  break;
                }
            }
        }
        catch (Exception e){
            Log.e(Constants.EXCEPTION_TAG,""+e.getMessage());
        }


    }

    public void filterList(List<LiveMapsModel> filteredList) {
        try {
            lstVehicles = filteredList;
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(Constants.EXCEPTION_TAG,""+e.getMessage());
        }
    }

}