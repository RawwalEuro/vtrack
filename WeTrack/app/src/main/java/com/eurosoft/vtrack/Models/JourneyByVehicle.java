package com.eurosoft.vtrack.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class JourneyByVehicle implements Serializable {

    private String vehicleId;
    private String vehicleType;
    private ArrayList<Journey> journeyArrayList;
    private String tripId;
    private String startDate;
    private String endDate;
    private String startAddress;
    private String endAddress;
    private String tripStartDate;
    private String tripEndDate;


    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public JourneyByVehicle(String vehicleId, String vehicleType, ArrayList<Journey> journeyArrayList) {
        this.vehicleId = vehicleId;
        this.vehicleType = vehicleType;
        this.journeyArrayList = journeyArrayList;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public ArrayList<Journey> getJourneyArrayList() {
        return journeyArrayList;
    }

    public void setJourneyArrayList(ArrayList<Journey> journeyArrayList) {
        this.journeyArrayList = journeyArrayList;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
}
