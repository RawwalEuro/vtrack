package com.eurosoft.vtrack.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.ActivityListNotifications;
import com.eurosoft.vtrack.Adapters.AdapterEventTypes;
import com.eurosoft.vtrack.Adapters.AdapterNotifications;
import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.NotificationBody;
import com.eurosoft.vtrack.Models.NotificationRulesModel;
import com.eurosoft.vtrack.Models.Notifications;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.Models.Zone;
import com.eurosoft.vtrack.R;
import com.eurosoft.vtrack.Utils.NetworkAvailable.AppStatus;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.vtrack.Utils.Pagination.PaginationScrollListener;
import com.eurosoft.vtrack.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import es.dmoral.toasty.Toasty;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.vtrack.Constants.APIV1;
import static com.eurosoft.vtrack.Constants.DATETIME_FORMAT_FROMBACKEND;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;

public class FragmentNotifications extends Fragment implements View.OnClickListener {


    private ImageView backIcon;
    private RecyclerView rvEvents;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout rlProgressBar;
    private int page = 0;
    private int limit = 20;
    private LoginResponse user;
    private String fetchedUrl = "";
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private ArrayList<Notifications> arrayListNotifications = new ArrayList<>();
    private AdapterNotifications adapterNotifications;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout noResultRl;
    private int TOTAL_PAGES = 0;
    private int TOTAL_LENGTH = 0;
    private String clientId;
    private DecimalFormat f;
    private RecyclerView rvEventsType;
    private AdapterEventTypes adapterEventTypes;
    private LinearLayoutManager linearLayoutManagerHorizontal;
    private String zoneId = "";
    private RadioGroup radioGroupOne;
    private String dateTill, dateFrom;
    private String currentTime = "";
    private LinearLayout llCustom;
    private boolean isCustomSelected = false;
    private RelativeLayout rlFrom, rlTo;
    private TextView from, to;
    private boolean isFrom = false;
    private Calendar calendar;
    private DatePickerDialog dialog;
    private RelativeLayout vehicleLl;
    private TextView vehicle, ignition;
    ArrayList<String> items = new ArrayList<>();
    SpinnerDialog spinnerDialog;
    SpinnerDialog spinnerDialogIgintitonType;
    private String selectedVehicle = "";
    private String selectedEvent = "";
    private List<Vehicles> vehiclesList = new ArrayList<>();
    private ArrayList<String> itemsType;
    private ArrayList<String> eventTypeId;
    private Button btnViewEvents;
    private RelativeLayout rlIgnition;
    private MasterPojo masterPojo;
    private ImageView navIcon;
    Calendar calendarfromdate, calendertilldate;
    Dialog datePickerDialogfromdate, datePickerDialogtilldate;
    DatePicker datePickertilldate;
    int hrsFrom = 00, minsFrom = 00, hrsTill = 23, minsTill = 59;

    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_notification, container, false);
        Stash.init(getActivity());
        user = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        clientId = Stash.getString(Constants.USER_CLIENT_ID);
        fetchedUrl = (String) Stash.getString(Constants.URL_FETCHED);
        f = new DecimalFormat();
        initViews(view);
        getVehicles();
        return view;
    }

    private void initViews(View view) {
        backIcon = view.findViewById(R.id.backIcon);
        rvEvents = view.findViewById(R.id.rvEvents);
        rlProgressBar = view.findViewById(R.id.rlProgressBar);
        circularProgressBar = view.findViewById(R.id.circularProgressBar);
        noResultRl = view.findViewById(R.id.noResultRl);
        llCustom = view.findViewById(R.id.llCustom);
        rlFrom = (RelativeLayout) view.findViewById(R.id.rlFrom);
        rlTo = (RelativeLayout) view.findViewById(R.id.rlTo);
        from = (TextView) view.findViewById(R.id.from);
        to = (TextView) view.findViewById(R.id.to);
        vehicleLl = (RelativeLayout) view.findViewById(R.id.vehicleLl);
        vehicle = (TextView) view.findViewById(R.id.vehicle);
        ignition = (TextView) view.findViewById(R.id.ignition);
        btnViewEvents = (Button) view.findViewById(R.id.btnViewEvents);
        rvEvents = view.findViewById(R.id.rvEvents);
        rlIgnition = view.findViewById(R.id.rlIgnition);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvEvents.setLayoutManager(linearLayoutManager);
        rvEvents.setAdapter(adapterNotifications);
        RelativeLayout toolbarHead = view.findViewById(R.id.toolbarHead);
        toolbarHead.setVisibility(View.GONE);
        TextView headertitle = (TextView) view.findViewById(R.id.header_title);
        TextView vechile_title = (TextView) view.findViewById(R.id.vechile_title);
        TextView eventTypeTitle = (TextView) view.findViewById(R.id.event_type_title);
        RadioButton today = (RadioButton) view.findViewById(R.id.todayRB);
        RadioButton custom = (RadioButton) view.findViewById(R.id.customRB);
        RadioButton last7day = (RadioButton) view.findViewById(R.id.weeklyRB);
        RadioButton last48hour = (RadioButton) view.findViewById(R.id.last48RB);
        TextView totitle = (TextView) view.findViewById(R.id.to_title);
        TextView fromtitle = (TextView) view.findViewById(R.id.from_title);
        TextView message = (TextView) view.findViewById(R.id.message);
        masterPojo =  (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        message.setText(masterPojo.getPleasewaitwearegettingyourevents());
        headertitle.setText(masterPojo.getEvents());
        vechile_title.setText(masterPojo.getVehicle());
        vehicle.setHint(masterPojo.getSelectVehicle());
        eventTypeTitle.setText(masterPojo.getEventType());
        ignition.setHint(masterPojo.getIgnitionReport());
        today.setText(masterPojo.getToday());
        custom.setText(masterPojo.getCustom());
        last7day.setText(masterPojo.getLast7Days());
        last48hour.setText(masterPojo.getLast48Hours());
        btnViewEvents.setText(masterPojo.getViewEvents());
        to.setHint(masterPojo.getSelectDate());
        from.setHint(masterPojo.getSelectDate());
        fromtitle.setText(masterPojo.getFrom());
        totitle.setText(masterPojo.getTo());
        rvEventsType = view.findViewById(R.id.rvEventsType);
        linearLayoutManagerHorizontal = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        adapterNotifications = new AdapterNotifications(getActivity());
        rvEvents.setLayoutManager(linearLayoutManager);
        rvEvents.setAdapter(adapterNotifications);
        radioGroupOne = (RadioGroup) view.findViewById(R.id.radioGroup);
        setupDateView();

/*        rlFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFrom = false;
                getCalendarInstance();
                dialog.show();
            }
        });

        rlTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isFrom = true;
                getCalendarInstance();
                dialog.show();
            }
        });*/

        todaySetup(llCustom, 0, "today");
        radioGroupOne.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.todayRB:
                        todaySetup(llCustom, 0, "today");
                        isCustomSelected = false;
                        break;

                    case R.id.last48RB:
                        todaySetup(llCustom, -2, "last48RB");
                        isCustomSelected = false;
                        break;

                    case R.id.weeklyRB:
                        todaySetup(llCustom, -7, "last48RB");
                        isCustomSelected = false;
                        break;

                    case R.id.customRB:
                        llCustom.setVisibility(View.VISIBLE);
                        isCustomSelected = true;
                        break;
                }
            }
        });


        backIcon.setVisibility(View.INVISIBLE);
        navIcon = (ImageView) view.findViewById(R.id.navIcon);
        navIcon.setVisibility(View.VISIBLE);
        btnViewEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation(isCustomSelected);
            }
        });
    }


    private void getVehicles() {
        class GetVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                vehiclesList = DatabaseClient
                        .getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .getAll();
                return vehiclesList;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                if (vehiclesList == null || vehiclesList.size() == 0) {
                    return;
                } else {
                    items = new ArrayList<>();
                    for (int i = 0; i < vehiclesList.size(); i++) {
                        items.add(vehiclesList.get(i).getVehicleReg());
                    }
                    itemsType = new ArrayList<>();
                    eventTypeId=new ArrayList<>();
                    getAllRules(itemsType);
                    vehicle.setText(vehiclesList.get(0).getVehicleReg());
                    selectedVehicle = vehiclesList.get(0).getVehicleReg();
                    spinnerDialog = new SpinnerDialog(getActivity(), items, masterPojo.getSelectVehicle(), masterPojo.getClose());// With No Animation
                    spinnerDialogIgintitonType = new SpinnerDialog(getActivity(), itemsType, masterPojo.getSelectreporttype(), masterPojo.getClose());// With 	Animation
                    spinnerDialog.setCancellable(true); // for cancellable
                    spinnerDialog.setShowKeyboard(false);// for open keyboard by default
                    spinnerDialogIgintitonType.setCancellable(true); // for cancellable
                    spinnerDialogIgintitonType.setShowKeyboard(false);

                    spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String item, int position) {
                            Toast.makeText(getActivity(), item + "  ", Toast.LENGTH_SHORT).show();
                            vehicle.setText(item);
                            selectedVehicle = item;
                        }
                    });

                    spinnerDialogIgintitonType.bindOnSpinerListener(new OnSpinerItemClick() {
                        @Override
                        public void onClick(String item, int position) {
                            try {
                                Toast.makeText(getActivity(), item + "  ", Toast.LENGTH_SHORT).show();
                                ignition.setText(item);
                                selectedEvent = eventTypeId.get(position);
                            }
                            catch (Exception e){
                                Log.e(EXCEPTION_TAG,""+e.getMessage());
                            }
                        }
                    });

                    rlIgnition.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            spinnerDialogIgintitonType.showSpinerDialog();
                        }
                    });

                    vehicleLl.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            spinnerDialog.showSpinerDialog();
                        }
                    });
                }
            }

        }
        GetVehiclesList gt = new GetVehiclesList();
        gt.execute();
    }


    private void validation(boolean isCustomTrue) {


        if (isCustomTrue == true) {

            String fromText = from.getText().toString();
            String toText = to.getText().toString();
            if (fromText.equalsIgnoreCase("") || fromText.equalsIgnoreCase("")) {
                Toasty.error(getActivity(), masterPojo.getYouhaventselectedfromdate(), Toasty.LENGTH_SHORT).show();
                return;
            }

            if (toText.equalsIgnoreCase("") || toText.equalsIgnoreCase("")) {
                Toasty.error(getActivity(), masterPojo.getYouhaventselectedfromdate(), Toasty.LENGTH_SHORT).show();
                return;
            }

            SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT_FROMBACKEND);
    //dates to be compare
            Date date1 = null;
            Date date2 = null;
            try {
                Log.e("FragmentNotifications","dateTill"+dateTill);
                Log.e("FragmentNotifications","dateFrom"+dateFrom);
                date1 = sdf.parse(dateFrom);
                date2 = sdf.parse(dateTill);
            } catch (ParseException e) {
                Log.e(EXCEPTION_TAG,""+e.getMessage());
            }

            if (isCustomTrue == true) {

                try {
                    if (sdf.format(date2).compareTo(sdf.format(date1)) < 0) {
                        Toasty.error(getActivity(), masterPojo.getFromdatecantbegreaterthantodate(), Toasty.LENGTH_SHORT).show();
                        return;
                    }
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }

            }

        }

        if (selectedVehicle.equalsIgnoreCase("")) {
            Toasty.warning(getActivity(), masterPojo.getSelectVehicletogeneratereport(), Toasty.LENGTH_LONG).show();
            return;
        }


        NotificationBody notificationBody = new NotificationBody();
        notificationBody.setEventType(selectedEvent);
        notificationBody.setVehicleReg(selectedVehicle);
        notificationBody.setToDate(dateTill);
        notificationBody.setFromDate(dateFrom);
        notificationBody.setClientId(clientId);
        notificationBody.setTimeZone(Stash.getString(Constants.TIME_ZONE));

        if (!AppStatus.getInstance(getActivity()).isOnline()) {
            Toasty.error(getActivity(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient(APIV1).create(ApiInterface.class);

        Call<CustomWebResponse<Notifications>> call = apiService.getAllEvents(notificationBody, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Notifications>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Notifications>> call, Response<CustomWebResponse<Notifications>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        progressVisiblityGone();
                        return;
                    }

                    if (!response.body().getSuccess()) {
                        progressVisiblityGone();
                        Toasty.error(getActivity(), masterPojo.getNoResultFound(), Toasty.LENGTH_SHORT).show();
                        //Toasty.error(getApplicationContext(), response.body().getMessage());
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        progressVisiblityGone();


                        if (response.body().getData().size() == 0 || response.body().getData() == null) {
                            Toasty.error(getActivity(), masterPojo.getNoResultFound(), Toasty.LENGTH_SHORT).show();
                            progressVisiblityGone();
                            return;
                        } else {
                            noResultRl.setVisibility(View.GONE);
                            progressVisiblityGone();
                            Intent intent = new Intent(getActivity(), ActivityListNotifications.class);
                            Stash.put(Constants.LIST_NOTIFICATION, response.body().getData());
                            startActivity(intent);
                        }

                    }
                }
                catch (Exception e){
                    progressVisiblityGone();
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    Toasty.error(getActivity(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<CustomWebResponse<Notifications>> call, Throwable t) {
                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });


    }


    private void todaySetup(LinearLayout layoutCustom, int i2, String today) {
        layoutCustom.setVisibility(View.GONE);
        getCalculatedDate("yyyy-MM-dd", i2);
        currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        dateFrom = getCalculatedDate("yyyy-MM-dd", i2) + "T00:00:00Z";
        dateTill = getCalculatedDate("yyyy-MM-dd", 0) + "T" + currentTime + "Z";
        Log.e("FragmentNotifications","dateFrom"+ dateFrom);
    }


    public static String getCalculatedDate(String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }


   /* private void getCalendarInstance() {
        calendar = Calendar.getInstance(TimeZone.getDefault());

        dialog = new DatePickerDialog(getActivity(), datePickerListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.getDatePicker().setMaxDate(new Date().getTime());

        setDataIsFrom(String.valueOf(calendar.get(Calendar.YEAR)), String.valueOf(calendar.get(Calendar.MONTH) + 1), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        setDataTo(String.valueOf(calendar.get(Calendar.YEAR)), String.valueOf(calendar.get(Calendar.MONTH) + 1), String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        dateTill = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        dateFrom = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }*/


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            selectedMonth = selectedMonth + 1;
            String year = String.valueOf(selectedYear);
            //int seletdMonth = (selectedMonth<10? Integer.parseInt(("0"+selectedMonth)) :(selectedMonth));
            String seletdMonth = f.format(selectedMonth);
            String month = seletdMonth;
            String day = (f.format(selectedDay));
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            if (isFrom) {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateTill = year + "-" + month + "-" + day + " " + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataIsFrom(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));
                            } else {
                                String time = "T" + hourOfDay + ":" + minute + ":00Z";
                                dateFrom = year + "-" + month + "-" + day + " " + f.format(hourOfDay) + ":" + f.format(minute) + ":00Z";
                                setDataTo(year, month, day, convertIntoTwo(hourOfDay) + ":" + convertIntoTwo(minute));
                            }
                        }
                    }, hour, minute, true);
            if (!isFrom) {
                timePickerDialog.updateTime(00, 00);
            }

            timePickerDialog.show();


            /*timePickerDialog.show();
           if (isFrom) {
                dateTill = year + "-" + month + "-" + day + " " + "00:00:00Z";
                setDataIsFrom(year, month, day);
            } else {
                dateFrom = year + "-" + month + "-" + day + " " + "00:00:00Z";
                setDataTo(year, month, day);
            }*/
        }
    };


    public String convertIntoTwo(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }


    private void setDataIsFrom(String year, String month, String day, String time) {
        to.setText(day + "-" + month + "-" + year + " " + time);
    }


    private void setDataTo(String year, String month, String day, String time) {
        //fromdate.setText(day + "-" + month + "-" + year);
        from.setText(day + "-" + month + "-" + year + " " + " " + time);
    }

    private void callApiGetZones() {
        if (!AppStatus.getInstance(getActivity().getApplicationContext()).isOnline()) {
            Toasty.error(getActivity().getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient(APIV1).create(ApiInterface.class);

        Notifications notifications = new Notifications(clientId);
        Call<CustomWebResponse<Zone>> call = apiService.getZones(notifications, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Zone>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Zone>> call, Response<CustomWebResponse<Zone>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        progressVisiblityGone();
                        return;
                    }

                    if (!response.body().getSuccess()) {
                        progressVisiblityGone();
                        Toasty.error(getActivity().getApplicationContext(), response.body().getMessage());
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        progressVisiblityGone();


                        if (response.body().getData() != null) {
                            zoneId = response.body().getData().get(0).getId();
                        }


                        adapterEventTypes = new AdapterEventTypes(getActivity(), response.body().getData(), new AdapterEventTypes.OnItemClickListenerEvent() {
                            @Override
                            public void onItemClick(View view, int position, Zone zone) {
                                zoneId = zone.getId();
                                loadFirstPage(page, limit);
                            }
                        });
                    }

                    rvEventsType.setLayoutManager(linearLayoutManagerHorizontal);
                    rvEventsType.setAdapter(adapterEventTypes);


                    rvEvents.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
                        @Override
                        protected void loadMoreItems() {


                            if (!AppStatus.getInstance(getActivity().getApplicationContext()).isOnline()) {
                                Toasty.error(getActivity().getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
                                return;
                            } else {
                                isLoading = true;
                                page += 1;
                                loadNextPage();
                            }


                        }

                        @Override
                        public boolean isLastPage() {
                            return isLastPage;
                        }

                        @Override
                        public boolean isLoading() {
                            return isLoading;
                        }
                    });

                    loadFirstPage(page, limit);
                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    Toasty.error(getActivity(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }


            }

            @Override
            public void onFailure(Call<CustomWebResponse<Zone>> call, Throwable t) {
                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });


    }


    private void loadFirstPage(int page, int limit) {

        if (!AppStatus.getInstance(getActivity().getApplicationContext()).isOnline()) {
            Toasty.error(getActivity().getApplicationContext(), masterPojo.getYouarenotconnectedtointernet(), Toasty.LENGTH_LONG).show();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient(APIV1).create(ApiInterface.class);

        Notifications notifications = new Notifications(clientId, zoneId);
        Call<CustomWebResponse<Notifications>> call = apiService.getAllEventsByPagination(page, limit, notifications, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Notifications>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Notifications>> call, Response<CustomWebResponse<Notifications>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        progressVisiblityGone();
                        return;
                    }

                    if (!response.body().getSuccess()) {
                        progressVisiblityGone();
                        Toasty.error(getActivity().getApplicationContext(), response.body().getMessage());
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        progressVisiblityGone();
                        adapterNotifications.clearAll();
                        adapterNotifications.addAll(response.body().getData());

                        if (response.body().getData().size() == 0 || response.body().getData() == null) {
                            noResultRl.setVisibility(View.VISIBLE);
                            return;
                        } else {
                            noResultRl.setVisibility(View.GONE);


                            try {
                                String path = response.body().getMessage();
                                String segments[] = path.split("=");
                                String document = segments[segments.length - 2];
                                String totalLength = document.substring(0, 2);
                                TOTAL_LENGTH = Integer.parseInt(totalLength);
                                TOTAL_PAGES = TOTAL_LENGTH / 20;
                                Log.e("FragmentNotifications", "TOTAL_PAGES"+TOTAL_PAGES + "");

                                if (page < TOTAL_PAGES) adapterNotifications.addLoadingFooter();
                                else isLastPage = true;
                            } catch (Exception e) {
                                Log.e(EXCEPTION_TAG,""+e.getMessage());
                            }


                        }

                    }
                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    Toasty.error(getActivity(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<Notifications>> call, Throwable t) {
                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                progressVisiblityGone();
            }
        });

    }

    private void loadNextPage() {

        if (page > TOTAL_PAGES) {
            adapterNotifications.removeLoadingFooter();
            return;
        }


        ApiInterface apiService = APIClient.getClient(fetchedUrl).create(ApiInterface.class);
        Notifications notifications = new Notifications(clientId, zoneId);
        Call<CustomWebResponse<Notifications>> call = apiService.getAllEventsByPagination(page, limit, notifications, "Bearer " + user.getAccessToken());

        call.enqueue(new Callback<CustomWebResponse<Notifications>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Notifications>> call, Response<CustomWebResponse<Notifications>> response) {
                try {
                    isLoading = false;
                    List<Notifications> results = response.body().getData();
                    adapterNotifications.addAll(results);
                    if (page < TOTAL_PAGES) {
                        adapterNotifications.addLoadingFooter();
                    } else {
                        adapterNotifications.removeLoadingFooter();
                        isLastPage = true;
                    }
                }
                catch (Exception e){
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                    Toasty.error(getActivity(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<CustomWebResponse<Notifications>> call, Throwable t) {
                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
            }
        });
    }


    @Override
    public void onClick(View v) {

    }

    public void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        btnViewEvents.setClickable(false);
    }


    public void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        btnViewEvents.setClickable(true);

    }

    private void setupDateView() {
        getCalendarInstance();
        rlFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFrom = false;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);
            }
        });

        rlTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isFrom = true;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);
            }
        });

        initDatePickerFromDateDialog();
        initDatePickerTillDateDialog();
    }

    public void initDatePickerFromDateDialog() {
        datePickerDialogfromdate = new Dialog(getActivity());
        datePickerDialogfromdate.setContentView(R.layout.datepicker_dialog);
        DatePicker datePicker = datePickerDialogfromdate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogfromdate.findViewById(R.id.today);
        EditText hours = datePickerDialogfromdate.findViewById(R.id.hours);
        EditText mins = datePickerDialogfromdate.findViewById(R.id.mins);
        hours.setHint("00");
        mins.setHint("00");
        textView1.setText(masterPojo.getFrom());
        TextView message = datePickerDialogfromdate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        TextView Ok = datePickerDialogfromdate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogfromdate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        datePicker.init(calendarfromdate.get(Calendar.YEAR), calendarfromdate.get(Calendar.MONTH), calendarfromdate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsFrom > 23 || minsFrom > 60) {
                    Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }

                if (hours.getText().toString().equals("")) {
                    hrsFrom=00;
                }
                if( mins.getText().toString().equals("")){
                    minsFrom=00;

                }
                setFromDate(datePicker.getDayOfMonth(), datePicker.getMonth(), datePicker.getYear(), convertIntoTwo(hrsFrom) + ":" + convertIntoTwo(minsFrom));
                datePickerDialogfromdate.dismiss();

            }
        });
        datePicker.setMaxDate(new Date().getTime());

        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogfromdate.dismiss();
            }
        });


       /* Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/


        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsFrom = Integer.parseInt(hours.getText().toString());
                }


                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals(""))

                        hrsFrom = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());

                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsFrom = Integer.parseInt(mins.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals(""))
                        minsFrom = Integer.parseInt(mins.getText().toString());
                    mins.setText(mins.getText().toString());
                    mins.setSelection(mins.getText().length());
                    return true;
                }
                return false;
            }
        });
    }

    public void initDatePickerTillDateDialog() {
        datePickerDialogtilldate = new Dialog(getActivity());
        datePickerDialogtilldate.setContentView(R.layout.datepicker_dialog);
        datePickertilldate = datePickerDialogtilldate.findViewById(R.id.simpleDatePicker);
        TextView textView1 = datePickerDialogtilldate.findViewById(R.id.today);
        textView1.setText(masterPojo.getTillDate());
        EditText hours = datePickerDialogtilldate.findViewById(R.id.hours);
        TextView message = datePickerDialogtilldate.findViewById(R.id.time_message);
        message.setText(masterPojo.getSelecttimein24HoursFormat());
        EditText mins = datePickerDialogtilldate.findViewById(R.id.mins);
        updateDatePickerTillDate(0l,hours,mins);
        hours.setHint("23");
        mins.setHint("59");
        TextView Ok = datePickerDialogtilldate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogtilldate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOK());
        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogtilldate.dismiss();
            }
        });
/*
        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hrsTill > 23 || minsTill > 60) {
                    Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }
                if (hours.getText().toString().equals("") ) {
                    hrsTill=23;
                }
                if(mins.getText().toString().equals("")){
                    minsTill=59;
                }


                setTillDate(datePickertilldate.getDayOfMonth(), datePickertilldate.getMonth(), datePickertilldate.getYear(), convertIntoTwo(hrsTill) + ":" + convertIntoTwo(minsTill));
                datePickerDialogtilldate.dismiss();

            }
        });*/
        hours.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!hours.getText().toString().equals("")) {
                    hrsTill = Integer.parseInt(hours.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!hours.getText().toString().equals(""))
                        hrsTill = Integer.parseInt(hours.getText().toString());
                    hours.setText(hours.getText().toString());
                    hours.setSelection(hours.getText().length());
                    return true;
                }
                return false;
            }
        });

        mins.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if (!mins.getText().toString().equals("")) {
                    minsTill = Integer.parseInt(mins.getText().toString());
                }

                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    if (!mins.getText().toString().equals(""))
                        minsTill = Integer.parseInt(mins.getText().toString());
                    mins.setText(mins.getText().toString());
                    mins.setSelection(mins.getText().length());

                    return true;
                }
                return false;
            }
        });

    }

    public void updateDatePickerTillDate(Long mindate,EditText hours,EditText mins) {
        datePickertilldate.init(calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                Log.d("Date", "Year=" + year + " Month=" + (month + 1) + " day=" + dayOfMonth);
                if (hrsTill > 23 || minsTill > 60) {
                    Toasty.error(getActivity(),  masterPojo.getInvalidTime(), Toasty.LENGTH_SHORT).show();
                    return;
                }
                if (hours.getText().toString().equals("") ) {
                    hrsTill=23;
                }
                if(mins.getText().toString().equals("")){
                    minsTill=59;
                }
                setTillDate(datePickertilldate.getDayOfMonth(), datePickertilldate.getMonth(), datePickertilldate.getYear(), convertIntoTwo(hrsTill) + ":" + convertIntoTwo(minsTill));
                datePickerDialogtilldate.dismiss();

            }
        });

        datePickertilldate.setMaxDate(new Date().getTime());
        datePickertilldate.setMinDate(mindate);

    }

    private void setFromDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);

        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        dateFrom =selectedYear + "-" + monthString + "-" + dayString + "T" + time + ":00Z";
        Log.e("FragmentNotifications","dateFrom"+dateFrom);
        calendarfromdate.set(selectedYear, selectedMonth, selectedDay);
        from.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogfromdate.dismiss();
    }

    private void setTillDate(int selectedDay, int selectedMonth, int selectedYear, String time) {
        selectedMonth = selectedMonth + 1;
        String monthString = String.valueOf(selectedMonth);
        String dayString = String.valueOf(selectedDay);
        String daytillString = String.valueOf(datePickertilldate.getDayOfMonth());


        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dayString.length() == 1) {
            dayString = "0" + dayString;
        }

        if (daytillString.length() == 1) {
            daytillString = "0" + daytillString;
        }
        dateTill = datePickertilldate.getYear() + "-" + monthString + "-" + daytillString + "T" + time + ":00Z";
        Log.e("FragmentNotifications","dateTill"+dateTill);
        calendertilldate.set(selectedYear, selectedMonth, selectedDay);
        to.setText(dayString + "-" + monthString + "-" + selectedYear + " " + time);
        datePickerDialogtilldate.dismiss();
    }


    private void getCalendarInstance() {
        calendarfromdate = Calendar.getInstance(TimeZone.getDefault());
        calendertilldate = Calendar.getInstance(TimeZone.getDefault());
    }
    public void getAllRules(ArrayList<String> itemsType) {
        itemsType.clear();
        ApiInterface apiService = APIClient.getClient(APIV1).create(ApiInterface.class);
        Call<List<NotificationRulesModel>> call = apiService.getAllRules("Bearer " + user.getAccessToken());
        call.enqueue(new Callback<List<NotificationRulesModel>>() {
            @Override
            public void onResponse(Call<List<NotificationRulesModel>> call, Response<List<NotificationRulesModel>> response) {
                try {
                    List<NotificationRulesModel> webResponse = response.body();
                    for(int i=0; i<webResponse.size(); i++){
                        itemsType.add(webResponse.get(i).getLanguagDict());
                        eventTypeId.add(webResponse.get(i).getRuleType());
                        Log.e("FragmentNotifications" , "itemsType"+webResponse.get(i).getRuleType());
                    }
                    ignition.setText(itemsType.get(0));
                    selectedEvent = eventTypeId.get(0);
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG,""+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<NotificationRulesModel>> call, Throwable t) {
                Toasty.error(getActivity(), masterPojo.getConnectionFailed() + "", Toast.LENGTH_SHORT, true).show();
                call.cancel();
            }
        });
    }

}
