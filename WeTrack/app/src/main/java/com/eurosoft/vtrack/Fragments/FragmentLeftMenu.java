package com.eurosoft.vtrack.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.eurosoft.vtrack.ActivityLogin;
import com.eurosoft.vtrack.ActivityReports;
import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.MainActivity;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.Vehicles;
import com.eurosoft.vtrack.R;
import com.eurosoft.vtrack.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;
import static com.eurosoft.vtrack.Constants.IsSatelliteView;

public class FragmentLeftMenu extends Fragment implements View.OnClickListener {


    private RelativeLayout logoutRl;
    private LoginResponse currentUser;
    private TextView userName, email;
    private LinearLayout events, trackHistory, dashboard;
    private List<Vehicles> vehiclesList = new ArrayList<>();
    private LinearLayout reports;
    private onCLoseDrawer onCLoseDrawerListeners;
    private  MasterPojo masterPojo;
    private onThreadListener onThreadListener;


    public interface onCLoseDrawer {
        public void sendEventCloseDrawer(boolean closeDrawer,String key);
    }

    public static Fragment newInstance() {
        FragmentLeftMenu f = new FragmentLeftMenu();
        return f;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            onCLoseDrawerListeners = (onCLoseDrawer) getActivity();
            onThreadListener=(onThreadListener) getActivity();
        } catch (ClassCastException e) {
            Log.e(EXCEPTION_TAG,""+e.getMessage());
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyInterface ");
        }

    }


    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left_menu, container, false);
        Stash.init(getActivity());
        currentUser = (LoginResponse) Stash.getObject(Constants.USER, LoginResponse.class);
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        logoutRl = view.findViewById(R.id.logoutRl);
        userName = view.findViewById(R.id.userName);
        email = view.findViewById(R.id.email);
        events = view.findViewById(R.id.events);
        trackHistory = view.findViewById(R.id.trackHistory);
        dashboard = view.findViewById(R.id.dashboard);
        reports = view.findViewById(R.id.reports);
        String clientname=Stash.getString(Constants.ClientName);
        userName.setText(clientname);
        email.setText(currentUser.getEmail());
        masterPojo= (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA,MasterPojo.class);
        TextView dashboard_text= view.findViewById(R.id.dashboard_text);
        TextView trip_history_title= view.findViewById(R.id.trip_history_title);
        TextView notifications= view.findViewById(R.id.notifications_text);
        TextView reports_text= view.findViewById(R.id.reports_text);
        TextView logout_text= view.findViewById(R.id.logout_text);
        dashboard_text.setText(masterPojo.getDashboard());
        trip_history_title.setText(masterPojo.getTripHistory());
        notifications.setText(masterPojo.getNotifications());
        reports_text.setText(masterPojo.getReports());
        logout_text.setText(masterPojo.getLogout());
        logoutRl.setOnClickListener(this);
        events.setOnClickListener(this);
        trackHistory.setOnClickListener(this);
        dashboard.setOnClickListener(this);
        reports.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.reports:
                onCLoseDrawerListeners.sendEventCloseDrawer(true,"");
                startActivity(new Intent(getActivity(), ActivityReports.class));
                break;

            case R.id.dashboard:
                onCLoseDrawerListeners.sendEventCloseDrawer(true,"LiveMap");
                break;

            case R.id.logoutRl:
                onThreadListener.setonThreadListener(false);
                Log.e("FragmentLeftMenu","TimerUpdates:"+"Cancel");

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(masterPojo.getAreyousure())
                        .setContentText(masterPojo.getYouwanttologout())
                        .setConfirmText(masterPojo.getYes())

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog
                                        .setTitleText(masterPojo.getLoggedOut())
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                callLogout(sDialog);
                            }
                        })

                        .setCancelText(masterPojo.getNo())
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();

                break;
        }

    }

    private void callLogout(SweetAlertDialog sDialog) {
        Stash.clear(Constants.isLoggenIn);
        deleteAllVehicles();
        sDialog.dismissWithAnimation();
        Stash.clear(IsSatelliteView);
        Intent i = new Intent(getActivity(), ActivityLogin.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        getActivity().finish();
    }


    private void getVehicles() {
        class GetVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                vehiclesList = DatabaseClient
                        .getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .getAll();
                return vehiclesList;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                Log.e("FragmentLeftMenu", "vehiclesList.size()"+vehiclesList.size() + "");
            }

        }
        GetVehiclesList gt = new GetVehiclesList();
        gt.execute();
    }


    private void deleteAllVehicles() {
        class DeleteVehiclesList extends AsyncTask<Void, Void, List<Vehicles>> {

            @Override
            protected List<Vehicles> doInBackground(Void... voids) {
                DatabaseClient.getInstance(getActivity())
                        .getAppDatabase()
                        .vehiclesDao()
                        .deleteAll();

                return null;
            }

            @Override
            protected void onPostExecute(List<Vehicles> orders) {
                super.onPostExecute(orders);
                getVehicles();

            }

        }
        DeleteVehiclesList gt = new DeleteVehiclesList();
        gt.execute();
    }

    public interface onThreadListener {
        public void setonThreadListener(Boolean state);
    }
}
