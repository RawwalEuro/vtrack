package com.eurosoft.vtrack.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.vtrack.Constants;
import com.eurosoft.vtrack.Models.DataWrapper;
import com.eurosoft.vtrack.Models.Journey;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.R;
import com.fxn.stash.Stash;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.eurosoft.vtrack.Constants.DATETIME_FORMATFROMBACKEND_ADAPTERLISTJOURNEY;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;

public class AdapterListJourneyByDate extends RecyclerView.Adapter<AdapterListJourneyByDate.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private List<Journey> lstJourney;
    private Context context;
    private MasterPojo masterPojo;
    private DataWrapper dataWrapper;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position,Journey journey);
    }

    public AdapterListJourneyByDate(Context ctx, List<Journey> listJourney, OnItemClickListener onItemClickListener,MasterPojo mmasterPojo) {
        context = ctx;
        lstJourney = listJourney;
        mOnItemClickListener = onItemClickListener;
        masterPojo=mmasterPojo;
        dataWrapper=DataWrapper.getInstance();
        Stash.init(context);
    }

    @Override
    public AdapterListJourneyByDate.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_list_journey, parent, false);
        AdapterListJourneyByDate.ViewHolder vh = new AdapterListJourneyByDate.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterListJourneyByDate.ViewHolder holder, int position) {
        Journey journey = lstJourney.get(position);
        holder.tripNo.setText(masterPojo.getTrip() + (position + 1));
        if (journey.getTripDurationHr() <= 0) {
            holder.travelTime.setText(masterPojo.getTotaltraveltimeis()+ journey.getTripDurationMins() + masterPojo.getMins());
        } else {
            holder.travelTime.setText(masterPojo.getTotaltraveltimeis()+ journey.getTripDurationHr() + masterPojo.getHr() + journey.getTripDurationMins() + masterPojo.getMins());
        }
        holder.fromAddress.setText(journey.getStargingpoint());
        holder.distance.setText(journey.getTotalDistance());
        holder.speed.setText(journey.getAverageSpeed());
        setData(journey,holder);
        holder.rlBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(),journey);
                Stash.put(Constants.JOURNEY_DATA,lstJourney.get(position));

            }
        });

    }


    public String convertDateFormat(String date,String formatter) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatter);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMMM dd yyyy");
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }


    public String converTimeFormat(String date,String formatter) throws ParseException {
        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatter);
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
        String finalDate = dateFormat2.format(objDate);
        return finalDate;
    }

/*
    private String timeCompare(String dateStart, String dateStop) {
      */
/*  String dateStart = "01/14/2012 09:29:58";
        String dateStop = "01/15/2012 10:31:48";*//*


        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            Log.e("d1", d1 + "");
            Log.e("d2", d2 + "");

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            return diffHours + " Hours " + diffMinutes + " mins " + diffSeconds + " secs";

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
*/

    @Override
    public int getItemCount() {
        return lstJourney.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tripNo, travelTime, fromDate, fromTime, fromAddress, toDate, toTime, toAddress, distance, speed,distanceHeading,speedHeading;
        private RelativeLayout rlBg;
        private ImageView car;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            rlBg = itemView.findViewById(R.id.rlBg);
            tripNo = itemView.findViewById(R.id.tripNo);
            travelTime = itemView.findViewById(R.id.travelTime);
            fromDate = itemView.findViewById(R.id.fromDate);
            fromTime = itemView.findViewById(R.id.fromTime);
            fromAddress = itemView.findViewById(R.id.fromAddress);
            toDate = itemView.findViewById(R.id.toDate);
            toTime = itemView.findViewById(R.id.toTime);
            distanceHeading=itemView.findViewById(R.id.distanceHeading);
            speedHeading=itemView.findViewById(R.id.speedHeading);
            toAddress = itemView.findViewById(R.id.toAddress);
            distance = itemView.findViewById(R.id.distance);
            speed = itemView.findViewById(R.id.speed);
            car = itemView.findViewById(R.id.car);
            distanceHeading.setText(masterPojo.getDistance());
            speedHeading.setText(masterPojo.getAvgSpeed());
        }
    }
    private void setData(Journey journey,AdapterListJourneyByDate.ViewHolder holder){
        try {
            if(journey.getTripEnd()==null){
              //  String tripStart = dataWrapper.stringWrapper(journey.getTripStart()).replace(".000Z", "");
                holder.toDate.setVisibility(View.INVISIBLE);
                holder.toTime.setVisibility(View.INVISIBLE);
                holder.toAddress.setText(masterPojo.getThisTripisRunning());
                holder.fromDate.setText(journey.getTripStartDateLabel());
                holder.fromTime.setText(journey.getTripStartTimeLabel());
            }
            else {
                holder.toDate.setText(journey.getTripEndDateLabel());
                holder.fromDate.setText(journey.getTripStartDateLabel());
                holder.fromTime.setText(journey.getTripStartTimeLabel());
                holder.toTime.setText(journey.getTripEndTimeLabel());
                holder.toAddress.setText(journey.getEndingPoint());
            }

        } catch (Exception e) {
            Log.e(EXCEPTION_TAG,""+e.getMessage());
            Toast.makeText(context,""+e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

}
