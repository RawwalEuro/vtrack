package com.eurosoft.vtrack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.vtrack.Fragments.FragmentHomeAdapter;
import com.eurosoft.vtrack.Fragments.FragmentNotifications;
import com.eurosoft.vtrack.Fragments.FragmentTrackingHistory;
import com.eurosoft.vtrack.Fragments.FragmentLeftMenu;
import com.eurosoft.vtrack.Fragments.HomeFragment;
import com.eurosoft.vtrack.Interfaces.onTripEventListener;
import com.eurosoft.vtrack.Models.LoginResponse;
import com.eurosoft.vtrack.Models.MasterPojo;
import com.eurosoft.vtrack.Models.VersionUpdate;
import com.eurosoft.vtrack.Utils.NetworkUtils.APIClient;
import com.eurosoft.vtrack.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.vtrack.Utils.NetworkUtils.CustomWebResponse;
import com.fxn.stash.Stash;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.vtrack.Constants.APIV1;
import static com.eurosoft.vtrack.Constants.EXCEPTION_TAG;

public class MainActivity extends AppCompatActivity implements onTripEventListener, FragmentLeftMenu.onCLoseDrawer,FragmentLeftMenu.onThreadListener {
    ViewPager viewPager;
    TabLayout tabLayout;
    ArrayList<Fragment> fragments;
    private LinearLayout tabListed;
    private ImageView navIcon, filter;
    private TextView headerTitle;
    public DrawerLayout mDrawer;
    private FragmentHomeAdapter pagerAdapter;
    private RelativeLayout toolbarHead;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout rlProgressBar;
    private MasterPojo masterPojo;
    String currentUser;
    onFilterClick onFilterClick;
    onDrawerListener onDrawerListener;
    onThreadListener onThreadListener;
    private FilterDialog filterDialog;
    FragmentManager mFragmentmanager;
    private Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stash.init(this);
        mFragmentmanager =  getSupportFragmentManager();
        initViews();
        setUpBottomNav();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        callApiGetVersion();
    }

    private void initViews() {
        masterPojo = (MasterPojo) Stash.getObject(Constants.LANGUAGE_DATA, MasterPojo.class);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        headerTitle = (TextView) findViewById(R.id.header_title);
        currentUser =  Stash.getString(Constants.ClientName);
        toolbarHead = (RelativeLayout) findViewById(R.id.toolbarHead);
        filter = (ImageView) findViewById(R.id.filter);
        navIcon = (ImageView) findViewById(R.id.navIcon);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        filterDialog = new FilterDialog();
        fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new FragmentTrackingHistory());
        fragments.add(new FragmentNotifications());
        headerTitle.setText(currentUser + "");

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFilterClick.setOnFilterClickListener();
            }
        });

        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* getSupportFragmentManager().beginTransaction().add(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();
                mDrawer.setScrimColor(getResources().getColor(R.color.semi_grey));*/
                mDrawer.openDrawer(GravityCompat.START, true);
            }
        });
        mDrawer.setScrimColor(getResources().getColor(R.color.semi_grey));
        mFragmentmanager.beginTransaction().add(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();



        mDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {
                Log.e("MainActivity", "Open");
                onDrawerListener.setonDrawerListener(true);
            }

            @Override
            public void onDrawerClosed(View view) {
                Log.e("MainActivity", "Close");
                onDrawerListener.setonDrawerListener(false);
                // your refresh code can be called from here
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

    }


    public void closeDrawer() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    private void setUpBottomNav() {
        try {
            pagerAdapter = new FragmentHomeAdapter(getSupportFragmentManager(), getApplicationContext(), fragments);
            viewPager.setAdapter(pagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_pin);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_steering);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_event);
            tabLayout.getTabAt(0).setText(masterPojo.getLiveMap());//
            tabLayout.getTabAt(1).setText(masterPojo.getTrackingHistory());//
            tabLayout.getTabAt(2).setText(masterPojo.getEvents());//
            tabListed = ((LinearLayout) tabLayout.getChildAt(0));
            for (int position = 0; position < tabListed.getChildCount(); position++) {
                LinearLayout item = ((LinearLayout) tabListed.getChildAt(position));
                item.setRotationX(180);
            }
            tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#00008B"));
            tabLayout.getTabAt(0).getIcon().setAlpha(255);
            tabLayout.getTabAt(1).getIcon().setAlpha(128);
            tabLayout.getTabAt(2).getIcon().setAlpha(128);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    switch (position) {
                        case 0:
                            fragment = pagerAdapter.getFragment(position);
                            headerTitle.setText(currentUser + "");
                            filter.setVisibility(View.GONE);
                            tabLayout.getTabAt(0).getIcon().setAlpha(255);
                            tabLayout.getTabAt(1).getIcon().setAlpha(128);
                            tabLayout.getTabAt(2).getIcon().setAlpha(128);
                            onThreadListener.setonThreadListener(true);
                            break;
                        case 1:
                            fragment = pagerAdapter.getFragment(position);
                            filter.setVisibility(View.GONE);
                            headerTitle.setText(masterPojo.getTrackingHistory());//
                            tabLayout.getTabAt(1).getIcon().setAlpha(255);
                            tabLayout.getTabAt(0).getIcon().setAlpha(128);
                            tabLayout.getTabAt(2).getIcon().setAlpha(128);
                            Stash.put(Constants.SHOW_BACK_BTN, 0);
                            onThreadListener.setonThreadListener(false);
                            break;
                        case 2:
                            fragment = pagerAdapter.getFragment(position);
                            filter.setVisibility(View.GONE);
                            headerTitle.setText(masterPojo.getEvents());//
                            tabLayout.getTabAt(2).getIcon().setAlpha(255);
                            tabLayout.getTabAt(1).getIcon().setAlpha(128);
                            tabLayout.getTabAt(0).getIcon().setAlpha(128);
                            Stash.put(Constants.SHOW_BACK_BTN, 0);
                            onThreadListener.setonThreadListener(false);
                            break;
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        } catch (Exception e) {
            Log.e(EXCEPTION_TAG, "" + e.getMessage());
        }
    }


    @Override
    public void onBackPressed() {

        mDrawer.setScrimColor(getResources().getColor(R.color.semi_grey));
        getSupportFragmentManager().beginTransaction().add(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();

        if (viewPager.getCurrentItem() == Constants.HOME_TAB) {
            finishAffinity();
        } else if (viewPager.getCurrentItem() == Constants.TRACKING_HISTORY_TAB) {
            viewPager.setCurrentItem(Constants.HOME_TAB);
        } else if (viewPager.getCurrentItem() == Constants.NOTIFICATIONS_TAB) {
            viewPager.setCurrentItem(Constants.HOME_TAB);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void sendDateAndVehicleReg(String startDate, String endDate, String vehReg) {
    }

    @Override
    public void sendEventCloseDrawer(boolean closeDrawer,String key) {
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
        mDrawer.closeDrawers();
        if(key.equals("LiveMap")){
            viewPager.setCurrentItem(Constants.HOME_TAB);
        }
    }



    public interface onFilterClick {
        public void setOnFilterClickListener();
    }

    public interface onDrawerListener {
        public void setonDrawerListener(Boolean state);
    }

    public interface onThreadListener {
        public void setonThreadListener(Boolean state);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof onFilterClick) {
            onFilterClick = (onFilterClick) fragment;
        }
        if (fragment instanceof onDrawerListener) {
            onDrawerListener = (onDrawerListener) fragment;
        }

        if(fragment instanceof onThreadListener){
            onThreadListener=(onThreadListener) fragment;
        }
    }

    private void callApiGetVersion() {
        VersionUpdate versionUpdate = new VersionUpdate(Stash.getString(Constants.USER_CLIENT_ID));
        ApiInterface apiService = APIClient.getClient(APIV1).create(ApiInterface.class);
        Call<CustomWebResponse<VersionUpdate>> call = apiService.versionUpdate(versionUpdate);
        call.enqueue(new Callback<CustomWebResponse<VersionUpdate>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<VersionUpdate>> call, Response<CustomWebResponse<VersionUpdate>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        return;
                    }
                    if (!response.body().getSuccess()) {
                        return;
                    }

                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().getData() == null) {
                            return;
                        }
                        VersionUpdate versionUpdate1 = response.body().getData().get(0);

                   /* versionUpdate1.setUpdate(true);
                    versionUpdate1.setPriority(1);*/

                        if (versionUpdate1.isUpdate() == false) {
                            return;
                        } else if (versionUpdate1.isUpdate() == true) {
                            try {
                                filterDialog.showDialog(MainActivity.this, versionUpdate1);
                            } catch (ParseException e) {
                                Log.e(EXCEPTION_TAG, "" + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(EXCEPTION_TAG, "" + e.getMessage());
                    Toasty.error(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<VersionUpdate>> call, Throwable t) {
                Log.e(EXCEPTION_TAG, "" + t.getMessage());
                Toasty.error(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    public class FilterDialog {

        public void showDialog(Activity activity, VersionUpdate versionUpdate) throws ParseException {
            final Dialog dialogFilter = new Dialog(activity, R.style.Theme_Dialog);
            dialogFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogFilter.setCancelable(false);
            dialogFilter.setContentView(R.layout.dialog_update_version);
            dialogFilter.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            Button btnUpdate = (Button) dialogFilter.findViewById(R.id.btnUpdate);
            Button btnClose = (Button) dialogFilter.findViewById(R.id.btnClose);
            TextView heading = (TextView) dialogFilter.findViewById(R.id.heading);
            btnClose.setText(masterPojo.getDismiss());
            btnUpdate.setText(masterPojo.getUpdate());
            heading.setText(masterPojo.getWehavenoticedthatyouareusingolderversionofthisapp());

            if (versionUpdate.getPriority() == 1) {
                btnUpdate.setVisibility(View.VISIBLE);
                btnClose.setVisibility(View.VISIBLE);
            }

            if (versionUpdate.getPriority() == 2) {
                btnUpdate.setVisibility(View.VISIBLE);
                btnClose.setVisibility(View.GONE);
            }

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    dialogFilter.dismiss();
                }
            });


            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogFilter.dismiss();
                }
            });
            dialogFilter.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
            Log.e("MainActivity", "onResume");
        }
        catch (Exception e){
            Log.e(EXCEPTION_TAG, ""+e.getMessage());
        }
    }
    @Override
    public void setonThreadListener(Boolean state) {
        onThreadListener.setonThreadListener(state);
    }

}